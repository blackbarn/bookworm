// # bytes-filter.js
// _public/modules/core/filters/_

// Bytes Filter

/*global angular */

angular.module('core').filter('bytes', function() {
    'use strict';

    return function(bytes, precision) {
        var units, number;
        units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'];
        if (bytes === 0) {
            return '0 bytes';
        }
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {
            return '-';
        }
        if (typeof precision === 'undefined') {
            precision = 1;
        }

        number = Math.floor(Math.log(bytes) / Math.log(1024));

        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
    };
});