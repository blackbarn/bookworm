// # header-controller-test.js
// _public/modules/core/tests/_

// Header Controller Tests

/*global ApplicationConfiguration */
(function () {
    'use strict';
    describe('HeaderController', function () {
        //Initialize global variables
        var HeaderController;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));

        beforeEach(inject(function ($controller) {
            HeaderController = $controller('HeaderController');
        }));

        it('should expose the authentication service', function () {
            expect(HeaderController.authentication).toBeTruthy();
        });
    });
})();