// # home-controller-test.js
// _public/modules/core/tests/_

// Home Controller Tests

/*global ApplicationConfiguration */
(function () {
    'use strict';
    describe('HomeController', function () {
        //Initialize global variables
        var HomeController;

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));

        beforeEach(inject(function ($controller) {

            HomeController = $controller('HomeController');
        }));

        it('should expose the authentication service', function () {
            expect(HomeController.authentication).toBeTruthy();
        });
    });
})();