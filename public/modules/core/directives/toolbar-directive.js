// # toolbar-directive.js
// _public/modules/core/directives/_

// Toolbar Directive

/*global angular */

angular.module('core').directive('bwToolbar', ['$modal', '$log', function ($modal, $log) {
    'use strict';

    return {
        restrict: 'A',
        replace: true,
        transclude: true,
        templateUrl: 'modules/core/views/toolbar.directive.view.html',
        scope: {
            collection: '=',
            pageChanged: '='
        },
        link: function (scope) {


            scope.openFilter = function (size) {
                var modalInstance = $modal.open({
                    templateUrl: 'modules/core/views/filter.modal.view.html',
                    controller: 'FilterModalController',
                    size: size,
                    resolve: {
                        filterData: function () {
                            return scope.collection.definitions;
                        },
                        collection: function () {
                            return scope.collection;
                        }
                    }
                });

                modalInstance.result.then(function (data) {
                    $log.info('Modal accepted', data);
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };
        }
    };
}]);
