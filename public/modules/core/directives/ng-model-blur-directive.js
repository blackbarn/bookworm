// # ng-model-blur-directive.js
// _public/modules/core/directives/_

// ngModel blur Directive

/*global angular */

angular.module('core').directive('ngModelOnblur', function() {
    'use strict';

    return {
        priority: 1,
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') {
                return;
            }

            elm.off('input keydown change');
            elm.on('blur', function() {
                scope.$apply(function() {
                    ngModelCtrl.$setViewValue(elm.val());
                });
            });
        }
    };
});
