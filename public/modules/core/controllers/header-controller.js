// # header-controller.js
// _public/modules/core/controllers/_

// Header Controller

/*global angular */
angular.module('core').controller('HeaderController', ['$rootScope', 'Authentication',
    function ($rootScope, Authentication) {
        'use strict';
        // Auth Service Reference
        this.authentication = Authentication;

        /**
         * Checks if menu is collapsed
         * @type {boolean}
         */
        this.isCollapsed = false;

        /**
         * Toggle's the menu collapsed state
         */
        this.toggleCollapsibleMenu = function () {
            this.isCollapsed = !this.isCollapsed;
        };

        // Collapse navbar when location changes
        $rootScope.$on('$locationChangeSuccess', function() {
            this.isCollapsed = false;
        }.bind(this));
    }
]);