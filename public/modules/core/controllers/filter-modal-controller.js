// # filter-modal-controller.js
// _public/modules/core/controllers/_

// Filter Modal Controller

/*global angular */
angular.module('core').controller('FilterModalController', ['$scope', '$modalInstance', 'filterData', 'collection',
    function ($scope, $modalInstance, filterData, collection) {
        'use strict';
        $scope.filterData = filterData;
        $scope.collection = collection;

        $scope.ok = function () {
            $modalInstance.close($scope.filterData);
        };

        $scope.clear = function () {
            angular.forEach($scope.filterData, function (val) {
                val.data = '';
            });
        };
    }
]);