// # home-controller.js
// _public/modules/core/controllers/_

// Home Controller

/*global angular */
angular.module('core').controller('HomeController', ['Authentication', function (Authentication) {
    'use strict';
    /**
     * Store Auth Reference
     */
    this.authentication = Authentication;
}]);