angular.module('core').controller('SearchController', ['Authentication', 'AuthorSearchProvider', 'BookSearchProvider', function (Authentication, AuthorSearchProvider, BookSearchProvider) {
    'use strict';

    var vm = this;

    /**
     * Store Auth Reference
     */
    this.authentication = Authentication;

    vm.query = "";

    vm.type = "author";

    vm.search = function (query) {

        if (vm.type === "author") {
            AuthorSearchProvider.$search({
                q: query
            }).$then(function (results) {
                vm.bookResults = [];
                vm.authorResults = results;
            });
        } else {
            BookSearchProvider.$search({
                q: query
            }).$then(function (results) {
                vm.authorResults = [];
                vm.bookResults = results;
            });
        }

    };

}]);