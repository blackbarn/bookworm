// # menus-service.js
// _public/modules/core/services/_

// Menu Service

/*global angular */
angular.module('core').service('Menus', [
    /**
     * @alias Menus
     */
    function () {
        'use strict';
        // Define a set of default roles
        this.defaultRoles = ['user'];

        // Define the menus object
        this.menus = {};

        // A private function for rendering decision
        var shouldRender = function (user) {
            var i, j;
            if (user) {
                for (i = 0; i < user.roles.length; i = i + 1) {
                    for (j = 0; j < this.roles.length; j = j + 1) {
                        if (this.roles[j] === user.roles[i]) {
                            return true;
                        }
                    }
                }
            } else {
                return this.isPublic;
            }

            return false;
        };

        // Validate menu existence
        this.validateMenuExistence = function (menuId) {
            if (menuId && menuId.length) {
                if (this.menus[menuId]) {
                    return true;
                } else {
                    throw new Error('Menu does not exists');
                }
            } else {
                throw new Error('MenuId was not provided');
            }
        };

        // Get the menu object by menu id
        this.getMenu = function (menuId) {
            // Validate that the menu exists
            this.validateMenuExistence(menuId);

            // Return the menu object
            return this.menus[menuId];
        };

        // Add new menu object by menu id
        this.addMenu = function (menuId, isPublic, roles) {
            // Create the new menu
            this.menus[menuId] = {
                isPublic: isPublic || false,
                roles: roles || this.defaultRoles,
                items: [],
                shouldRender: shouldRender
            };

            // Return the menu object
            return this.menus[menuId];
        };

        // Remove existing menu object by menu id
        this.removeMenu = function (menuId) {
            // Validate that the menu exists
            this.validateMenuExistence(menuId);

            // Return the menu object
            delete this.menus[menuId];
        };

        // Add menu item object
        this.addMenuItem = function (menuId, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles) {
            // Validate that the menu exists
            this.validateMenuExistence(menuId);

            // Push new menu item
            this.menus[menuId].items.push({
                title: menuItemTitle,
                link: menuItemURL,
                uiRoute: menuItemUIRoute || ('/' + menuItemURL),
                isPublic: isPublic || this.menus[menuId].isPublic,
                roles: roles || this.defaultRoles,
                shouldRender: shouldRender
            });

            // Return the menu object
            return this.menus[menuId];
        };

        // Remove existing menu object by menu id
        this.removeMenuItem = function (menuId, menuItemURL) {
            var i;
            // Validate that the menu exists
            this.validateMenuExistence(menuId);

            // Search for menu item to remove
            for (i = 0; i < this.menus[menuId].items; i = i + 1) {
                if (this.menus[menuId].items[i].link === menuItemURL) {
                    this.menus[menuId].items.splice(i, 1);
                }
            }

            // Return the menu object
            return this.menus[menuId];
        };

        //Adding the topbar menu
        this.addMenu('topbar');
    }
]);