// # dialog-service.js
// _public/modules/core/services/_

// Dialog Service

/*global angular */

angular.module('core').factory('Dialog', ['$modal', function ($modal) {
    'use strict';

    function confirmation(message, title) {
        var modal = $modal.open({
            templateUrl: 'modules/core/views/confirmation.modal.view.html',
            backdrop: 'static',
            keyboard: false,
            resolve: {
                data: function() {
                    return {
                        title: title ? title : 'Confirm',
                        message: message
                    };
                }
            },
            controller: ['$scope', '$modalInstance', 'data', function ($scope, $modalInstance, data) {
                $scope.data = data;

                $scope.ok = function() {
                    $modalInstance.close();
                };

                $scope.cancel = function() {
                    $modalInstance.dismiss();
                };
            }]
        });

        return modal.result;
    }

    return {
        confirmation: confirmation
    };
}]);