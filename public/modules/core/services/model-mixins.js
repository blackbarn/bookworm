

// # model-mixins.js
// _public/modules/core/services/_

// Restmod Model Mixins

/*global angular */

angular.module('core').service('DefinitionModel', ['restmod', function (restmod) {
    'use strict';
    return restmod.mixin({
        '@definitions': {},
        '@$defineProperties': function (definitions) {
            this.definitions = definitions;
        }
    });
}]);

angular.module('core').service('FilterableModel', ['restmod', function (restmod) {
    'use strict';
    return restmod.mixin({
        '@$getFilters': function () {
            var filters = {};
            angular.forEach(this.definitions, function (val, key) {
                if (val && val.isFilterable && val.data && val.data !== '') {
                    filters[key] = '/' + val.data + '/i';
                }
            });
            return filters;
        },
        '@$isFiltering': function () {
            var isFiltering = false;
            angular.forEach(this.definitions, function (val) {
                if (!isFiltering && val.data && val.data !== '') {
                    isFiltering = true;
                }
            });
            return isFiltering;
        }
    });
}]);

angular.module('core').service('ViewTypeModel', ['restmod', function (restmod) {
    'use strict';
    return restmod.mixin({
        '@isCollapsed': true,
        '@$toggleCollapsed': function () {
            this.isCollapsed = !this.isCollapsed;
        }
    });
}]);
angular.module('core').service('SortableModel', ['restmod', function (restmod) {
    'use strict';

    return restmod.mixin({
        '@predicate': '',
        '@reverse': true,
        '@$setPredicate': function (value) {
            if (this.predicate === value) {
                this.reverse = !this.reverse;
            }
            this.predicate = value;
        },
        '@$getSortableFields': function () {
            var sortable = {};
            angular.forEach(this.definitions, function (value, key) {
                if (value.isSortable) {
                    sortable[key] = value;
                }
            });
            return sortable;
        }

    });
}]);

angular.module('core').service('StatusModel', ['restmod', function (restmod) {
    'use strict';

    return restmod.mixin({
        '@statusFields': {},
        '@$setStatusFields': function (fields) {
            this.statusFields = fields;
        },
        '@$hasStatusFields': function () {
            return Object.keys(this.statusFields).length > 0;
        }

    });
}]);

angular.module('core').service('PagingModel', ['restmod', function (restmod) {
    'use strict';

    return restmod.mixin({
        '@page': 1,
        '@perPage': 10,
        '@total': 0,
        '@$getNumPages': function () {
            return Math.floor(this.total / this.perPage);
        }

    });
}]);

angular.module('core').service('BulkModel', ['restmod', function (restmod) {
    'use strict';

    return restmod.mixin({

        '@$updateAndSave': function (items, props) {
            return this.$save(this.$update(items, props));
        },
        '@$update': function (items, props) {
            angular.forEach(items, function (item) {
                angular.extend(item, props || {});
            });
            return items;
        },
        /**
         * ## $save
         *
         * Save the collection
         *
         * @param {Object[]=} items Optional list of items to save
         * @returns {*|CommonApi}
         */
        '@$save': function (items) {
            var rawCollection, url, request;

            items = items || this;

            // this can be replaced by this.$wrap when collection wrap function is added (soon).
            rawCollection = items.map(function(r) { return r.$encode(); });
            // var rawCollection = this.$wrap(RMUtils.UPDATE_MASK);
            url = this.$updateUrl ? this.$updateUrl() : this.$url();

            request = {
                method: 'PUT',
                url: url,
                data: rawCollection
            };

            this.$dispatch('before-save-selected', [request]);

            return this.$send(request, function(_response) {
                this.$deselectAll();
                this.$dispatch('after-save-selected', [_response]);
            }, function(_response) {
                this.$dispatch('after-save-selected-error', [_response]);
            });
        }
    });
}]);

angular.module('core').service('SelectableModel', ['restmod', function (restmod) {
    'use strict';

    return restmod.mixin({
        // Client only property to determine selected status
        selected: { init: false, mask: true },

        /**
         * ## $hasSelected
         *
         * Determines if the collection has any selected items;
         * @returns {boolean}
         */
        '@$hasSelected': function () {
            var i, len;
            for (i = 0, len = this.length; i < len; i = i + 1) {
                if (this[i].selected) {
                    return true;
                }
            }
            return false;
        },
        '@$getSelected': function () {
            return this.filter(function (item) {
                return item.selected;
            });
        },
        /**
         * ## $selectAll
         *
         * Selects all items in the collection
         */
        '@$selectAll': function () {
            this.forEach(function (item) {
                item.selected = true;
            });
            this.checked = true;
        },
        '@$toggleChecked': function () {
            if (this.checked) {
                this.$deselectAll();
            } else {
                this.$selectAll();
            }
        },
        /**
         * ## $deselectAll
         *
         * De-selects all items in the collection
         */
        '@$deselectAll': function () {
            this.forEach(function (item) {
                item.selected = false;
            });
            this.checked = false;
        }
    });

}]);
