// # core-config.js
// _public/modules/core/config/_

// Core Configuration

/*global angular */
// Configuring the Books module
angular.module('core').config(['$translateProvider', 'restmodProvider',
    function ($translateProvider, restmodProvider) {
        'use strict';

        $translateProvider.useStaticFilesLoader({
            prefix: '/languages/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('en');

        restmodProvider.rebase(function () {
            this.disableRenaming();
        });
    }
]);