// # core-routes.js
// _public/modules/core/config/_

// Core Routes Configuration

/*global angular */
// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        'use strict';
        // Redirect to home view when route not found
        $urlRouterProvider.otherwise('/');

        // Home state routing
        $stateProvider.
            state('home', {
                url: '/',
                templateUrl: 'modules/core/views/home.view.html'
            })
            .state('search', {
                url: '/search',
                templateUrl: 'modules/core/views/search.view.html',
                controller: 'SearchController',
                controllerAs: 'sc'
            });
    }
]);