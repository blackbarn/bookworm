// # author-controller.js
// _public/modules/authors/controller/_

// Author Controller

/*global angular */
angular.module('authors').controller('AuthorController', ['$scope', '$stateParams', '$log', '$location', 'socket', 'Authentication', 'author',
    function ($scope, $stateParams, $log, $location, socket, Authentication, author) {
        'use strict';

        // Store a reference to this as `vm` or "view model" object.
        var vm = this;

        // Store a reference to the current author
        vm.author = author;

        // Forward author event to scope
        socket.forward('author', $scope);

        // Watch for author event
        $scope.$on('socket:author', function (evt, author) {
            vm.author.merge(author);
        });

        // Store a reference to the auth service
        vm.authentication = Authentication;
    }
]);