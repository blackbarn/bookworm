// # authors-controller.js
// _public/modules/authors/controller/_

// Authors Controller

/*global angular */
// Authors controller
angular.module('authors').controller('AuthorsController', ['$scope', '$stateParams', '$log', '$location', '$filter', 'socket', 'ngNotify', 'Authentication', 'Author',
    function ($scope, $stateParams, $log, $location, $filter, socket, ngNotify, Authentication, Author) {
        'use strict';

        var vm;

        // Store a reference to this as `vm` or "view model" object.
        vm = this;

        // Store auth reference
        vm.authentication = Authentication;

        // Forward author event to scope
        socket.forward('author', $scope);

        // Watch for author event
        $scope.$on('socket:author', function (evt, evAuthor) {
            // When author event is sent and author matches current author, update status
            angular.forEach(vm.authors, function (author) {
                author.merge(evAuthor);
            });

        });

        // Create collection
        vm.authors = Author.$collection();

        vm.authors.$defineProperties({
            'name': {
                isSortable: true,
                isFilterable: true
            },
            'status': {
                isSortable: true,
                isFilterable: true,
                values: ['paused', 'active', 'refreshing', 'refreshing_new_books', 'refreshing_books']
            }
        });

        // Set default predicate
        vm.authors.predicate = 'name';

        // Define data refresh method
        vm.refresh = function () {

            vm.authors.$refresh(angular.extend({
                page: vm.authors.page,
                perPage: vm.authors.perPage,
                sort: ((vm.authors.reverse) ? '-' : '') + vm.authors.predicate,
                expand: 'provider',
                author: $stateParams.authorId
            }, vm.authors.$getFilters())).$then(function (results) {
                // Set ng-table total value
                vm.authors.total = results.$response.headers('X-Total-Count');
            }, function (err) {
                ngNotify.set($filter('translate')('ERROR_RETRIEVING_AUTHORS', { error: err.status + ': ' + (err.data.message || err.statusText)}), {
                    type: 'error',
                    sticky: true
                });
            });

        };

        // Watch for page changes
        vm.pageChanged = function () {
            vm.refresh();
        };

        // Watch for sorting changes
        $scope.$watch(function () {
            return vm.authors.predicate + vm.authors.reverse + JSON.stringify(vm.authors.definitions);
        }, function () {
            vm.refresh();
        });
    }
]);