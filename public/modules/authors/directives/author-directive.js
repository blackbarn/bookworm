// # author-directive.js
// _public/modules/authors/directives/_

// Author Directive

/*global angular */

angular.module('authors').directive('bwAuthor', ['$location', '$log', 'Dialog', 'Author', function ($location, $log, Dialog, Author) {
    'use strict';

    return {
        restrict: 'A',
        replace: true,
        transclude: true,
        templateUrl: function (elem, attrs) {
            if (attrs.isSearch) {
                return 'modules/authors/views/author.search.directive.view.html';
            } else {
                return 'modules/authors/views/author.directive.view.html';
            }
        },
        scope: {
            author: '=',
            isCollapsed: '=',
            view: '=',
            isList: '='
        },
        link: function (scope) {
            scope.remove = function () {
                Dialog.confirmation('Remove Author? Doing so will remove all books and releases associated with them.').then(function () {
                    scope.author.$destroy().$then(function () {
                        if (!scope.isList) {
                            $location.path('authors');
                        }
                    });
                });
            };

            scope.add = function () {
                var author = Author.$create(scope.author).$then(function () {
                    $log.log('saved', author.name);
                }, function (err) {
                    $log.log('err saving', err, author.name);
                });
            };
        }
    };
}]);
