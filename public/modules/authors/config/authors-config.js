// # authors-config.js
// _public/modules/authors/config/_

// Authors Configuration

/*global angular */
// Configuring the Authors module
angular.module('authors').run(['ngNotify',
    function (ngNotify) {
        'use strict';
        ngNotify.config({
            theme: 'pure'
        });
    }
]);

