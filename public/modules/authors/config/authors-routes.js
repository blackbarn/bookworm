// # authors-routes.js
// _public/modules/authors/config/_

// Authors Routes Configuration

/*global angular */
//Setting up route
angular.module('authors').config(['$stateProvider',
    function ($stateProvider) {
        'use strict';
        // Authors state routing
        $stateProvider.
            state('authors', {
                url: '/authors',
                templateUrl: 'modules/authors/views/authors.view.html',
                controller: 'AuthorsController',
                controllerAs: 'ac'
            }).
            state('author', {
                url: '/authors/:authorId',
                template: '<div data-bw-author data-author="ac.author" data-view="\'full\'" />',
                controller: 'AuthorController',
                controllerAs: 'ac',
                resolve: {
                    author: ['Author', '$stateParams', function (Author, $stateParams) {
                        return Author.$new($stateParams.authorId).$fetch({
                            expand: 'provider,books'
                        });
                    }]
                }
            });
    }
]);