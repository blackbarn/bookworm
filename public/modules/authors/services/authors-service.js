// # authors-service.js
// _public/modules/authors/service/_

// Authors Service

/*global angular */

// Author Socket
angular.module('authors').factory('socket', ['socketFactory', function (socketFactory) {
    'use strict';
    return socketFactory();
}]);

// Author Model
angular.module('authors').factory('Author', ['$filter', '$log', 'restmod', 'ngNotify', function ($filter, $log, restmod, ngNotify) {
    'use strict';
    /**
     * @alias Author
     * @type {StaticApi}
     */
    var model = restmod.model('/authors', 'DefinitionModel', 'SelectableModel', 'BulkModel', 'SortableModel', 'PagingModel', 'StatusModel', 'ViewTypeModel', 'FilterableModel', {
        // Model property configuration
        PRIMARY_KEY: '_id',
        id: { mask: 'C'},
        books: { hasMany: 'Book', inline: true, mask: 'CU'},
        user: { hasOne: 'User', key: 'user', inline: true, mask: 'CU'},
        provider: { hasOne: 'Provider', key: 'provider', inline: true, mask: 'CU'},
        statuses: { mask: true, init: ['active', 'paused', 'refreshing_books', 'refreshing_new_books', 'refreshing']},

        // Model custom methods

        /**
         * ## isRefreshing
         *
         * Determines if the author is in a refreshing state
         * @returns {boolean}
         */
        isRefreshing: function () {
            return this.status && this.status.indexOf('refresh') === 0;
        },
        /**
         * ## pause
         *
         * Pause the author
         */
        pause: function () {
            this.status = 'paused';
            this.$save();
        },
        /**
         * ## resume
         *
         * Resume the author
         */
        resume: function () {
            this.status = 'active';
            this.$save();
        },
        /**
         * ## refresh
         *
         * Refresh the author
         */
        refresh: function () {
            this.status = 'refreshing';
            this.$save();
        },
        /**
         * ## merge
         *
         * Merge one author's data into this one
         *
         * @param {Author} author Author to merge from
         */
        merge: function (author) {
            if (author._id === this._id) {
                this.status = author.status;
            }
        },
        /**
         * ## refreshBooks
         *
         * Refresh the authors books, new or all.
         * @param {boolean} newOnly Only refresh new books
         */
        refreshBooks: function (newOnly) {
            this.status = (newOnly) ? 'refreshing_new_books' : 'refreshing_books';
            this.$save();
        },

        // Model Hooks

        /**
         * ## ~after-save hook
         */
        '~after-save': function () {
            ngNotify.set($filter('translate')('AUTHOR_SAVED_SUCCESSFULLY', { name: this.name }), 'success');
        },
        /**
         * ## ~after-save-error hook
         * @param {Error} err Error object
         */
        '~after-save-error': function (err) {
            ngNotify.set($filter('translate')('ERROR_SAVING_AUTHOR', { name: this.name, error: err.status + ': ' + (err.data.message || err.statusText) }), 'error');
        },
        /**
         * ## ~after-save-selected
         */
        '~after-save-selected': function () {
            ngNotify.set($filter('translate')('AUTHORS_SAVED_SUCCESSFULLY', {}), 'success');
        },
        /**
         * ## ~after-save-selected-error
         * @param err
         */
        '~after-save-selected-error': function (err) {
            ngNotify.set($filter('translate')('ERROR_SAVING_AUTHORS', { error: err.status + ': ' + (err.data.message || err.statusText) }), 'error');
        }
    });
    return model;
}]);