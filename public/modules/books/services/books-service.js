// # books-service.js
// _public/modules/books/service/_

// Books Service

/*global angular */


// Books Socket
angular.module('books').factory('socket', ['socketFactory', function (socketFactory) {
    'use strict';
    return socketFactory();
}]);

// Book Model
angular.module('books').factory('Book', ['$filter', '$log', 'restmod', 'ngNotify', function ($filter, $log, restmod, ngNotify) {
    'use strict';
    /**
     * @alias Book
     * @type {StaticApi}
     */
    var model = restmod.model('/books', 'DefinitionModel', 'SelectableModel', 'BulkModel', 'SortableModel', 'PagingModel', 'StatusModel', 'ViewTypeModel', 'FilterableModel', {
        // Model property configuration
        PRIMARY_KEY: '_id',
        releases: { hasMany: 'Release', inline: true, mask: 'CU'},
        user: { hasOne: 'User', key: 'user', inline: true, mask: 'CU'},
        provider: { hasOne: 'Provider', key: 'provider', inline: true, mask: 'CU'},
        author: { belongsTo: 'Author', key: 'author', inline: true, mask: 'CU'},
        statuses: { mask: true, init: ['wanted', 'wanted_new', 'skipped', 'downloaded', 'excluded']},

        // Model custom methods

        /**
         * ## merge
         *
         * Merge one book data with this one
         * @param {Book} book Book to merge from
         */
        merge: function (book) {
            if (this._id === book._id) {
                this.status = book.status;
            }
        },

        /**
         * ## setStatus
         *
         * Set the status of the book and save
         *
         * @param {String} status The status to set
         */
        setStatus: function (status) {
            this.status = status;
            this.$save();
        },

        // Model Hooks

        /**
         * ## ~after-save hook
         */
        '~after-save': function () {
            ngNotify.set($filter('translate')('BOOK_SAVED_SUCCESSFULLY', { title: this.title }), 'success');
        },
        /**
         * ## ~after-save-error hook
         * @param {Error} err Error object
         */
        '~after-save-error': function (err) {
            ngNotify.set($filter('translate')('ERROR_SAVING_BOOK', { title: this.title, error: err.status + ': ' + (err.data.message || err.statusText) }), 'error');
        },
        /**
         * ## ~after-save-selected
         */
        '~after-save-selected': function () {
            ngNotify.set($filter('translate')('BOOKS_SAVED_SUCCESSFULLY', {}), 'success');
        },
        /**
         * ## ~after-save-selected-error
         * @param err
         */
        '~after-save-selected-error': function (err) {
            ngNotify.set($filter('translate')('ERROR_SAVING_BOOKS', {error: err.status + ': ' + (err.data.message || err.statusText) }), 'error');
        }
    });
    return model;
}]);