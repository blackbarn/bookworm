// # books-routes.js
// _public/modules/books/config/_

// Books Routes Configuration

/*global angular */
//Setting up route
angular.module('books').config(['$stateProvider',
    function ($stateProvider) {
        'use strict';
        // Books state routing
        $stateProvider.
            state('books', {
                url: '/books',
                templateUrl: 'modules/books/views/books.view.html',
                abstract: true
            }).
            state('books.list', {
                url: '',
                templateUrl: 'modules/books/views/books.table.view.html',
                controller: 'BooksController',
                controllerAs: 'bc'
            }).
            state('author.books', {
                url: '/books',
                templateUrl: 'modules/books/views/books.table.view.html',
                controller: 'BooksController',
                controllerAs: 'bc'
            }).
            state('book', {
                url: '/authors/:authorId/books/:bookId',
                template: '<div data-bw-book data-book="bc.book" data-view="\'full\'" />',
                controller: 'BookController',
                controllerAs: 'bc',
                resolve: {
                    book: ['Book', '$stateParams', function (Book, $stateParams) {
                        return Book.$new($stateParams.bookId).$fetch({
                            expand: 'provider,author,releases'
                        });
                    }]
                }
            });
    }
]);