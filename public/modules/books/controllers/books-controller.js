// # books-controller.js
// _public/modules/books/controller/_

// Books Controller

/*global angular */
angular.module('books').controller('BooksController', ['$scope', '$location', '$stateParams', '$filter', 'socket', 'ngNotify', 'Authentication', 'Book',
    function ($scope, $location, $stateParams, $filter, socket, ngNotify, Authentication, Book) {
        'use strict';

        var vm;

        // Store a reference to this as `vm` or "view model" object.
        vm = this;

        // Store auth reference
        vm.authentication = Authentication;

        // Forward book event to scope
        socket.forward('book', $scope);

        // Watch for book event
        $scope.$on('socket:book', function (evt, evBook) {
            // When book event is sent and book matches current book, update status
            angular.forEach(vm.books, function (book) {
                book.merge(evBook);
            });

        });

        // Create collection
        vm.books = Book.$collection();

        vm.books.$defineProperties({
            'title': {
                isSortable: true,
                isFilterable: true
            },
            'published': {
                isSortable: true,
                isFilterable: true
            },
            'status': {
                isSortable: true,
                isFilterable: true,
                values: Book.$build().statuses
            }
        });

        // Set default predicate
        vm.books.predicate = 'published';

        // Define data refresh method
        vm.refresh = function () {

            vm.books.$refresh(angular.extend({
                page: vm.books.page,
                perPage: vm.books.perPage,
                sort: ((vm.books.reverse) ? '-' : '') + vm.books.predicate,
                expand: 'provider,author',
                author: $stateParams.authorId
            }, vm.books.$getFilters())).$then(function (results) {
                // Set ng-table total value
                vm.books.total = results.$response.headers('X-Total-Count');
            }, function (err) {
                ngNotify.set($filter('translate')('ERROR_RETRIEVING_BOOKS', { error: err.status + ': ' + (err.data.message || err.statusText)}), {
                    type: 'error',
                    sticky: true
                });
            });

        };

        // Watch for page changes
        vm.pageChanged = function () {
            vm.refresh();
        };

        // Watch for sorting changes
        $scope.$watch(function () {
            return vm.books.predicate + vm.books.reverse + JSON.stringify(vm.books.definitions);
        }, function () {
            vm.refresh();
        });
    }
]);