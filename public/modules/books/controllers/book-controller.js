// # book-controller.js
// _public/modules/books/controller/_

// Book Controller

/*global angular */
angular.module('books').controller('BookController', ['$scope', '$stateParams', '$location', 'socket', 'Authentication', 'book',
    function ($scope, $stateParams, $location, socket, Authentication, book) {
        'use strict';

        // Store a reference to this as `vm` or "view model" object.
        var vm = this;

        // Store a reference to the current book
        vm.book = book;

        // Store a reference to the auth service
        vm.authentication = Authentication;

        // Forward book event to scope
        socket.forward('book', $scope);

        // Watch for book event
        $scope.$on('socket:book', function (evt, book) {
            // When book event is sent and book matches current book, update status
            vm.book.merge(book);
        });
    }
]);