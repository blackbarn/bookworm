// # book-directive.js
// _public/modules/books/directives/_

// Book Directive

/*global angular */

angular.module('books').directive('bwBook', ['$location', '$log', 'Dialog', 'Book', function ($location, $log, Dialog, Book) {
    'use strict';

    return {
        restrict: 'A',
        replace: true,
        transclude: true,
        templateUrl: function (elem, attrs) {
            if (attrs.isSearch) {
                return 'modules/books/views/book.search.directive.view.html';
            } else {
                return 'modules/books/views/book.directive.view.html';
            }
        },
        scope: {
            book: '=',
            isCollapsed: '=',
            view: '=',
            isList: '='
        },
        link: function (scope) {
            scope.remove = function () {
                Dialog.confirmation('Remove Book? This will also remove it\'s releases.').then(function () {
                    scope.book.$destroy().$then(function () {
                        if (!scope.isList) {
                            $location.path('books');
                        }
                    });
                });
            };

            scope.add = function () {
                var book = Book.$build(scope.book);
                book.$save(function () {
                    $log.log('saved', book.title);
                }, function (err) {
                    $log.log('err saving', book.title);
                });
            };
        }
    };
}]);
