// # providers-service.js
// _public/modules/providers/service/_

// Providers Service

/*global angular */
angular.module('provider').factory('Provider', ['restmod', function (restmod) {
    'use strict';

    var model = restmod.model('/providers', 'DefinitionModel', 'SelectableModel', 'BulkModel', 'SortableModel', 'PagingModel', 'StatusModel', 'ViewTypeModel', 'FilterableModel', {
        PRIMARY_KEY: '_id',
        user: { hasOne: 'User'}
    });
    return model;
}]);

angular.module('provider').factory('BookSearchProvider', ['restmod', function (restmod) {
    'use strict';

    var model = restmod.model('/providers/books', 'DefinitionModel', 'ViewTypeModel', {
        PRIMARY_KEY: '_id'
    });
    return model;
}]);

angular.module('provider').factory('AuthorSearchProvider', ['restmod', function (restmod) {
    'use strict';

    var model = restmod.model('/providers/authors', 'DefinitionModel', 'ViewTypeModel', {
        PRIMARY_KEY: '_id'
    });
    return model;
}]);