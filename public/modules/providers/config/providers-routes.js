// # providers-routes.js
// _public/modules/providers/config/_

// Providers Routes Configuration

/*global angular */
//Setting up route
angular.module('provider').config(['$stateProvider',
    function ($stateProvider) {
        'use strict';
        // Providers state routing
        $stateProvider.
            state('providers', {
                url: '/providers',
                templateUrl: 'modules/providers/views/providers.view.html'
            }).
            state('provider', {
                url: '/providers/:providerId',
                templateUrl: 'modules/providers/views/provider.view.html'
            });
    }
]);