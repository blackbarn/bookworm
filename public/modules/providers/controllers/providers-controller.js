// # providers-controller.js
// _public/modules/providers/controller/_

// Providers Controller

/*global angular */
// Providers controller
angular.module('provider').controller('ProvidersController', ['$scope', '$stateParams', '$location', 'Authentication', 'Provider',
    function ($scope, $stateParams, $location, Authentication, Provider) {
        'use strict';
        /**
         * Store auth reference
         */
        $scope.authentication = Authentication;

        /**
         * Create Provider
         */
        $scope.create = function () {
            // Create new Provider object
            // TODO: add other fields
            var provider = new Provider({
                name: this.name
            });

            // Redirect after save
            provider.$save(function (response) {
                $location.path('providers/' + response._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });

            // Clear form fields
            this.name = '';
        };

        /**
         * Remove existing provider
         * @param {object} provider Provider Resource object to remove
         */
        $scope.remove = function (provider) {
            var i;
            if (provider) {
                provider.$remove();

                // find provider in current collection and splice it out
                for (i = 0; i < $scope.providers.length; i = i + 1) {
                    if ($scope.providers[i] === provider) {
                        $scope.providers.splice(i, 1);
                    }
                }
            } else {
                $scope.provider.$remove(function () {
                    $location.path('providers');
                });
            }
        };

        /**
         * Update existing provider
         */
        $scope.update = function () {
            var provider = $scope.provider;

            provider.$update(function () {
                $location.path('providers/' + provider._id);
            }, function (errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        /**
         * Find a list of providers
         */
        $scope.find = function () {
            $scope.providers = Provider.query();
        };

        /**
         * Find a single provider
         */
        $scope.findOne = function () {
            $scope.provider = Provider.get({
                providerId: $stateParams.providerId
            });
        };
    }
]);