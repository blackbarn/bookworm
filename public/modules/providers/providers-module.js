// # providers-module.js
// _public/modules/providers/_

// Providers Module

/*global ApplicationConfiguration */
// Use application configuration module to register a new module
ApplicationConfiguration.registerModule('provider');