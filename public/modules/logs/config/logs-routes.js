// # logs-routes.js
// _public/modules/logs/config/_

// Logs Routes Configuration

/*global angular */
//Setting up route
angular.module('logs').config(['$stateProvider',
    function ($stateProvider) {
        'use strict';
        // Books state routing
        $stateProvider.
            state('logs', {
                url: '/logs',
                templateUrl: 'modules/logs/views/logs.view.html',
                controller: 'LogsController',
                controllerAs: 'lc'
            });

    }
]);