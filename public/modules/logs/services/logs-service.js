// # logs-service.js
// _public/modules/logs/service/_

// Logs Service

/*global angular */


// Books Socket
angular.module('logs').factory('socket', ['socketFactory', function (socketFactory) {
    'use strict';
    return socketFactory();
}]);

// Log Model
angular.module('logs').factory('Log', ['$filter', '$log', 'restmod', function ($filter, $log, restmod) {
    'use strict';
    /**
     * @alias Log
     * @type {StaticApi}
     */
    var model = restmod.model('/logs', 'DefinitionModel', 'PagingModel', {
        PRIMARY_KEY: '_id'
    });

    return model;
}]);