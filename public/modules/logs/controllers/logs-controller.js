// # logs-controller.js
// _public/modules/logs/controller/_

// Logs Controller

/*global angular */
angular.module('logs').controller('LogsController', ['$scope', '$location', '$log', '$filter', 'socket', 'ngNotify', 'Authentication', 'Log',
    function ($scope, $location, $log, $filter, socket, ngNotify, Authentication, Log) {
        'use strict';

        var vm;

        // Store a reference to this as `vm` or "view model" object.
        vm = this;

        // Store auth reference
        vm.authentication = Authentication;

        // Forward log event to scope
        socket.forward('log', $scope);

        // Watch for book event
        $scope.$on('socket:log', function (evt, evLog) {
            if (evLog) {

                if ((vm.logs.length + 1) > vm.logs.perPage) {
                    vm.logs.pop();
                }
                vm.logs.unshift(evLog);
                vm.logs.total = vm.logs.total + 1;
            }
        });

        vm.logs = Log.$collection();

        vm.logs.perPage = 20;

        vm.refresh = function () {

            vm.logs.$refresh(angular.extend({
                page: vm.logs.page,
                perPage: vm.logs.perPage
            }, {})).$then(function (results) {
                vm.logs.total = (results.length === 0) ? vm.logs.page * vm.logs.perPage : (vm.logs.page * vm.logs.perPage) + 1;
            }, function (err) {
                ngNotify.set($filter('translate')('ERROR_RETRIEVING_LOGS', { error: err.status + ': ' + (err.data.message || err.statusText)}), {
                    type: 'error',
                    sticky: true
                });
            });
        };

        // Watch for page changes
        vm.pageChanged = function () {
            vm.refresh();
        };

        vm.refresh();
    }
]);