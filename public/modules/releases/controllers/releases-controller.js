// # releases-controller.js
// _public/modules/releases/controller/_

// Releases Controller

/*global angular */
angular.module('releases').controller('ReleasesController', ['$scope', '$location', '$stateParams', '$filter', 'socket', 'ngNotify', 'Authentication', 'Release',
    function ($scope, $location, $stateParams, $filter, socket, ngNotify, Authentication, Release) {
        'use strict';

        var vm;

        // Store a reference to this as `vm` or "view model" object.
        vm = this;

        // Store auth reference
        vm.authentication = Authentication;

        // Forward release event to scope
        socket.forward('release', $scope);

        // Watch for release event
        $scope.$on('socket:release', function (evt, evRelease) {
            // When release event is sent and release matches current release, update status
            angular.forEach(vm.releases, function (release) {
                release.merge(evRelease);
            });

        });

        vm.releases = Release.$collection();

        vm.releases.$defineProperties({
            'name': {
                isSortable: true,
                isFilterable: true
            },
            'status': {
                isSortable: true,
                isFilterable: true,
                values: ['available', 'wanted', 'downloaded', 'ignored', 'snatched']
            },
            'releaseDate': {
                label: 'release_date',
                isSortable: true,
                isFilterable: true
            }
        });

        // Set default predicate
        vm.releases.predicate = 'releaseDate';

        // Define data refresh method
        vm.refresh = function () {

            vm.releases.$refresh(angular.extend({
                page: vm.releases.page,
                perPage: vm.releases.perPage,
                sort: ((vm.releases.reverse) ? '-' : '') + vm.releases.predicate,
                expand: 'provider,book',
                book: $stateParams.bookId
            }, vm.releases.$getFilters())).$then(function (results) {
                // Set ng-table total value
                vm.releases.total = results.$response.headers('X-Total-Count');
            }, function (err) {
                ngNotify.set($filter('translate')('ERROR_RETRIEVING_RELEASES', { error: err.status + ': ' + (err.data.message || err.statusText)}), {
                    type: 'error',
                    sticky: true
                });
            });

        };

        // Watch for page changes
        vm.pageChanged = function () {
            vm.refresh();
        };

        // Watch for sorting changes
        $scope.$watch(function () {
            return vm.releases.predicate + vm.releases.reverse + JSON.stringify(vm.releases.definitions);
        }, function () {
            vm.refresh();
        });
    }
]);