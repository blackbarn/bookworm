// # release-controller.js
// _public/modules/releases/controller/_

// Release Controller

/*global angular */
angular.module('releases').controller('ReleaseController', ['$scope', '$stateParams', '$location', 'socket', 'Authentication', 'release',
    function ($scope, $stateParams, $location, socket, Authentication, release) {
        'use strict';

        // Store a reference to this as `vm` or "view model" object.
        var vm = this;

        // Store a reference to the current release
        vm.release = release;

        // Store a reference to the auth service
        vm.authentication = Authentication;

        // Forward release event to scope
        socket.forward('release', $scope);

        // Watch for release event
        $scope.$on('socket:release', function (evt, release) {
            vm.release.merge(release);
        });
    }
]);