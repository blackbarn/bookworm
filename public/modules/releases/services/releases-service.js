// # releases-service.js
// _public/modules/releases/service/_

// Releases Service

/*global angular */
// Release Model
angular.module('releases').factory('Release', ['$filter', '$log', 'restmod', 'ngNotify', function ($filter, $log, restmod, ngNotify) {
    'use strict';
    /**
     * @alias Book
     * @type {StaticApi}
     */
    var model = restmod.model('/releases', 'DefinitionModel', 'SelectableModel', 'BulkModel', 'SortableModel', 'PagingModel', 'StatusModel', 'ViewTypeModel', 'FilterableModel', {
        // Model property configuration
        PRIMARY_KEY: '_id',
        user: { hasOne: 'User', key: 'user', inline: true, mask: 'CU'},
        provider: { hasOne: 'Provider', key: 'provider', inline: true, mask: 'CU'},
        book: { belongsTo: 'Book', key: 'book', inline: true, mask: 'CU'},
        statuses: { mask: true, init: ['wanted', 'available', 'downloaded', 'ignored']},
        // Model custom methods

        /**
         * ## merge
         *
         * Merge one release data with this one
         * @param {Release} release Release to merge from
         */
        merge: function (release) {
            if (this._id === release._id) {
                this.status = release.status;
                this.directory = release.directory;
            }
        },

        /**
         * ## setStatus
         *
         * Set the status of the release and save
         *
         * @param {String} status The status to set
         */
        setStatus: function (status) {
            this.status = status;
            this.$save();
        },

        // Model Hooks

        /**
         * ## ~after-save hook
         */
        '~after-save': function () {
            ngNotify.set($filter('translate')('RELEASE_SAVED_SUCCESSFULLY', { name: this.name }), 'success');
        },
        /**
         * ## ~after-save-error hook
         * @param {Error} err Error object
         */
        '~after-save-error': function (err) {
            ngNotify.set($filter('translate')('ERROR_SAVING_RELEASE', { name: this.name, error: err.status + ': ' + (err.data.message || err.statusText) }), 'error');
        },
        /**
         * ## ~after-save-selected
         */
        '~after-save-selected': function () {
            ngNotify.set($filter('translate')('RELEASES_SAVED_SUCCESSFULLY', {}), 'success');
        },
        /**
         * ## ~after-save-selected-error
         * @param err
         */
        '~after-save-selected-error': function (err) {
            ngNotify.set($filter('translate')('ERROR_SAVING_RELEASES', {error: err.status + ': ' + (err.data.message || err.statusText) }), 'error');
        }
    });
    return model;
}]);

// Release Socket
angular.module('releases').factory('socket', ['socketFactory', function (socketFactory) {
    'use strict';
    return socketFactory();
}]);