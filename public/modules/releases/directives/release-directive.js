// # release-directive.js
// _public/modules/releases/directives/_

// Release Directive

/*global angular */

angular.module('releases').directive('bwRelease', ['$location', 'Dialog', function ($location, Dialog) {
    'use strict';

    return {
        restrict: 'A',
        replace: true,
        transclude: true,
        templateUrl: 'modules/releases/views/release.directive.view.html',
        scope: {
            release: '=',
            isCollapsed: '=',
            view: '=',
            isList: '='
        },
        link: function (scope) {
            scope.remove = function () {
                Dialog.confirmation('Remove Release?').then(function () {
                    scope.release.$destroy().$then(function () {
                        if (!scope.isList) {
                            $location.path('releases');
                        }
                    });
                });
            };
        }
    };
}]);
