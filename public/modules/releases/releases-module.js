// # releases-module.js
// _public/modules/releases/_

// Releases Module

/*global ApplicationConfiguration */
// Use application configuration module to register a new module
ApplicationConfiguration.registerModule('releases');