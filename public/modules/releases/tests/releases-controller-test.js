// # releases-controller-test.js
// _public/modules/releases/tests/_

// Releases Controller Tests

/*global ApplicationConfiguration */
(function () {
    'use strict';
    // Releases Controller Spec
    describe('Releases Controller Tests', function () {
        // Initialize global variables
        var ReleasesController,
            scope,
            $httpBackend,
            $stateParams,
            $location;

        // The $resource service augments the response object with methods for updating and deleting the resource.
        // If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
        // the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
        // When the toEqualData matcher compares two objects, it takes only object properties into
        // account and ignores methods.
        beforeEach(function () {
            jasmine.addMatchers({
                toEqualData: function () {
                    return {
                        compare: function (actual, expected) {
                            return {
                                pass: angular.equals(actual, expected)
                            };
                        }
                    };
                }
            });
        });

        // Then we can start by loading the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));

        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function ($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
            // Set a new global scope
            scope = $rootScope.$new();

            // Point global variables to injected services
            $stateParams = _$stateParams_;
            $httpBackend = _$httpBackend_;
            $location = _$location_;

            // Initialize the Releases controller.
            ReleasesController = $controller('ReleasesController', {
                $scope: scope
            });
            $httpBackend.expectGET(/languages\/[a-zA-Z-]+\.json/).respond({});
        }));

        it('$scope.find() should create an array with at least one Release object fetched from XHR', inject(function (Releases) {
            // Create sample Release using the Releases service
            var sampleRelease = new Releases({
                name: 'New Release'
            });

            // Create a sample Releases array that includes the new Release
            var sampleReleases = [sampleRelease];

            // Set GET response
            $httpBackend.expectGET('releases').respond(sampleReleases);

            // Run controller functionality
            scope.find();
            $httpBackend.flush();

            // Test scope value
            expect(scope.releases).toEqualData(sampleReleases);
        }));

        it('$scope.findOne() should create an array with one Release object fetched from XHR using a releaseId URL parameter', inject(function (Releases) {
            // Define a sample Release object
            var sampleRelease = new Releases({
                name: 'New Release'
            });

            // Set the URL parameter
            $stateParams.releaseId = '525a8422f6d0f87f0e407a33';

            // Set GET response
            $httpBackend.expectGET(/releases\/([0-9a-fA-F]{24})$/).respond(sampleRelease);

            // Run controller functionality
            scope.findOne();
            $httpBackend.flush();

            // Test scope value
            expect(scope.release).toEqualData(sampleRelease);
        }));

        it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function (Releases) {
            // Create a sample Release object
            var sampleReleasePostData = new Releases({
                name: 'New Release'
            });

            // Create a sample Release response
            var sampleReleaseResponse = new Releases({
                _id: '525cf20451979dea2c000001',
                name: 'New Release'
            });

            // Fixture mock form input values
            scope.name = 'New Release';

            // Set POST response
            $httpBackend.expectPOST('releases', sampleReleasePostData).respond(sampleReleaseResponse);

            // Run controller functionality
            scope.create();
            $httpBackend.flush();

            // Test form inputs are reset
            expect(scope.name).toEqual('');

            // Test URL redirection after the Release was created
            expect($location.path()).toBe('/releases/' + sampleReleaseResponse._id);
        }));

        it('$scope.update() should update a valid Release', inject(function (Releases) {
            // Define a sample Release put data
            var sampleReleasePutData = new Releases({
                _id: '525cf20451979dea2c000001',
                name: 'New Release'
            });

            // Mock Release in scope
            scope.release = sampleReleasePutData;

            // Set PUT response
            $httpBackend.expectPUT(/releases\/([0-9a-fA-F]{24})$/).respond();

            // Run controller functionality
            scope.update();
            $httpBackend.flush();

            // Test URL location to new object
            expect($location.path()).toBe('/releases/' + sampleReleasePutData._id);
        }));

        it('$scope.remove() should send a DELETE request with a valid releaseId and remove the Release from the scope', inject(function (Releases) {
            // Create new Release object
            var sampleRelease = new Releases({
                _id: '525a8422f6d0f87f0e407a33'
            });

            // Create new Releases array and include the Release
            scope.releases = [sampleRelease];

            // Set expected DELETE response
            $httpBackend.expectDELETE(/releases\/([0-9a-fA-F]{24})$/).respond(204);

            // Run controller functionality
            scope.remove(sampleRelease);
            $httpBackend.flush();

            // Test array after successful delete
            expect(scope.releases.length).toBe(0);
        }));
    });
}());