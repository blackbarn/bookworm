// # releases-routes.js
// _public/modules/releases/config/_

// Releases Routes Configuration

/*global angular */
//Setting up route
angular.module('releases').config(['$stateProvider',
    function ($stateProvider) {
        'use strict';
        // Releases state routing
        $stateProvider.
            state('releases', {
                url: '/releases',
                templateUrl: 'modules/releases/views/releases.view.html',
                abstract: true
            }).
            state('releases.list', {
                url: '',
                templateUrl: 'modules/releases/views/releases.table.view.html',
                controller: 'ReleasesController',
                controllerAs: 'rc'
            }).
            state('book.releases', {
                url: '/releases',
                templateUrl: 'modules/releases/views/releases.table.view.html',
                controller: 'ReleasesController',
                controllerAs: 'rc'
            }).
            state('release', {
                url: '/authors/:authorId/books/:bookId/releases/:releaseId',
                template: '<div data-bw-release data-release="rc.release" data-view="\'full\'" />',
                controller: 'ReleaseController',
                controllerAs: 'rc',
                resolve: {
                    release: ['Release', '$stateParams', function (Release, $stateParams) {
                        return Release.$new($stateParams.releaseId).$fetch({
                            expand: 'provider,book'
                        });
                    }]
                }
            });
    }
]);