// # authentication-controller-test.js
// _public/modules/users/tests/_

// Authentication Controller Tests

/*global ApplicationConfiguration */
(function () {
    'use strict';
    // Authentication controller Spec
    describe('AuthenticationController', function () {
        // Initialize global variables
        var AuthenticationController,
            $httpBackend,
            $stateParams,
            $location;

        beforeEach(function () {
            jasmine.addMatchers({
                toEqualData: function () {
                    return {
                        compare: function (actual, expected) {
                            return {
                                pass: angular.equals(actual, expected)
                            };
                        }
                    };
                }
            });
        });

        // Load the main application module
        beforeEach(module(ApplicationConfiguration.applicationModuleName));

        // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
        // This allows us to inject a service but then attach it to a variable
        // with the same name as the service.
        beforeEach(inject(function ($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
            // Point global variables to injected services
            $stateParams = _$stateParams_;
            $httpBackend = _$httpBackend_;
            $location = _$location_;

            // Initialize the Authentication controller
            AuthenticationController = $controller('AuthenticationController');
            $httpBackend.expectGET(/languages\/[a-zA-Z-]+\.json/).respond({});
        }));


        it('signin() should login with a correct user and password', function () {
            // test expected GET request
            $httpBackend.when('POST', '/auth/signin').respond(200, 'Fred');
            AuthenticationController.signin();
            $httpBackend.flush();
            expect(AuthenticationController.authentication.user).toEqual('Fred');
            expect($location.url()).toEqual('/');
        });


        it('signin() should fail to log in with nothing', function () {
            $httpBackend.expectPOST('/auth/signin').respond(400, {
                'message': 'Missing credentials'
            });
            AuthenticationController.signin();
            $httpBackend.flush();
            expect(AuthenticationController.error).toEqual('Missing credentials');
        });

        it('signin() should fail to log in with wrong credentials', function () {
            // Foo/Bar combo assumed to not exist
            AuthenticationController.authentication.user = 'Foo';
            AuthenticationController.credentials = 'Bar';
            $httpBackend.expectPOST('/auth/signin').respond(400, {
                'message': 'Unknown user'
            });
            AuthenticationController.signin();
            $httpBackend.flush();
            expect(AuthenticationController.error).toEqual('Unknown user');
        });

        it('signup() should register with correct data', function () {

            // test expected GET request
            AuthenticationController.authentication.user = 'Fred';
            $httpBackend.when('POST', '/auth/signup').respond(200, 'Fred');
            AuthenticationController.signup();
            $httpBackend.flush();
            expect(AuthenticationController.authentication.user).toBe('Fred');
            expect(AuthenticationController.error).toEqual(undefined);
            expect($location.url()).toBe('/');
        });

        it('signup() should fail to register with duplicate Username', function () {
            $httpBackend.when('POST', '/auth/signup').respond(400, {
                'message': 'Username already exists'
            });
            AuthenticationController.signup();
            $httpBackend.flush();
            expect(AuthenticationController.error).toBe('Username already exists');
        });


    });
}());