// # users-routes.js
// _public/modules/users/config/_

// Users Routes Configuration

/*global angular */
// Setting up route
angular.module('users').config(['$stateProvider',
    function ($stateProvider) {
        'use strict';
        // Users state routing
        $stateProvider.
            state('profile', {
                url: '/settings/profile',
                templateUrl: 'modules/users/views/settings/edit.profile.view.html'
            }).
            state('password', {
                url: '/settings/password',
                templateUrl: 'modules/users/views/settings/change.password.view.html'
            }).
            state('signup', {
                url: '/signup',
                templateUrl: 'modules/users/views/signup.view.html'
            }).
            state('signin', {
                url: '/signin',
                templateUrl: 'modules/users/views/signin.view.html'
            });
    }
]);