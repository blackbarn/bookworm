// # users-config.js
// _public/modules/users/config/_

// Users Configuration

/*global angular */
// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
    function ($httpProvider) {
        'use strict';
        // Set the httpProvider "not authorized" interceptor
        $httpProvider.interceptors.push(['$q', '$location', 'Authentication',
        function ($q, $location, Authentication) {
            return {
                responseError: function (rejection) {
                    switch (rejection.status) {
                    case 401:
                        // De-authenticate the global user
                        Authentication.user = null;

                        // Redirect to signin page
                        $location.path('signin');
                        break;
                    case 403:
                        // Add unauthorized behaviour
                        break;
                    }

                    return $q.reject(rejection);
                }
            };
        }
        ]);
    }
]);