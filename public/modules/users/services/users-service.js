// # users-service.js
// _public/modules/users/services/_

// Users Service

/*global angular */
// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
    function ($resource) {
        'use strict';
        return $resource('users', {}, {
            update: {
                method: 'PUT'
            }
        });
    }
]);

angular.module('users').factory('User', ['restmod', function (restmod) {
    'use strict';

    var model = restmod.model('/users', {
        PRIMARY_KEY: '_id'
    });

    return model;
}]);