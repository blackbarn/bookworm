// # authentication-service.js
// _public/modules/users/services/_

// Authentication Service

/*global angular */
// Authentication service for user variables
angular.module('users').factory('Authentication', [
    function () {
        'use strict';
        var _this = this;

        _this._data = {
            user: window.user
        };

        return _this._data;
    }
]);