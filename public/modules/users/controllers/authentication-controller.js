// # authentication-controller.js
// _public/modules/users/controllers/_

// Authentication Controller

/*global angular */
angular.module('users').controller('AuthenticationController', ['$http', '$location', 'Authentication',
    function ($http, $location, Authentication) {
        'use strict';
        var vm = this;

        // Authentication Service Reference
        this.authentication = Authentication;

        // If user is signed in then redirect back home
        if (this.authentication.user) {
            $location.path('/');
        }

        /**
         * Sign up
         */
        this.signup = function () {
            $http.post('/auth/signup', this.credentials).success(function (response) {
                //If successful we assign the response to the global user model
                vm.authentication.user = response;

                //And redirect to the index page
                $location.path('/');
            }).error(function (response) {
                vm.error = response.message;
            });
        };

        /**
         * Sign in
         */
        this.signin = function () {
            $http.post('/auth/signin', this.credentials).success(function (response) {
                //If successful we assign the response to the global user model
                vm.authentication.user = response;

                //And redirect to the index page
                $location.path('/');
            }).error(function (response) {
                vm.error = response.message;
            });
        };
    }
]);