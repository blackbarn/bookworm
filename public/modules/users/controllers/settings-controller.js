// # settings-controller.js
// _public/modules/users/controllers/_

// Settings Controller

/*global angular */
angular.module('users').controller('SettingsController', ['$http', '$location', 'Users', 'Authentication',
    function ($http, $location, Users, Authentication) {
        'use strict';
        /**
         * Store User reference
         * @type {object}
         */
        this.user = Authentication.user;

        // If user is not signed in then redirect back home
        if (!this.user) {
            $location.path('/');
        }

        /**
         * Update a user profile
         */
        this.updateUserProfile = function () {
            this.success = this.error = null;
            var user = new Users(this.user);

            user.$update(function (response) {
                this.success = true;
                Authentication.user = response;
            }.bind(this), function (response) {
                this.error = response.data.message;
            }.bind(this));
        };

        /**
         * Change user password
         */
        this.changeUserPassword = function () {
            this.success = this.error = null;

            $http.post('/users/password', this.passwordDetails).success(function () {
                // If successful show success message and clear form
                this.success = true;
                this.passwordDetails = null;
            }.bind(this)).error(function (response) {
                this.error = response.message;
            }.bind(this));
        };
    }
]);