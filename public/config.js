// # config.js
// _public/_

// Application Config

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function () {
    'use strict';
	// Init module configuration options
	var applicationModuleName = 'bookworm';
	var applicationModuleVendorDependencies = ['ngResource', 'ui.router', 'ui.bootstrap', 'ui.utils',
        'pascalprecht.translate', 'ngNotify', 'btford.socket-io', 'restmod', 'truncate',
        'ngAnimate', 'angularMoment'];

	// Add a new vertical module
	var registerModule = function (moduleName) {
		// Create angular module
		angular.module(moduleName, []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();