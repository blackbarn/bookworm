// # express.js
// _config/_

// Configure the Express Application
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var express = require('express'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    compress = require('compression'),
    methodOverride = require('method-override'),
    cookieParser = require('cookie-parser'),
    helmet = require('helmet'),
    passport = require('passport'),
    swagger = require('swagger-express'),
    mongoStore = require('connect-mongo')({
        session: session
    }),
    fs = require('fs'),
    flash = require('connect-flash'),
    config = require('./config'),
    partialResponse = require('./middleware/partial-response'),
    paging = require('./middleware/query-pagination'),
    sorting = require('./middleware/query-sorting'),
    expanding = require('./middleware/query-expand'),
    consolidate = require('consolidate'),
    path = require('path'),
    logger = require('../app/providers/log/logger').get('Express'),
    APIError = require('../app/errors/api-error');


// Exported as a function, so be sure to pass in the database instance.
module.exports = function (db) {
    // Initialize express app
    var app, swaggerApiYml;

    app = express();

    // Globbing model files
    config.getGlobbedFiles('./app/models/*.js').forEach(function (modelPath) {
        require(path.resolve(modelPath))(app);
    });

    // Setting application local variables
    app.locals.title = config.app.title;
    app.locals.description = config.app.description;
    app.locals.keywords = config.app.keywords;
    app.locals.jsFiles = config.getJavaScriptAssets();
    app.locals.cssFiles = config.getCSSAssets();

    // Passing the request url to environment locals
    app.use(function (req, res, next) {
        res.locals.url = req.protocol + ':// ' + req.headers.host + req.url;
        next();
    });

    // Should be placed before express.static
    app.use(compress({
        filter: function (req, res) {
            return (/json|text|javascript|css/).test(res.getHeader('Content-Type'));
        },
        level: 9
    }));

    // Showing stack errors
    app.set('showStackError', true);

    // Set swig as the template engine
    app.engine('html', consolidate[config.templateEngine]);

    // Set views path and view engine
    app.set('view engine', 'html');
    app.set('views', './app/views');

    // Environment dependent middleware
    if (process.env.NODE_ENV === 'development') {
        // Enable logger (morgan)
        app.use(morgan('dev'));

        // Disable views cache
        app.set('view cache', false);
    } else if (process.env.NODE_ENV === 'production') {
        app.locals.cache = 'memory';
    }

    // Request body parsing middleware should be above methodOverride
    app.use(bodyParser.urlencoded());
    app.use(bodyParser.json());
    app.use(methodOverride());

    // Enable jsonp
    app.enable('jsonp callback');

    // CookieParser should be above session
    app.use(cookieParser());

    // Express MongoDB session storage
    app.use(session({
        secret: config.sessionSecret,
        store: new mongoStore({
            db: db.connection.db,
            collection: config.sessionCollection
        })
    }));

    // Use partial response middleware.
    // Do not run partial response when it contains code and message properties.
    app.use(partialResponse({ignoreWhenContains: 'stack'}));

    // Use pagination middleware
    app.use(paging({}));

    // Use sorting middleware
    app.use(sorting({}));

    // Use expanding middleware
    app.use(expanding({}));

    // Use passport session
    app.use(passport.initialize());
    app.use(passport.session());

    // Connect flash for flash messages
    app.use(flash());

    // Use helmet to secure Express headers
    app.use(helmet.xframe());
    app.use(helmet.iexss());
    app.use(helmet.contentTypeOptions());
    app.use(helmet.ienoopen());
    app.disable('x-powered-by');

    // Setting the app router and static folder
    app.use(express.static(path.resolve('./public')));

    // Fetch file names
    swaggerApiYml = fs.readdirSync('./tmp/swagger/');

    // Build path
    swaggerApiYml = swaggerApiYml.map(function (file) {
        return './tmp/swagger/' + file;
    });
    // Setting up swagger
    app.use(swagger.init(app, {
        apiVersion: config.api.version,
        swaggerVersion: '1.0',
        swaggerURL: '/swagger',
        swaggerJSON: '/api-docs',
        swaggerUI: './public/lib/swagger-ui/dist',
        basePath: config.api.basePath,
        info: {
            title: config.api.title,
            description: config.api.description
        },
        apis: swaggerApiYml
    }));

    // Globbing routing files
    config.getGlobbedFiles('./app/routes/**/*.js').forEach(function (routePath) {
        require(path.resolve(routePath))(app);
    });

    // log errors
    app.use(function (err, req, res, next) {
        logger.log('error', logger.errorAsJSON(err));
        next(err);
    });

    // Respond with error
    app.use(function (err, req, res, next) {
        if (req.xhr || req.accepts('application/json')) {
            if (!(err instanceof APIError)) {
                err = new APIError(err);
            }
            res.json(err.statusCode || err.status || 500, (typeof err.toJSON === 'function') ? err.toJSON() : err);
        } else {
            res.status(err.statusCode || 500).render('500', {
                error: err.stack
            });
        }
    });

    // Assume 404 since no middleware responded
    app.use(function (req, res) {
        res.status(404).render('404', {
            url: req.originalUrl,
            error: 'Not Found'
        });
    });

    return app;
};
