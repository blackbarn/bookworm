// # query-sorting.js
// _config/middleware/_

// Sorting Middleware
// Parses sorting query params into separate provider sort conventions with defaults.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var cast = require('../../app/util/cast');
var _ = require('lodash');

/**
 * @name SortingOptions
 * @property {String} googleBooksDefault The default sort for google books.
 * @property {String} mongoDefault The default sort for mongo.
 */

/**
 * Export sorting middleware
 *
 * @param {SortingOptions} options Options for middleware
 * @returns {Function}
 */
module.exports = function (options) {

    options = options || {};

    options = _.defaults(options, {
        googleBooksDefault: 'relevance',
        mongoDefault: '-created'
    });

    /**
     * ## Middleware Function
     *
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     * @param {Function} next Middleware callback
     */
    return function (req, res, next) {
        if (!req._sorting) {
            // Define sorting properties
            req._sorting = {
                sort: cast.string(req.query.sort, ''),
                sortMongo: cast.string(req.query.sort, options.mongoDefault).replace(',', ' '),
                sortGoogleBooks: cast.string(req.query.sort, options.googleBooksDefault)
            };
        }
        next();
    };
};
