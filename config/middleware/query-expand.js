// # query-expand.js
// _config/middleware/_

// Expand Middleware
// Parses an `expand` query param into a format that can be used with MongoDB's populate.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var cast = require('../../app/util/cast');
var _ = require('lodash');
var url = require('url');

/**
 * @name ExpandOptions
 * @property {Object} force Force populate, overrides any query.
 */

/**
 * Export Expand middleware
 *
 * @param {ExpandOptions} options Options for middleware
 * @returns {Function}
 */
module.exports = function (options) {

    options = options || {};

    options = _.defaults(options, {
        force: {
            user: {
                path: 'user',
                select: 'username'
            }
        }
    });

    /**
     * ## Middleware Function
     *
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     * @param {Function} next Middleware callback
     */
    return function (req, res, next) {
        var expandParts, expandMap, expand;

        if (!req._expanding) {

            expand = cast.string(req.query.expand, '');

            expandParts = expand.split(',');

            // Helper map
            expandMap = options.force;

            // Iterate expand items
            _.forEach(expandParts, function (part) {
                var subParts, subPartParent, subPartChild;
                // Check for child identifiers. user.username for example
                if (part.indexOf('.') !== -1) {
                    // Split it up
                    subParts = part.split('.');
                    // Save references
                    subPartParent = subParts[0];
                    subPartChild = subParts[1];
                    // Grab existing or set default map object
                    expandMap[subPartParent] = expandMap[subPartParent] || {};
                    // Set the path
                    expandMap[subPartParent].path = subPartParent;

                    // When the identifier exists in the force config, use its select value.
                    if (options.force[subPartParent]) {
                        expandMap[subPartParent].select = options.force[subPartParent].select;
                    } else {
                        // Append to existing select value
                        expandMap[subPartParent].select = (expandMap[subPartParent].select || '') + ' ' + subPartChild;
                    }
                } else {
                    // No children, top level only.
                    // Create object and set path
                    expandMap[part] = expandMap[part] || {};
                    expandMap[part].path = part;

                    // Use forceConfig if identifier exists, otherwise use any existing select in the map.
                    if (options.force[part]) {
                        expandMap[part].select = options.force[part].select;
                    } else {
                        expandMap[part].select = expandMap[part].select || '';
                    }

                }
            });

            // Set data
            req._expanding = {
                populate: _.toArray(expandMap)
            };
        }
        next();
    };
};
