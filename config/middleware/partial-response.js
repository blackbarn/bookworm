// # partial-response.js
// _config/middleware/_

// Partial Response Express Middleware
// Runs JSON through `json-mask` before responding to the request.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var jsonMask = require('json-mask');

/**
 * @name PartialResponseOptions
 * @property {String} ignoreWhenContains When the object contains these properties, do not execute partial response.
 */

/**
 * Export partialResponse middleware
 *
 * @param {PartialResponseOptions} options Options for middleware
 * @returns {Function}
 */
module.exports = function (options) {
    options = options || {};

    /**
     * ## partialResponse
     *
     * Take an object and filter it creating a partial.
     *
     * @param {Object} obj object to filter
     * @param {String} fields json-mask compliant filter string.
     * @returns {Object}
     */
    function partialResponse(obj, fields) {
        // If no fields, don't filter. Otherwise filter the object with the fields.
        if (!fields) {
            return obj;
        } else if (options.ignoreWhenContains) {
            if (jsonMask(obj, options.ignoreWhenContains) === null) {
                return jsonMask(obj, fields);
            } else {
                return obj;
            }
        } else {
            return jsonMask(obj, fields);
        }
    }

    /**
     * ## wrap
     *
     * Wrap the original function with additional logic.
     *
     * @param {Function} original Original function, e.g., res.json
     * @returns {Function}
     */
    function wrap(original) {
        // returning a wrapped function.
        return function () {
            // get the query param that contains the filter list.
            var param = this.req.query[options.query || 'fields'];

            // If only one argument, user is not specifying status code along with data.
            if (arguments.length === 1) {
                // Call the original function with the filtered data (first arg)
                original(partialResponse(arguments[0], param));
            } else if (arguments.length === 2) {
                // We have two arguments, user is sending status code along with data, handle accordingly.
                if (typeof arguments[1] === 'number') {
                    original(arguments[1], partialResponse(arguments[0], param));
                } else {
                    original(arguments[0], partialResponse(arguments[1], param));
                }
            }
        };
    }

    /**
     * ## Middleware Function
     *
     * Middleware function which masks the json/jsonp
     */
    return function (req, res, next) {
        // When not already wrapped
        if (!res.__isJSONMaskWrapped) {
            // Handle json
            res.json = wrap(res.json.bind(res));
            // Handle jsonp
            res.jsonp = wrap(res.jsonp.bind(res));
            // Mark that we've wrapped the response
            res.__isJSONMaskWrapped = true;
        }
        next();
    };
};
