// # query-pagination.js
// _config/middleware/_

// Pagination Middleware
// Parses pagination query parameters and populates useful properties and exposes methods to set
// response pagination headers.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var cast = require('../../app/util/cast');
var _ = require('lodash');
var url = require('url');

/**
 * @name PaginationOptions
 * @property {Number} defaultPage The default page number
 * @property {Number} defaultPerPage The default per-page number.
 */

/**
 * Export pagination middleware
 *
 * @param {PaginationOptions} options Options for middleware
 * @returns {Function}
 */
module.exports = function (options) {
    options = options || {};

    options = _.defaults(options, {
        defaultPage: 1,
        defaultPerPage: 10
    });


    /**
     * ## Middleware Function
     *
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     * @param {Function} next Middleware callback
     */
    return function (req, res, next) {
        var page, perPage, parsedUrl;
        // When not already paginated
        if (!req._pagination) {

            // Parse the URL
            parsedUrl = url.parse(req.url, true);

            // Define page, perPage and sort with defaults
            page = cast.int(req.query.page, 10, options.defaultPage);
            perPage = cast.int(req.query.perPage, 10, options.defaultPerPage);

            // Define pagination properties
            req._pagination = {
                skip: (page * perPage) - perPage,
                limit: perPage,
                page: page,
                perPage: perPage
            };

            /**
             * Set pagination values for the response
             * @param {Number} totalPages Total number of pages possible for the query
             * @param {Number} totalItems Total items returned by the query
             */
            res.setPaginationValues = function (totalPages, totalItems) {
                var links = {}, nextPage, previousPage, firstPage, lastPage;

                // Remove the `search` property so that `format` uses the `query` property when formatting.
                delete parsedUrl.search;

                res.setHeader('X-Total-Count', totalItems);
                res.setHeader('X-Total-Pages', totalPages);

                // Calculate pages
                firstPage = 1;
                previousPage = ((page - 1) < 1) ? 1 : (page - 1);
                nextPage = (((page + 1) > totalPages) ? totalPages : (page + 1));
                lastPage = totalPages;

                // Define first link
                links.first = url.format(_.merge(parsedUrl, {
                    query: {
                        page: firstPage,
                        perPage: perPage
                    }
                }));

                // Define last link
                links.last = url.format(_.merge(parsedUrl, {
                    query: {
                        page: totalPages,
                        perPage: perPage
                    }
                }));

                // Define previous link when applicable
                if (page > firstPage) {
                    links.prev = url.format(_.merge(parsedUrl, {
                        query: {
                            page: previousPage,
                            perPage: perPage
                        }
                    }));
                }

                // Define next link when applicable
                if (page < lastPage) {
                    links.next = url.format(_.merge(parsedUrl, {
                        query: {
                            page: nextPage,
                            perPage: perPage
                        }
                    }));
                }

                // Set `Link` header for paging.
                res.links(links);
            };
        }
        next();
    };
};
