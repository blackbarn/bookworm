// # local-apikey.js
// _config/strategies_

// Passport Local API Key Strategy
// Authenticates users given an API Key associated with a user.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var passport = require('passport'),
    LocalAPIKeyStrategy = require('passport-localapikey').Strategy,
    User = require('mongoose').model('User');

module.exports = function () {
    // Use local strategy
    passport.use(new LocalAPIKeyStrategy({
        apiKeyField: 'api_key'
    }, function (apikey, done) {
            User.findOne({
                apikey: apikey
            }, function (err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, {
                        message: 'Invalid API Key'
                    });
                }
                return done(null, user);
            });
        }
    ));
};