// # init.js
// _config/_

// Initializer for Configuration
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var glob, colors;
glob = require('glob');
colors = require('colors');

module.exports = function () {

    // Before we begin, lets set the environment variable.
    // We'll Look for a valid NODE_ENV variable and if one cannot be found load the development NODE_ENV

    glob('./config/env/' + process.env.NODE_ENV + '.js', {
        sync: true
    }, function (err, environmentFiles) {
        console.log();
        if (!environmentFiles.length) {
            if (process.env.NODE_ENV) {
                console.log(('No configuration file found for "' + process.env.NODE_ENV + '" environment using development instead').yellow);
            } else {
                console.log('NODE_ENV is not defined! Using default development environment'.red);
            }
            process.env.NODE_ENV = 'development';
        } else {
            console.log(('Application loaded using the "' + process.env.NODE_ENV + '" environment configuration').inverse);
        }
        console.log();
    });
};