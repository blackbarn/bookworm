// # all.js
// _config/env/_

// All Environments Config
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var pkg = require('../../package.json'),
    os = require('os');

// Export the config
module.exports = {
    app: {
        title: 'Bookworm',
        description: 'Full-Stack JavaScript with MongoDB, Express, AngularJS, and Node.js',
        keywords: 'MongoDB, Express, AngularJS, Node.js'
    },
    logging: {
        console: {
            enabled: true,
            level: 'info'
        },
        mongo: {
            enabled: true,
            level: 'info'
        }
    },
    port: process.env.PORT || 3000,
    templateEngine: 'swig',
    sessionSecret: 'bookworm',
    sessionCollection: 'sessions',
    userAgent: 'Bookworm/' + pkg.version.replace(' ', '-') + ' (' + os.platform() + ' ' + os.release() + ')',
    api: {
        version: '1.0',
        title: 'Bookworm API',
        description: 'The API of Bookworm. If anything is missing, create a ticket!',
        basePath: 'http://localhost:' + (process.env.PORT || 3000)
    },
    assets: {
        lib: {
            css: [
                'public/lib/bootstrap/dist/css/bootstrap.css',
                'public/lib/bootstrap/dist/css/bootstrap-theme.css',
                'public/lib/ng-notify/dist/ng-notify.min.css',
                'public/lib/ng-table/ng-table.css',
                'public/lib/trNgGrid/release/trNgGrid.min.css',
                'public/lib/angular-loading-bar/build/loading-bar.css'
            ],
            js: [
                'public/lib/lodash/dist/lodash.js',
                'public/lib/jquery/dist/jquery.js',
                'public/lib/bootstrap/dist/js/bootstrap.js',
                'public/lib/socket.io-client/socket.io.js',
                'public/lib/moment/min/moment-with-locales.js',
                'public/lib/angular/angular.js',
                'public/lib/angular-animate/angular-animate.js',
                'public/lib/angular-resource/angular-resource.js',
                'public/lib/angular-restmod/dist/angular-restmod-bundle.js',
                'public/lib/angular-ui-router/release/angular-ui-router.js',
                'public/lib/angular-ui-utils/ui-utils.js',
                'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                'public/lib/angular-translate/angular-translate.js',
                'public/lib/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
                'public/lib/ng-notify/dist/ng-notify.min.js',
                'public/lib/angular-socket-io/socket.js',
                'public/lib/angular-truncate/src/truncate.js',
                'public/lib/angular-loading-bar/build/loading-bar.js',
                'public/lib/angular-moment/angular-moment.js'

            ]
        },
        css: [
            'public/modules/**/css/*.css'
        ],
        js: [
            'public/config.js',
            'public/application.js',
            'public/modules/*/*.js',
            'public/modules/*/*[!tests]*/*.js'
        ],
        tests: [
            'public/lib/angular-mocks/angular-mocks.js',
            'public/modules/*/tests/*.js'
        ]
    }
};