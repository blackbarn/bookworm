// # development.js
// _config/env/_

// Development Environments Config
'use strict';

// Export the config
module.exports = {
    db: 'mongodb://localhost/bookworm-dev',
    app: {
        title: 'Bookworm - Development Environment'
    },
    logging: {
        console: {
            enabled: true,
            level: 'debug'
        },
        mongo: {
            enabled: true,
            level: 'debug'
        }
    }
};