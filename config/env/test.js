// # test.js
// _config/env/_

// Test Environments Config
'use strict';

// Export the config
module.exports = {
    db: 'mongodb://localhost/bookworm-test',
    port: 3001,
    app: {
        title: 'Bookworm - Test Environment'
    }
};