// # config.js
// _config/_

// Configuration Utilities
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var _, colors, glob, all, env, envCustom, conf;
_ = require('lodash');
colors = require('colors');
glob = require('glob');
all = require('./env/all');
env = require('./env/' + process.env.NODE_ENV);

// Try and grab a environment specific configuration, it will fail if it does not exist or is invalid.
try {
    // Grab custom JSON environment specific config (user provided)
    envCustom = require('./env-custom/' + process.env.NODE_ENV + '.json');
} catch (e) {
    // Either the file did not exist, or it contained errors, let 'em know.
    envCustom = {};
    console.log(('[Notice] No valid custom configuration file found for environment "' + process.env.NODE_ENV + '"').blue);
    console.log();
}

// Merge environment with all
conf = _.merge(all, env);
// Merge custom environment with the rest.
conf = _.merge(conf, envCustom);

// Load app configurations
module.exports = conf;

/**
 * ## getGlobbedFiles
 *
 * Get files by glob patterns
 *
 * @param {String} globPatterns Glob Patterns
 * @param {String} [removeRoot] Remove this path
 */
module.exports.getGlobbedFiles = function (globPatterns, removeRoot) {
    // For context switching
    var _this = this;

    // URL paths regex
    var urlRegex = new RegExp('^(?:[a-z]+:)?//', 'i');

    // The output array
    var output = [];

    // If the glob pattern is an array, we use each pattern in a recursive way, otherwise we use glob
    if (_.isArray(globPatterns)) {
        globPatterns.forEach(function (globPattern) {
            output = _.union(output, _this.getGlobbedFiles(globPattern, removeRoot));
        });
    } else if (_.isString(globPatterns)) {
        if (urlRegex.test(globPatterns)) {
            output.push(globPatterns);
        } else {
            glob(globPatterns, {
                sync: true
            }, function (err, files) {
                if (removeRoot) {
                    files = files.map(function (file) {
                        return file.replace(removeRoot, '');
                    });
                }

                output = _.union(output, files);
            });
        }
    }

    return output;
};

/**
 * ## getJavaScriptAssets
 *
 * Get the modules JavaScript files
 *
 * @param {Boolean} [includeTests] Include Tests?
 */
module.exports.getJavaScriptAssets = function (includeTests) {
    var output = this.getGlobbedFiles(this.assets.lib.js.concat(this.assets.js), 'public/');

    // To include tests
    if (includeTests) {
        output = _.union(output, this.getGlobbedFiles(this.assets.tests));
    }

    return output;
};

/**
 * ## getCSSAssets
 *
 * Get the modules CSS files
 */
module.exports.getCSSAssets = function () {
    return this.getGlobbedFiles(this.assets.lib.css.concat(this.assets.css), 'public/');
};