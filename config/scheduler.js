// # scheduler.js
// _config/_

// Scheduler Configuration
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var Provider = require('mongoose').model('Provider'),
    logger = require('../app/providers/log/logger').get('Scheduler'),
    _ = require('lodash'),
    config = require('./config');

module.exports = function () {

    Provider.getProvidersByType('processor', true)
        .then(function (providers) {
            providers.forEach(function (provider) {
                provider.initializeSchedule();
            });
        });
};