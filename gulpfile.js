// # gulpfile
//
// Project gulpfile for handling build automation.
//
// ## Usage
// ```sh
// gulp <task>
// ```
// ## Examples
// ```sh
// $ gulp
// $ gulp build
// ```
// ## Help
// Simply run
// ```
// $ gulp help
// ``
// for a list of tasks.

// Now on with the show!

'use strict';
// Module Dependencies
var path = require('path');
var argv = require('minimist')(process.argv.slice(2));
var gulp = require('gulp');
var fs = require('fs');
var cp = require('child_process');
JSON.minify = JSON.minify ||  require('node-json-minify');

// Gulp Plugins
var istanbul = require('gulp-istanbul');
var plato = require('gulp-plato');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var header = require('gulp-header');
var jshint = require('gulp-jshint');
var csslint = require('gulp-csslint');
var cssmin = require('gulp-cssmin');
var clean = require('gulp-clean');
var nodemon = require('gulp-nodemon');
var bump = require('gulp-bump');
var karma = require('gulp-karma');
var mocha = require('gulp-mocha');
var runSequence = require('gulp-run-sequence');
var taskListing = require('gulp-task-listing');

// Resource/Config Files
var pkg = require('./package.json');
var init = require('./config/init')();
var config = require('./config/config');

// JavaScript and CSS header banner
var banner = ['/**',
    ' * ${pkg.name} - ${pkg.description}',
    ' * @version v${pkg.version}',
    ' * @link ${pkg.homepage}',
    ' * @license ${pkg.license}',
    ' */'].join('\n');

/**
 * ## Lint Tasks
 *
 * Tasks for linting JavaScript and CSS utilizing `rc` files.
 */

// Lint Client JavaScript
gulp.task('lint:client:js', function () {
    return gulp.src(config.assets.js)
        .pipe(jshint('./public/.jshintrc'))
        .pipe(jshint.reporter('default'))
        .pipe(jshint.reporter('fail'));
});

// Lint Client JavaScript Tests
gulp.task('lint:client:tests', function () {
    return gulp.src(['public/modules/*/tests/*.js'])
        .pipe(jshint('./public/.jshintrc'))
        .pipe(jshint.reporter('default'))
        .pipe(jshint.reporter('fail'));
});

// Lint Client CSS
gulp.task('lint:client:css', function () {
    return gulp.src(config.assets.css)
        .pipe(csslint('.csslintrc'))
        .pipe(csslint.reporter());
});

// Lint Server JavaScript
gulp.task('lint:server:js', function () {
    return gulp.src(['gulpfile.js', 'server.js', 'config/**/*.js', 'app/**/*.js', '!app/tests/**/*.js'])
        .pipe(jshint('./.jshintrc'))
        .pipe(jshint.reporter('default'))
        .pipe(jshint.reporter('fail'));
});

// Lint Server JavaScript Tests
gulp.task('lint:server:tests', function () {
    return gulp.src(['app/tests/**/*.js'])
        .pipe(jshint('./app/tests/.jshintrc'))
        .pipe(jshint.reporter('default'))
        .pipe(jshint.reporter('fail'));
});

// Group Task for Linting them all
gulp.task('lint', ['lint:client:js', 'lint:client:tests', 'lint:client:css', 'lint:server:js', 'lint:server:tests']);

/**
 * ## Compacting Tasks
 *
 * Tasks for compacting JavaScript and CSS (compress/obfuscate).
 */

// Compact Client JavaScript.
// All client JavaScript is compacted into `application.min.js`.
gulp.task('compact:js', function () {
    return gulp.src(config.assets.js)
        .pipe(uglify())
        .pipe(concat('application.min.js'))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest('public/dist/'));
});

// Compact Client CSS.
// All client CSS is compacted into `application.min.css`.
gulp.task('compact:css', function () {
    return gulp.src(config.assets.css)
        .pipe(cssmin())
        .pipe(concat('application.min.css'))
        .pipe(header(banner, {pkg: pkg}))
        .pipe(gulp.dest('public/dist/'));
});

// Group Task for Compacting them all.
gulp.task('compact', ['compact:js', 'compact:css']);


/**
 * ## Testing Tasks
 *
 * Tasks for testing client JavaScript and CSS as well as server JavaScript.
 */

// Test Client JavaScript.
// Also runs client `plato` task.
gulp.task('test:client', ['plato:client'], function () {
    return gulp.src(config.assets.lib.js.concat(config.assets.js, config.assets.tests), {read: false})
        .pipe(karma({
            configFile: 'karma.conf.js',
            action: 'run'
        }))
        .on('error', function () {
            console.log('\x1b[31m', 'Exiting. At least one Karma test failed!', '\x1b[0m');
            process.exit();
        });
});

// Test Server JavaScript.
// Also runs server `plato` task.
gulp.task('test:server', ['plato:server'], function () {
    // Requires `server.js` first to init the server/db instance.
    // TODO: see if there is a way to *not* do this.
    //require('server.js');
    // Grab server related JavaScript (except tests)
    gulp.src(['app/**/*.js', 'config/**/*.js', '!app/tests/**'])
        // Send them through `istanbul`
        .pipe(istanbul())
        .on('finish', function () {
            // Now that the files are instrumented, run the mocha tests
            gulp.src(['app/tests/**/*.js'])
                .pipe(mocha())
                .pipe(istanbul.writeReports({
                    dir: './reports/server/coverage',
                    reportOpts: { dir: './reports/server/coverage'}
                }))
                .once('end', function () {
                    process.exit();
                });
        });
});

// Group test task.
// Only allows being run when `NODE_ENV=test` to ensure proper environment.
gulp.task('test', function (done) {
    if (process.env.NODE_ENV === 'test') {
        runSequence('test:client', 'test:server', done);
    } else {
        console.log('\x1b[31m', 'NODE_ENV is not set to test! Please use NOD_ENV=test gulp test', '\x1b[0m');
    }

});

/**
 * ## Reporting, Analysis and Documentation Tasks
 *
 * Tasks for running report related tasks. This includes static analysis and documentation generation.
 *
 */

// Plato Static Analysis for server JavaScript.
// Outputs to `reports/server/plato`.
gulp.task('plato:server', function () {
    return gulp.src(['gulpfile.js', 'server.js', 'config/**/*.js', 'app/**/*.js', '!app/tests/**/*.js'])
        .pipe(plato('reports/server/plato', {
            jshint: {
                options: JSON.parse(JSON.minify(fs.readFileSync('./.jshintrc', 'utf8')))
            },
            complexity: {
                trycatch: true
            }
        }));
});

// Plato Static Analysis for client JavaScript.
// Outputs to `reports/client/plato`.
gulp.task('plato:client', function () {
    return gulp.src(config.assets.js)
        .pipe(plato('reports/client/plato', {
            jshint: {
                options: JSON.parse(JSON.minify(fs.readFileSync('public/.jshintrc', 'utf8')))
            },
            complexity: {
                trycatch: true
            }
        }));
});

// Group task for `plato`.
gulp.task('plato', ['plato:client', 'plato:server']);

// Docker documentation generation.
// Utilizes `child_process` `exec` to run the local node module until a reliable gulp plugin exists.
// Pass in `--dockerWatch` to keep docker watching files for changes.
// ```sh
// gulp docker --dockerWatch
// ```
gulp.task('docker', function (done) {

    var binPath, dockerArgs;
    binPath = './node_modules/.bin/docker ';
    /** @namespace argv.dockerWatch*/
    dockerArgs = [
        // Show numbers
        '-n',
        // Ignore hidden files
        '-I',
        // Only update changed files
        '-u',
        // Exclude these paths
        '-x node_modules,public/lib,reports,doc',
        // Define extras
        '--extras fileSearch',
        // Optional watch flag
        ((argv.dockerWatch) ? '-w': '')
    ];
    cp.exec(binPath + dockerArgs.join(' ')).on('exit', done);
});

/**
 * ## Miscellaneous Tasks
 *
 * Other tasks that don't fit into a particular category.
 */

// Compile swagger api
gulp.task('swagger-api', function () {
    var files = fs.readdirSync('config/swagger-api/');

    files.forEach(function (model) {
        if (model !== 'models.yml') {
            gulp.src(['config/swagger-api/' + model, 'config/swagger-api/models.yml'])
                .pipe(concat(model))
                .pipe(gulp.dest('tmp/swagger'));
        }
    });
});

// Clean Generated Directories.
gulp.task('clean', function () {
    return gulp.src(['public/dist', 'reports/client', 'reports/server', 'doc', 'tmp'], {read: false})
        .pipe(clean());
});

// Watch assets for changes and run tasks on those changes.
gulp.task('watch', function () {
    // When client JavaScript changes, lint it!
    gulp.watch(config.assets.js, ['lint:client:js']);
    // When client CSS changes, lint it!
    gulp.watch(config.assets.cs, ['lint:client:css']);
    // When server JavaScript changes, lint it!
    gulp.watch(['gulpfile.js', 'server.js', 'config/**/*.js', 'app/**/*.js'], ['lint:server']);
});

// Bump bower and package versions.
// See [gulp-bump](https://github.com/stevelacy/gulp-bump).
// Usage: `gulp bump --<type>`

gulp.task('bump', function () {
    /** @namespace argv.type */
    return gulp.src(['./bower.json', 'package.json'])
        // Defaulting to `patch` type
        .pipe(bump({type: argv.type || 'patch'}))
        .pipe(gulp.dest('./'));
});

// Run a nodemon server
// Will auto-restart the server when it detects changes in watched files.
// Usage: `gulp server` or `gulp server --debug`.
gulp.task('server', function () {
    /** @namespace argv.debug */
    nodemon({
        script: 'server.js',
        ext: 'js html yml',
        watch: ['app/', 'public/', 'config/'],
        ignore: ['public/lib/', '.git/', 'node_modules/', 'reports/', '.idea/', 'doc/', 'tmp/'],
        nodeArgs: [argv.debug ? '--debug' : '--']
    });
});

// Build!.
// By linting and compacting.
gulp.task('build', ['lint', 'compact', 'swagger-api']);

// Help
gulp.task('help', taskListing);

// Default task, ran when you simply type `gulp`.
// Will first `lint` then run the `server`.
gulp.task('default', function (done) {
    runSequence('lint', 'swagger-api', 'server', done);
});





