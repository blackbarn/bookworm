// # server
//
// Bootstraps the application.

'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var init = require('./config/init')(),
	config = require('./config/config'),
	mongoose = require('mongoose'),
    logger = require('./app/providers/log/logger');

// ## Bootstrap the application
// _Order of loading **is** important!_

// Bootstrap db connection
var db = mongoose.connect(config.db);

// Init the express application
var app = require('./config/express')(db);

// Bootstrap passport config
require('./config/passport')();

// Bootstrap scheduling
require('./config/scheduler')(app);

var server = require('http').Server(app);

var io = require('socket.io')(server);

app.io = io;

logger.setIo(io);

// Start the app by listening on `<port>`
server.listen(config.port);

// Expose app
exports = module.exports = app;

// Logging that the application has started.
logger.get('Server').log('info', 'Bookworm started on port ' + config.port);
