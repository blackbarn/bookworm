// # cast.js
// _app/util/_

// Utility module to help casting of various types with defaults support.
'use strict';

function Cast () {}

/**
 * ## Cast.prototype.bool
 *
 * Cast something to a boolean
 *
 * @param {*} val Value to cast
 * @param {*} [otherwise] Default value to use when cast fails.
 * @returns {*}
 */
Cast.prototype.bool = function (val, otherwise) {
    var results, result;

    // Support aliases
    results = {
        'true' 	: true,
        'false'	: false,
        '1' 	: true,
        '0' 	: false,
        'on' 	: true,
        'off' 	: false,
        'yes'	: true,
        'no' : false
    };

    // If we are given a boolean already, return it
    if ( val === true || val === false) {
        return val;
    }

    // Look up the value in our alias map
    result = results[(val + '').toLowerCase()];

    // Return the result if we got a match
    if (result === true || result === false) {
        return result;
    }
    else {
        // No match, return the provided default
        return otherwise;
    }
};

/**
 * ## Cast.prototype.string
 *
 * Cast something to a string. If value is null or undefined, use otherwise.
 *
 * @param {*} val Value to cast
 * @param {*} [otherwise] Default value to use when cast fails.
 * @returns {*}
 */
Cast.prototype.string = function (val, otherwise) {
    if (val === null || typeof val === 'undefined') {
        return otherwise;
    }
    else {
        return (val + '');
    }
};

/**
 * ## Cast.prototype.int
 *
 * Cast something to an int
 *
 * @param {*} val Value to cast
 * @param {Number} radix The `radix` to use when parsing the value to an integer
 * @param {*} [otherwise] Default value to use when cast fails.
 * @returns {*}
 */
Cast.prototype.int = function (val, radix, otherwise) {
    var result = parseInt(val, radix);

    if (isNaN(result)) {
        return otherwise;
    }
    else {
        return result;
    }
};

/**
 * ## Cast.prototype.float
 *
 * Cast something to a float
 *
 * @param {*} val Value to cast
 * @param {*} [otherwise] Default value to use when cast fails.
 * @returns {*}
 */
Cast.prototype.float = function (val, otherwise) {
    var result = parseFloat(val);

    if (isNaN(result)) {
        return otherwise;
    }
    else {
        return result;
    }
};

// Export a created object
module.exports = new Cast();