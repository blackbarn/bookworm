// # mongoose.js
// _app/util/_

// Mongoose Utilities Module
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var APIError = require('../errors/api-error');
var _ = require('lodash');

/**
 * @name Error
 * @property {Number} statusCode Status Code associated with this error
 * @property {Object} errors Optional errors object for multiple errors
 * @property {String} verboseMessage verbose message is the regular thrown error message.
 */

/**
 * ## mongooseUtil.parseError
 *
 * Parses a MongooseDB Error into a more user-friendly error.
 *
 * @param {Error} err Error object
 * @param {String} [model] Optional Database model
 * @returns {APIError}
 */
module.exports.parseError = function (err, model) {
    var message;
    // Provide defaults
    err.name = err.name || 'MongoDB Error';
    err.statusCode = 400;

    // When an error `code` exists, attempt to look it up and provide
    // meaningful information in the `message`, `statusCode`
    if (err.code) {
        switch (err.code) {
        case 11000:
        case 11001:
            message = (model || 'Entity') + ' already exists';
            err.statusCode = 409;
            break;
        default:
            message = 'Something went wrong';
            err.statusCode = 500;
        }
    } else {
        // No `code` exists, attempt to iterate over the `errors` collection
        for (var errName in err.errors) {
            if (err.errors.hasOwnProperty(errName)) {
                if (err.errors[errName].message) {
                    message = err.errors[errName].message;
                    err.statusCode = 400;
                }
            }
        }
    }

    // When the `NODE_ENV` is `development` we store the original message within
    // the `verboseMessage` property.
    if (process.env.NODE_ENV === 'development') {
        err.verboseMessage = err.message;
    }

    // Set the error message
    err.message = message || err.message;

    return new APIError(err);
};

/**
 * ## mongooseUtil.getMatchingProperties
 *
 * Finds matching properties between a schema and request query
 *
 * @param {Object} paths Mongoose Schema Paths Object
 * @param {Object} query Express request query object
 * @param {Array} ignore Array of property names to ignore
 * @returns {Object} Matched properties with the query value.
 */
module.exports.getMatchingProperties = function (paths, query, ignore) {
    var matches = {}, regSplit;
    ignore = ignore || {};
    // Iterate over schema properties
    _.forEach(paths, function (path) {
        var property = path.path;
        // Only process if property exists on the query,
        // property is not private,
        // and property is not within the ignore list.
        if (query[property] && property.indexOf('_') !== 0 && !_.contains(ignore, property)) {
            // If our query value is intended to be a regex `/something/i`
            // split it up and build the regex.
            if (query[property].indexOf('/') === 0) {
                regSplit = query[property].split('/');
                matches[property] = new RegExp(regSplit[1], regSplit[2]);
            } else {
                // Use direct value
                matches[property] = query[property];
            }

        }
    });
    return matches;
};