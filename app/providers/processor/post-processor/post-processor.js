// # post-processor.js
// _app/providers/process/post-processor/_

// Post-Process Provider. Processes a download directory for releases by moving them to a destination directory
// and marking things downloaded.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var Q = require('q'),
    qfs = require('q-io/fs'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    path = require('path'),
    logger = require('../../log/logger').get('Post-Process-Provider');


/**
 * ## PostProcessProvider Constructor
 * @param {Provider} provider Provider Object
 * @constructor
 */
var PostProcessProvider = function (provider) {
    /**
     * Private Provider Reference
     * @type {Provider}
     * @private
     */
    this._provider = provider;

    this._Release = mongoose.model('Release');

    this._safePathRegex = '[^ 0-9a-zA-Z-[]()_\/]';
};

/**
 * ## PostProcessProvider.prototype.findDirectories
 *
 * Finds directories within the download directory. These directories are treated as downloaded releases.
 *
 * @returns {Promise.<String[], Error>} List of directory names when fulfilled, error when rejected.
 */
PostProcessProvider.prototype.findDirectories = function () {
    var directory;
    directory = this._provider.providerDetails.downloadDirectory;

    // Ensure directory is defined
    if (!_.isEmpty(directory)) {
        // Grab a list of items in the directory
        return qfs.listTree(directory).then(function (files) {
            // Iterate through keeping track of directories/files.
            return Q.all(files.map(function (file) {
                // Get file stat
                return qfs.stat(file).then(function (stat) {
                    return {
                        isDirectory: stat.isDirectory(),
                        file: file
                    };
                }.bind(this));
            }.bind(this)));
        }.bind(this)).then(function (files) {
            // Pull out all directories
            return _.pluck(_.filter(files, function (file) {
                return file.isDirectory;
            }), 'file');
        });
    } else {
        logger.log('warn', 'Download Directory is not set');
        return Q.fcall(function () {
            return [];
        });
    }


};

/**
 * ## PostProcessProvider.prototype.resolvePatternPath
 *
 * Takes a string pattern and a book and returns a resolved destination path.
 * e.g., /$Author/$Title -> /Some Author/Some Title
 *
 * @param {String} pattern The string pattern containing replacers. e.g., $First, $author, $Title, $Year
 * @param {Book} book Book object to resolve for.
 * @returns {Promise.<String|Null, Error>} Resolved path when fulfilled, error when rejected.
 */
PostProcessProvider.prototype.resolvePatternPath = function (pattern, book) {

    // First populate the book's author
    return Q.ninvoke(book, 'populate', 'author')
        .then(function (book) {
            var replacers, firstLetter, path;

            path = pattern;

            // Find the first letter of the author name
            firstLetter = book.author.name.slice(0, 1);

            // Define replacers and their values
            replacers = {
                '$First': firstLetter.toUpperCase(),
                '$first': firstLetter.toLowerCase(),
                '$Author': book.author.name,
                '$author': book.author.name.toLowerCase(),
                '$Title': book.title,
                '$title': book.title.toLowerCase(),
                '$Year': moment(book.published).format('YYYY')
            };

            // Iterate replacers, performing the replacement on the provided path
            _.forEach(replacers, function (value, key) {
                path = path.replace(key, value);
            });

            // Remove any unsafe characters
            return ((_.isEmpty(path)) ? null : path.replace(new RegExp(this._safePathRegex, 'g'), ''));
        }.bind(this));

};

/**
 * ## PostProcessProvider.prototype.moveRelease
 *
 * Moves a release from the download directory to a destination directory. Optionally leaving source files.
 *
 * @param {Release} release Release to move
 * @returns {Promise.<Release, Error>} Release when fulfilled, error when rejected.
 */
PostProcessProvider.prototype.moveRelease = function (release) {
    var destinationDirectory, sourceDirectory;


    // Grab the release's directory.
    sourceDirectory = release.directory;

    // Populate the release book author.
    return Q.ninvoke(release.book, 'populate', 'author')
        .then(function (book) {
            // Resolve the destination path for the book.
            return this.resolvePatternPath(this._provider.providerDetails.folderFormat, book)
                .then(function(folderName) {
                    // Join folder with directory
                    destinationDirectory = path.join(this._provider.providerDetails.destinationDirectory, folderName);

                    logger.log('info', '%s - Moving release to destination directory %s', release.name, destinationDirectory);

                    // Ensure source directory exists
                    return qfs.exists(sourceDirectory)
                        .then(function (exists) {
                            if (exists) {
                                return sourceDirectory;
                            } else {
                                throw new Error('Directory ' + sourceDirectory + ' for release no longer exists');
                            }
                        }).then(function () {
                            logger.log('debug', '%s - Creating destination directory', release.name, {directory: destinationDirectory});
                            // Create destination directory, using configured permissions
                            return qfs.makeTree(destinationDirectory, this._provider.providerDetails.directoryPermissions);
                        }.bind(this))
                        .then(function () {
                            logger.log('debug', '%s - Copying directory', release.name, {from: sourceDirectory, to: destinationDirectory});
                            // Copy the source files to the destination
                            return qfs.copyTree(sourceDirectory, destinationDirectory);
                        })
                        .then(function () {
                            // When not keeping original files, remove source files.
                            if (this._provider.providerDetails.keepOriginalFiles) {
                                return null;
                            } else {
                                logger.log('debug', '%s - Removing original release directory', release.name, {directory: sourceDirectory});
                                return qfs.removeTree(sourceDirectory);
                            }
                        }.bind(this))
                        .then(function () {
                            release.directory = destinationDirectory;
                            return release;
                        });
                }.bind(this));
        }.bind(this));

};

/**
 * ## PostProcessProvider.prototype.processRelease
 *
 * Process a single release
 *
 * @param {Release} release Release to process
 * @returns {Promise.<Release, Error>} Release when fulfilled, error when rejected.
 */
PostProcessProvider.prototype.processRelease = function (release) {
    // Populate the release book
    return Q.ninvoke(release, 'populate', 'book').then(function (release) {
        var book = release.book;
        // Only process if both book and release are in snatched state.
        if (book && book.status === 'snatched' && release.status === 'snatched') {
            logger.log('info', '%s - Processing release', release.name);
            // Only process if destination directory is set
            if (!_.isEmpty(this._provider.providerDetails.destinationDirectory)) {
                // Move the release
                return this.moveRelease(release).then(function (release) {
                    // After moving set both book and release as downloaded
                    book.status = 'downloaded';
                    release.status = 'downloaded';
                    book.notify(book.status);
                    return Q.all([Q.ninvoke(book, 'save'), Q.ninvoke(release, 'save')]).then(function () {
                        logger.log('info', '%s - Finished processing book', book.title);
                        return release;
                    }.bind(this));
                }.bind(this));
            } else {
                logger.log('warn', '%s - Not moving release, Destination Directory not set', release.name);
                return null;
            }
        } else {
            return null;
        }
    }.bind(this));
};

/**
 * ## PostProcessProvider.prototype.process
 *
 * Execute the processor
 *
 * @returns {Promise.<Release[], Error>} List of releases processed when fulfilled, error otherwise.
 */
PostProcessProvider.prototype.process = function () {
    logger.log('info', '%s - Provider process starting, looking for releases', this._provider.name);
    // Grab release directories
    return this.findDirectories().then(function (directories) {
        // Iterate directories finding the release that matches each one.
        return Q.all(directories.map(this._Release.getReleaseForDirectory.bind(this._Release))).then(function (releases) {
            // Remove any invalids
            return _.compact(releases);
        }).then(function (releases) {
            // Process releases
            return Q.all(releases.map(this.processRelease.bind(this)));
        }.bind(this));
    }.bind(this)).then(function (releases) {
        // Remove unprocessed releases
        releases = _.compact(releases);
        logger.log('info', '%s - Provider process finished. Processed %s releases', this._provider.name, releases.length);
        return releases;
    }.bind(this)).fail(function (err) {
        logger.log('error', logger.errorAsJSON(err));
        throw err;
    });
};

module.exports = PostProcessProvider;