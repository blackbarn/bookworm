// # logger.js
// _app/providers/log/_

// Logging Module.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var config = require('../../../config/config'),
    winston = require('winston'),
    moment = require('moment'),
    winstonMongo = require('winston-mongodb').MongoDB,
    _ = require('lodash');

// Split up the database path into pieces so we can pass values into `winston-mongodb`.
// Example: `mongodb://localhost/bookworm-dev` -> `["mongodb:", "", "localhost", "bookworm-dev"]`
var dbPieces = config.db.split('/');

var io = null;

/**
 * ## setIo
 *
 * Set the socket.io instance for the logger
 * @param {Object} obj Socket.IO instance
 */
module.exports.setIo = function (obj) {
    io = obj;
};

/**
 * ## get
 *
 * Retrieve a logger instance with a name/label
 *
 * @param {String} name Logger label
 * @returns {exports.Logger}
 */
module.exports.get = function (name) {

    var logger, transports;

    // Define transports array
    transports = [];

    // Conditionally add console logger
    if (config.logging.console.enabled) {
        transports.push(new winston.transports.Console({
            level: config.logging.console.level,
            colorize: true,
            timestamp: function () {
                return moment().format('YYY-MM-DD h:mm:ssa');
            },
            label: name
        }));
    }

    // Conditionally add mongo logger
    if (config.logging.mongo.enabled) {
        transports.push(new winstonMongo({
            db: dbPieces[3],
            host: dbPieces[2],
            level: config.logging.mongo.level,
            label: name
        }));
    }
    logger = new winston.Logger({
        transports: transports
    });

    /**
     * ## winston.errorAsJSON
     *
     * Static method to help with formatting errors into `JSON`
     * @lends winston
     * @param {Error} err The error object to format
     * @returns {Object} `JSON` formatted error
     */
    logger.errorAsJSON = function (err) {
        return {
            message: err.message,
            stack: err.stack,
            errors: err.errors || {}
        };
    };

    // When the logger log's something, emit it via socket.io
    logger.on('logging', function (transport, level, msg, meta) {
        if (io && transport.name === 'mongodb') {
            io.emit('log', _.extend({ timestamp: new Date(), label: transport.label, level: level, message: msg}, meta));
        }
    });

    return logger;
};