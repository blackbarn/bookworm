// # notifiers.js
// _app/providers/notify/_

// Processors Provider
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    logger = require('../log/logger').get('Notifiers-Provider'),
    q = require('q');


/**
 * ## NotifiersProvider constructor
 *
 * Constructs an instance of a NotifiersProvider object
 *
 * @constructor
 */
var NotifiersProvider = function () {
    this.Provider = mongoose.model('Provider');
};

/**
 * ## NotifiersProvider.prototype._invoke
 *
 * @param {String} method The method to invoke on the provider instance
 * @param {Object} options Query options object
 * @returns {Promise.<Object[], Error>} A list of objects when fulfilled, error when rejected
 * @private
 */
NotifiersProvider.prototype._invoke = function (method, options) {
    // Retrieve all release search providers
    return this.Provider.getProvidersByType(this.Provider.getTypesEnum().NOTIFY, true)
        // Then retrieve provider instances for each of them
        .then(function (providers) {
            return providers.map(function (provider) {
                return provider.getInstance();
            }.bind(this));
        })
        // Then using those instances, find some results.
        .then(function (instances) {
            // Only use reduce when we have more than once instance
            if (instances && instances.length > 1) {
                // We use reduce to pass the previous promise along so we can do a waterfall type of request
                return instances.reduce(function (prev, curr) {
                    // Grab the value of the previous promise
                    return prev
                        .then(function (results) {
                            // when there are results, return them
                            // Otherwise, continue the chain by getting the promise for the next value.
                            if (results && results.length) {
                                return results;
                            } else if (curr) {
                                return curr[method](options);
                            } else {
                                logger.log('info', 'Provider not configured to create instances of this type');
                            }
                        })
                        .catch(function (err) {
                            // When we have errors, log them and return an empty array (to ignore the provider).
                            logger.log('error', logger.errorAsJSON(err));
                            return [];
                        });
                }, q([]));
            } else if (instances && instances.length === 1) {
                if (instances[0]) {
                    // We only have one instance, call it directly.
                    return instances[0][method](options);
                } else {
                    logger.log('info', 'Provider not configured to create instances of this type');
                }
            } else {
                // No instances, return 0 results
                logger.log('info', 'There are no configured notification providers');
                return [];
            }
        });
};

/**
 * ## NotifiersProvider.prototype.notify
 *
 * Notifies all notifiers
 *
 * @param {Object} options Query options for the find
 * @returns {Promise.<Object[], Error>} A list of objects when fulfilled, error when rejected
 */
NotifiersProvider.prototype.notify = function (options) {
    return this._invoke('notify', options);
};

module.exports = NotifiersProvider;
