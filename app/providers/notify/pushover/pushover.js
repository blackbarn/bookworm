// # pushover.js
// _app/providers/notify/pushover/_

// Pushover Notification Provider
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var Q = require('q'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    request = require('request'),
    logger = require('../../log/logger').get('Pushover-Notification-Provider');


/**
 * ## PushoverProvider Constructor
 * @param {Object} provider Provider Object
 * @constructor
 */
var PushoverProvider = function (provider) {
    // Reference to the provider
    this._provider = provider;

};

/**
 * ## PushoverProvider.prototype.parseResponse
 *
 * Parse the Pushover Response
 *
 * @param {Object} response Pushover Response
 * @returns {Promise.<Object, Error>} Object with details when fulfilled, error when rejected.
 */
PushoverProvider.prototype.parseResponse = function (response) {
    var deferred = Q.defer();

    if (response && response.errors) {
        deferred.resolve({
            success: false,
            message: response.errors[0],
            statusCode: 400
        });
    } else if (response && !response.errors) {
        deferred.resolve({
            success: true,
            message: 'success',
            statusCode: 200
        });
    } else {
        deferred.reject(new Error('Could not parse pushover response'));
    }
    return deferred.promise;
};

/**
 * ## PushoverProvider.prototype.shouldNotify
 *
 * Parse the Pushover Response
 *
 * @param {String} event Notification event. e.g., `snatched` or `downloaded`.
 * @returns {Boolean} True/False on should notify.
 */
PushoverProvider.prototype.shouldNotify = function (event) {
    if (this._provider.providerDetails.onSnatch && event === 'snatched') {
        return true;
    }
    return this._provider.providerDetails.onDownload && event === 'downloaded';

};

/**
 * ## PushoverProvider.prototype.notify
 *
 * Parse the Pushover Response
 *
 * @param {Object} options Notify options
 * @param {String} options.event Event for notification
 * @param {Book} options.book The book we are notifying about
 * @returns {Promise.<Object, Error>} Object with details when fulfilled, error when rejected.
 */
PushoverProvider.prototype.notify = function (options) {
    var params;
    options = options || {};
    if (this.shouldNotify(options.event)) {
        logger.log('info', '%s - Notifying on %s for %s', this._provider.name, options.book.title, options.event);
        params = this._provider.providerDetails.api;
        params.title = this._provider.providerDetails.title;
        params.message = 'Book ' + options.book.title + ' was ' + options.event;
        params.url = options.book.providerData.link;
        params.url_title = options.book.title + '@' + this._provider.name;

        return Q.nfcall(request, {
            uri: this._provider.providerDetails.api.uri,
            method: 'POST',
            json: true,
            form: params
        }).spread(function (http, response) {
            return this.parseResponse(response);
        }.bind(this));
    } else {
        return Q.fcall(function () {
            return {
                success: false,
                message: 'Notifier not enabled or event not set to notify'
            };
        });
    }
};



module.exports = PushoverProvider;