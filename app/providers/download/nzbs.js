// # nzbs.js
// _app/providers/download/_

// NZBs Provider
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    logger = require('../log/logger').get('NZBs-Provider'),
    q = require('q');


/**
 * ## NzbsProvider constructor
 *
 * Constructs an instance of a NzbsProvider object
 *
 * @constructor
 */
var NzbsProvider = function () {
    this.Provider = mongoose.model('Provider');
};

/**
 * ## NzbsProvider.prototype._invoke
 *
 * @param {String} method The method to invoke on the provider instance
 * @param {Object} options Add options object
 * @returns {Promise.<Object[], Error>} A list of objects when fulfilled, error when rejected
 * @private
 */
NzbsProvider.prototype._invoke = function (method, options) {
    // Retrieve all release search providers
    return this.Provider.getProvidersByType(this.Provider.getTypesEnum().NZB_DOWNLOAD, true)
        // Then retrieve provider instances for each of them
        .then(function (providers) {
            return providers.map(function (provider) {
                return provider.getInstance();
            }.bind(this));
        })
        // Then using those instances, find some results.
        .then(function (instances) {
            // Only use reduce when we have more than once instance
            if (instances && instances.length > 1) {
                // We use reduce to pass the previous promise along so we can do a waterfall type of request
                return instances.reduce(function (prev, curr) {
                    // Grab the value of the previous promise
                    return prev
                        .then(function (results) {
                            // when there are results, return them
                            // Otherwise, continue the chain by getting the promise for the next value.
                            if (results && results.length) {
                                return results;
                            } else {
                                return curr[method](options);
                            }
                        })
                        .catch(function (err) {
                            // When we have errors, log them and return an empty array (to ignore the provider).
                            logger.log('error', logger.errorAsJSON(err));
                            return [];
                        });
                }, q([]));
            } else if (instances && instances.length === 1) {
                // We only have one instance, call it directly.
                return instances[0][method](options);
            } else {
                // No instances, return 0 results
                logger.log('info', 'There are no configured nzb download providers');
                return [];
            }
        });
};

/**
 * ## NzbsProvider.prototype.add
 *
 * Add a NZB
 *
 * @param {Object} options Add options for the operation
 * @returns {Promise.<Release, Error>} A release when fulfilled, error when rejected
 */
NzbsProvider.prototype.add = function (options) {
    return this._invoke('add', options);
};

module.exports = NzbsProvider;
