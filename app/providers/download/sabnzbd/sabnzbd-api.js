// # sabnzbd-api.js
// _app/providers/download/sabnzbd/_

// Interface with an instance of SABnzbd+ through its API
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var Q = require('q'),
    SABnzbd = require('sabnzbd'),
    apiDefaults = require('./sabnzbd-api-defaults'),
    _ = require('lodash');


/**
 * ## SABnzbdAPI Constructor
 *
 * Creates a new SABnzbdAPI instance. All methods are called on once instance of this object.
 *
 * @param {SABnzbdAPIOptions} options Options Object
 */
var SABnzbdAPI = function (options) {
    var host;

    /**
     * @name SABnzbdAPIOptions
     * @type {Object}
     * @property {String} host The host of the API
     * @property {String} apikey The API Key for the API
     * @property {String} category The default category
     */

    // Store api defaults
    this._defaults = apiDefaults;

    // Extend defaults with options
    this._settings = _.merge({}, this._defaults, options || {});

    // Handle missing protocol
    host = (this._settings.host.indexOf('http') === 0) ? this._settings.host : 'http://' + this._settings.host;

    // Create an instance of the SABnzbd api interface.
    this._api = new SABnzbd(host, this._settings.apikey);
};

/**
 * ## SABnzbdAPI.prototype.add
 *
 * Add a nzb using its url category and display name
 *
 * @param {Object} options Add options
 * @returns {Promise.<Object, Error>} Response object when fulfilled, error when rejected.
 */
SABnzbdAPI.prototype.add = function (options) {
    // Make the call
    return this._api.cmd('addurl', {
        name: options.url,
        cat: options.category || this._settings.category,
        nzbname: options.name
    }).then(function (response) {
        // When response status is false, there was an error
        if (response.status === false) {
            throw new Error('Could not add to sabnzbd.');
        } else {
            return response;
        }
    }.bind(this));
};

// Export
module.exports = SABnzbdAPI;