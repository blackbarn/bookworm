// # sabnzbd.js
// _app/providers/download/sabnzbd/_

// Sabnzbd Provider
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var Q = require('q'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    logger = require('../../log/logger').get('SABnzbd-Provider'),
    SABnzbdAPI = require('./sabnzbd-api');


/**
 * ## SABnzbdprovider Constructor
 * @param {Object} provider Provider Object
 * @constructor
 */
var SABnzbdProvider = function (provider) {
    // Reference to the provider
    this._provider = provider;
    // Reference to an api instance configured for this provider
    this._api = new SABnzbdAPI(provider.providerDetails);
};

/**
 * ## SABnzbdAPI.prototype.add
 *
 * Add a nzb using its url category and display name
 *
 * @param {Object} options Add options
 * @returns {Promise.<Object, Error>} Response object when fulfilled, error when rejected.
 */
SABnzbdProvider.prototype.add = function (options) {
    // First, find some releases given the options
    logger.log('info', 'Adding %s to sabnzbd', options.name);
    return this._api.add(options)
        .fail(function (err) {
            logger.log('error', 'Could not add nzb: %s', options.name, logger.errorAsJSON(err));
        });
};



module.exports = SABnzbdProvider;