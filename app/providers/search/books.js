// # books.js
// _app/providers/search/_

// Books Provider
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    logger = require('../log/logger').get('Books-Provider'),
    q = require('q');


/**
 * ## BooksProvider constructor
 *
 * Constructs an instance of a BooksProvider object
 *
 * @constructor
 */
var BooksProvider = function () {
    // Grab provider schema
    this.Provider = mongoose.model('Provider');
};

/**
 * ## BooksProvider.prototype._invoke
 *
 * @param {String} method The method to invoke on the provider instance
 * @param {Object} options Query options object
 * @returns {Promise.<Object[], Error>} A list of objects when fulfilled, error when rejected
 * @private
 */
BooksProvider.prototype._invoke = function (method, options) {
    // Retrieve all book search providers
    return this.Provider.getProvidersByType(this.Provider.getTypesEnum().BOOK_SEARCH, true)
        // Then retrieve provider instances for each of them
        .then(function (providers) {
            return providers.map(function (provider) {
                return provider.getInstance();
            }.bind(this));
        })
        // Then using those instances, find some results.
        .then(function (instances) {
            // Only use reduce when we have more than once instance
            if (instances && instances.length > 1) {
                // We use reduce to pass the previous promise along so we can do a waterfall type of request
                return instances.reduce(function (prev, curr) {
                    // Grab the value of the previous promise
                    return prev
                        .then(function (results) {
                            // when there are results, return them
                            // Otherwise, continue the chain by getting the promise for the next value.
                            if (results && results.length) {
                                return results;
                            } else {
                                return curr[method](options);
                            }
                        })
                        .catch(function (err) {
                            // When we have errors, log them and return an empty array (to ignore the provider).
                            logger.log('error', logger.errorAsJSON(err));
                            return [];
                        });
                }, q([]));
            } else if (instances && instances.length === 1) {
                // We only have one instance, call it directly.
                return instances[0][method](options);
            } else {
                // No instances, return 0 results
                logger.log('info', 'There are no configured book search providers');
                return [];
            }
        });
};

/**
 * ## BooksProvider.prototype.find
 *
 * Find/Search for books from book providers.
 * Retrieves a list of book search providers and searches them one by one until a successful response is given.
 *
 * @param {QueryOptions} options Query options for the find
 * @returns {Promise.<Object[], Error>} A list of objects when fulfilled, error when rejected
 */
BooksProvider.prototype.find = function (options) {

    return this._invoke('find', options);
};

/**
 * ## BooksProvider.prototype.findAuthors
 *
 * Find/Search for authors from book providers.
 * Retrieves a list of relevant authors for the given search
 *
 * @param {QueryOptions} options Query options for the find
 * @returns {Promise.<Object[], Error>} A list of objects when fulfilled, error when rejected
 */
BooksProvider.prototype.findAuthors = function (options) {
    return this._invoke('findAuthors', options);
};

/**
 * ## BooksProvider.prototype.findAuthorById
 *
 * Find an author given it's id
 *
 * @param {String} id Author (Provider/External) ID
 * @returns {Promise.<Author, Error>} The author when fulfilled, error when rejected.
 */
BooksProvider.prototype.findAuthorById = function (id) {
    return this._invoke('findAuthorById', id);
};

module.exports = BooksProvider;
