// # google-books.js
// _app/providers/search/google-books/_

// Google Books Provider
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var Q = require('q'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    logger = require('../../log/logger').get('Google-Books-Provider'),
    GoogleBooksAPI = require('./google-books-api'),
    GoogleBooksAPIParser = require('./google-books-api-parser'),
    Author = mongoose.model('Author'),
    Book = mongoose.model('Book');


/**
 * ## GoogleBooksProvider Constructor
 * @param {Object} provider Provider Object
 * @constructor
 */
var GoogleBooksProvider = function (provider) {
    // Reference to the provider
    this._provider = provider;
    // Reference to an api instance configured for this provider
    this._api = new GoogleBooksAPI(provider.providerDetails.request || {}, provider.providerDetails.query || {}, provider.providerDetails.cache || {});
    // Reference to the parser
    this._parser = new GoogleBooksAPIParser(this._provider);

    // Error strings
    this.errors = {
        NO_BOOK_DATA: 'No book data provided',
        NO_VOLUME_INFO: 'No volume info available for book'
    };
};

/**
 * ## GoogleBooksProvider.prototype.constructBook
 *
 * Constructs a book out of a GoogleBooks Response Item
 *
 * @param {Object} queryOptions Options of the original query that found the book data.
 * @param {BookItem} data Book Data
 * @returns {Promise.<Book|Null,Error>} Book or Null (ignored) when fulfilled, error when rejected.
 */
GoogleBooksProvider.prototype.constructBook = function (queryOptions, data) {
    var deferred, volume, bookData, book, author, isbn,
        description, languages, ignoredWords, ignoredWord,
        imageSmall, image;

    deferred = Q.defer();
    // Reject when no data is present
    if (_.isEmpty(data)) {
        deferred.reject(new Error(this.errors.NO_BOOK_DATA));
    } else if (_.isEmpty(data.volumeInfo)) {
        // Resolve with empty item when volumeInfo is empty
        logger.log('debug', 'Volume info not found, skipping');
        deferred.resolve();
    } else {
        /**
         * @name data.volumeInfo
         * @type {GoogleBooksVolume}
         */
        volume = data.volumeInfo;

        // Grab the author for the book
        author = this._parser.getAuthorFromVolume(volume);

        // Grab the isbn for the book
        isbn = this._parser.getBookISBNFromVolume(volume);

        // Grab the list of languages to care about
        languages = (this._provider.providerDetails && this._provider.providerDetails.filters && this._provider.providerDetails.languages) || 'en';

        // If the description is not empty, use it.
        if (!_.isEmpty(volume.description)) {
            description = volume.description;
        } else {
            description = '';
        }

        // Grab ignored words
        ignoredWords = this._provider.providerDetails.ignoredWords || [];

        // Look up any ignored words, using first ignored word.
        ignoredWord = this._parser.detectIgnoredWords(ignoredWords, volume.title);

        image = (volume.imageLinks && volume.imageLinks.thumbnail) ? volume.imageLinks.thumbnail : '';
        imageSmall = (volume.imageLinks && volume.imageLinks.smallThumbnail) ? volume.imageLinks.smallThumbnail : '';


        if (_.isEmpty(volume.title)) {
            // Book title did not exist, ignore book.
            logger.log('debug', 'Title not defined for book, skipping', {guid: data.id});
            deferred.resolve();
        } else if (!author) {
            // Author did not exist, ignore book.
            logger.log('debug', 'Author could not be found for book, skipping', {title: volume.title, author: volume.authors || volume.author});
            deferred.resolve();
        } else if (!_.isEmpty(languages) && !_.contains(languages, volume.language)) {
            // If we care about languages, this book did not match, ignore book.
            logger.log('debug', 'Book language does not match selected language filter, skipping', {title: volume.title, filter: languages, language: languages});
            deferred.resolve();
        } else if ((this._provider.providerDetails && this._provider.providerDetails.filters && this._provider.providerDetails.filters.description) && !description.length) {
            // If we are filtering on empty descriptions, skip it when empty.
            logger.log('debug', 'Book does not contain a description, skipping', {title: volume.title});
            deferred.resolve();
        } else if ((this._provider.providerDetails && this._provider.providerDetails.filters && this._provider.providerDetails.filters.isbn) && _.isEmpty(isbn)) {
            // If we are filtering on empty isbn, skip it when empty.
            logger.log('debug', 'Industry Identifier (ISBN) not available for Book, skipping', {title: volume.title});
            deferred.resolve();
        } else if (ignoredWord) {
            // Skip book if it contains an ignored word.
            logger.log('info', 'Book title contains an ignored word, skipping', {ignoredWord: ignoredWord, title: volume.title});
            deferred.resolve();
        } else {
            // Build the book!
            bookData = {
                externalId: data.id,
                title: volume.title,
                description: description,
                publisher: volume.publisher,
                averageRating: (typeof volume.averageRating !== 'undefined') ? volume.averageRating : 0,
                pageCount: volume.pageCount,
                published: (volume.publishedDate) ? volume.publishedDate : '',
                image: image,
                imageSmall: imageSmall,
                isbn: isbn,
                language: volume.language,
                status: 'skipped',
                providerData: {
                    id: data.id,
                    selfLink: data.selfLink,
                    authorName: author,
                    q: queryOptions.q
                },
                provider: this._provider,
                user: this._provider.user
            };


            book = new Book(bookData);

            // Resolve with the built book
            deferred.resolve(Q.ninvoke(book, 'populate', 'provider', 'name type canonicalName description'));
        }
    }
    // Return that promise.
    return deferred.promise;
};

/**
 * ## GoogleBooksProvider.prototype.find
 *
 * Find some books!
 *
 * @param {Object} options Find Options
 * @returns {Promise.<Book[],Error>} A list of books when fulfilled, error when rejected.
 */
GoogleBooksProvider.prototype.find = function (options) {
    // First, find some books given the options
    return this._api.find(options)
        // Parse the response
        .then(function (results) {
            if (!_.isArray(results)) {
                results = [results];
            }
            return Q.all(results.map(this._parser.parseResponse.bind(this)));
        }.bind(this))
        .then(function (items) {
            items = _.flatten(items);
            return this._parser.removeDuplicates(items);
        }.bind(this))
        // Construct books out of the items
        .then(function (items) {
            return Q.all(items.map(_.partial(this.constructBook, options).bind(this)));
        }.bind(this))
        // Exclude any non-objects (ignored books).
        .then(function (books) {
            return _.filter(books, function (book) {
                return _.isObject(book);
            });
        }.bind(this));
};

/**
 * ## GoogleBooksProvider.prototype.findOne
 *
 * Find a single book given its id
 *
 * @param {String} volumeId Book volume id
 * @param {Object} options Search options
 * @returns {Promise.<Book|Null,Error>} A single book (or null if not found) when fulfilled, error when rejected.
 */
GoogleBooksProvider.prototype.findOne = function (volumeId, options) {
    // Find one book
    return this._api.findOne(volumeId, options)
        // Parse the response
        .then(this._parser.parseResponse)
        // When there is a result, make the book
        .then(function (items) {
            if (items && items[0]) {
                return this.constructBook(options, items[0]);
            } else {
                return null;
            }
        }.bind(this));
};

/**
 * ## GoogleBooksProvider.prototype.findAuthors
 *
 * Query the Google Books API for a list of books matching an author query.
 * Using these results, compile a relevance rating for each to find the best match for the search.
 *
 * @param {Object} options Search options
 * @returns {Promise.<Object[],Error>} List of author relevance results when fulfilled, error when rejected.
 */
GoogleBooksProvider.prototype.findAuthors = function (options) {
    var localOptions;

    localOptions = _.clone(options, true);
    // Run an `inauthor` search with the query.
    localOptions.q = 'inauthor:' + localOptions.q;

    // Run the search
    return this.find(localOptions)
        // Compile the relevance rating results
        .then(this._parser.collateAuthors.bind(this._parser))
        .then(function (authors) {
            return authors.map(function (author) {
                return new Author(author);
            });
        });
};

/**
 * ## GoogleBooksProvider.prototype.findAuthorById
 *
 * Google Books does not have an author registry.
 *
 * @param {String} id ExternalID Of the author
 * @returns {Promise.<Null, Error>} Null when fulfilled, error when rejected.
 */
GoogleBooksProvider.prototype.findAuthorById = function (id) {
    logger.log('info', 'This provider does not support findAuthorById. %s', id);
    return Q.fcall(function () {
        return null;
    });
};
module.exports = GoogleBooksProvider;