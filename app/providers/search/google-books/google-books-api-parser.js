// # google-books-api-parser.js
// _app/providers/search/google-books/_

// Parser for Google Books API responses
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var Q = require('q'),
    _ = require('lodash');


/**
 * ## GoogleBooksAPIParser Constructor
 *
 * Creates a new GoogleBooksAPIParser instance.
 *
 * @param {Object} provider The provider that provided these books
 */
var GoogleBooksAPIParser = function (provider) {
    // Define error strings
    this.errors = {
        EMPTY_RESPONSE: 'Google Books API Response is empty',
        RESPONSE_ERROR: 'Google Books API Responded with an unknown error',
        VOLUME_EMPTY: 'Volume is empty',
        NO_AUTHOR: 'Author could not be found for book',
        NO_ISBN: 'ISBN could not be found for book'
    };
    this._provider = provider;
};

/**
 * @name GoogleBooksResponse
 * @property {Object} error Error object when errors are present
 * @property {Number} totalItems Total number of items when more than one result
 * @property {Array} items Array of response items
 */

/**
 * @name BookItem
 * @alias item
 * @property {String} id item Id
 * @property {GoogleBooksVolume} volumeInfo Item Volume Information
 */

/**
 * @name ImageLinks
 * @property {String} thumbnail Normal sized thumbnail
 * @property {String} smallThumbnail Small sized thumbnail
 */

/**
 * @name GoogleBooksVolume
 * @property {String} title Book Title
 * @property {String} author Book Author
 * @property {Array} authors List of book authors
 * @property {Object} industryIdentifiers Industry Identifiers object (ISBN stuff)
 * @property {Date} publishedDate Book Published Date
 * @property {ImageLinks} imageLinks Various image links for the book
 */

/**
 * ## GoogleBooksAPIParser.prototype.parseResponse
 *
 * Parse a Google Books API Response
 *
 * @param {GoogleBooksResponse} response Google Books API Response object
 */
GoogleBooksAPIParser.prototype.parseResponse = function (response) {
    var error, items, deferred, customError;
    // Define our deferred object
    deferred = Q.defer();

    if (_.isEmpty(response)) {
        // No response
        deferred.reject(new Error(this.errors.EMPTY_RESPONSE));
    } else if (response.error) {

        if (_.isEmpty(response.error.errors)) {
            // Handle unknown error
            error = this.errors.RESPONSE_ERROR;
        } else {
            // Grab first error
            error = 'Google Books API: ' + response.error.errors[0].message + ': ' + response.error.errors[0].reason;
        }
        customError = new Error(error);
        customError.name = 'GoogleBooksError';
        // Reject the deferred with this error.
        deferred.reject(customError);
    } else {
        // Handle single book response
        if (_.isUndefined(response.totalItems)) {
            items = [response];
        } else if (response.items) {
            // Ensure no duplicate books
            items = _.uniq(response.items, function (item) {
                return item.id;
            });
            // Ensure no duplicate book titles
            items = _.uniq(items, function (item) {
                return item.volumeInfo && item.volumeInfo.title;
            });
        } else {
            // Was not single or multiple items, something else is afoot. Send back empty list.
            items = [];
        }
        // Resolve with our items
        deferred.resolve(items);
    }
    // Return that promise
    return deferred.promise;
};

/**
 * ## GoogleBooksAPIParser.prototype.removeDuplicates
 *
 * @param {Book[]} items List of book items to remove duplicates from
 * @returns {Promise.<Book[], Error>} Dupe removed book list when fulfilled, error when rejected.
 */
GoogleBooksAPIParser.prototype.removeDuplicates = function (items) {
    // Ensure no duplicate books
    items = _.uniq(items, function (item) {
        return item.id;
    });
    // Ensure no duplicate book titles
    items = _.uniq(items, function (item) {
        return item.volumeInfo.title.trim();
    });
    return Q.fcall(function () {
        return items;
    });
};
/**
 * ## GoogleBooksAPIParser.prototype.getAuthorFromVolume
 *
 * Get an author from a google books volume object. Each book could have none, one or multiple authors.
 * We need to get only one author and be as accurate as we can.
 *
 * @param {GoogleBooksVolume} volume A Google Books Volume Object
 * @param {String} [query] The original query to use as a haystack search.
 * @returns {String|Null} Author name or null if no author.
 */
GoogleBooksAPIParser.prototype.getAuthorFromVolume = function (volume, query) {
    var author;
    // Null author if volume is empty
    if (_.isEmpty(volume)) {
        return null;
    } else {
        // When only one author, return that
        if (volume.author) {
           return volume.author;
        } else if (volume.authors) {
            // When multiple authors either return the first one if no query is given.
            // Otherwise when a query is given search it for a match.
            if (query) {
               author = _.find(volume.authors, function (possibleAuthor) {
                   return possibleAuthor.toLowerCase().indexOf(query.toLowerCase()) !== -1;
               });
            }
            // Return an author if we found one, otherwise, the first in the array.
            return author || _.first(volume.authors);
        } else {
            // No author or authors array present, returning null.
            return null;
        }
    }
};

/**
 * ## GoogleBooksAPIParser.prototype.getBookISBNFromVolume
 *
 * Get the first ISBN 10 or 13 for the book.
 *
 * @param {GoogleBooksVolume} volume A Google Books Volume Object
 * @returns {String|Null} ISBN if available, null otherwise.
 */
GoogleBooksAPIParser.prototype.getBookISBNFromVolume = function (volume) {
    var isbn;
    // Return null if volume is empty
    if (_.isEmpty(volume)) {
        return null;
    } else {
        // Parse industry identifiers
        if (volume.industryIdentifiers) {
            // Grab the first isbn 10 or isbn 13 we can find and return it.
            isbn = _.find(volume.industryIdentifiers, function (value) {
                return value.type === 'ISBN_10' || value.type === 'ISBN_13';
            });
            if (isbn && isbn.identifier) {
                return isbn.identifier;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
};

/**
 * ## GoogleBooksAPIParser.prototype.collateAuthors
 *
 * Collate a list of authors given a list of books.
 * Authors are granted a relevance rating (frequency of occurrence)
 *
 * @param {object[]} books List of books
 * @returns {Promise.<Array,Error>} An array of author/frequency objects when fulfilled, error when rejected.
 */
GoogleBooksAPIParser.prototype.collateAuthors = function (books) {
    // When books does not exist, define empty array.
    if (!books) {
        books = [];
    } else if (!_.isArray(books)) {
        // Force it to be an array
        books = [books];
    }

    if (_.isEmpty(books)) {
        return Q.fcall(function () {
            return books;
        }, null);
    } else {
        // Use reduce on books to calculate
        return Q.all(_.reduce(books, function (counts, book) {
            counts[book.providerData.authorName] = (counts[book.providerData.authorName] || 0) + 1;
            return counts;
        }, {})).then(function (counts) {
            return Q.all(_.map(counts, function (count, name) {
                return {
                    name: name,
                    relevance: count,
                    provider: this._provider,
                    user: this._provider.user
                };
            }.bind(this)));
        }.bind(this));
    }


};

/**
 * ## GoogleBooksAPIParser.prototype.detectIgnoredWords
 *
 * Detect ignored words in a haystack
 *
 * @param {Array} ignoredWords List of ignored words to check for
 * @param {String} haystack The string to check for ignored words
 * @returns {String|Null}
 */
GoogleBooksAPIParser.prototype.detectIgnoredWords = function (ignoredWords, haystack) {
    var i, workingWord, ignoredWord;
    ignoredWord = null;
    // We search the haystack for the presence of at least one ignored word
    if (haystack && ignoredWords && ignoredWords.length) {
        for (i = 0; i < ignoredWords.length; i = i + 1) {
            workingWord = ignoredWords[i];
            workingWord = workingWord.replace(/^\s+/g, '').replace(/\s+$/g, '');
            if (!_.isEmpty(workingWord) && haystack.indexOf(workingWord) !== -1) {
                ignoredWord = workingWord;
                break;
            }
        }
    }
    // Return the first word found, or null if none.
    return ignoredWord;
};

// Export it
module.exports = GoogleBooksAPIParser;