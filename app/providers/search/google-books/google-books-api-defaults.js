/**
 * @author Kyle Brown <blackbarn@gmail.com>
 */
module.exports = {
    queryOptions: {
        maxResults: 40,
        startIndex: 0,
        langRestrict: 'en',
        printType: 'books',
        orderBy: 'relevance',
        key: '',
        pagingQueryParallelCount: 1,
        pagingQueryLimit: 1
    },
    cacheOptions: {
        maxAge: 0,
        async: true,
        length: 1,
        primitive: true
    },
    requestOptions: {
        useSharedQueue: true,
        requestQueueDelay: 1500,
        requestQueueParallelCount: 1,
        apiUrl: 'https://www.googleapis.com/books/v1/volumes',
        userAgent: require('../../../../config/config').userAgent
    }
};