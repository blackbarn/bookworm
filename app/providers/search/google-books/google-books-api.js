// # google-books-api.js
// _app/providers/search/google-books/_

// Interface with the Google Books API
// Search (query) or find by id
// Search across pages automatically
// See the [Google Books API Docs](https://developers.google.com/books/docs/v1/using#PerformingSearch) for more details
// on query specifics.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var _ = require('lodash'),
    cast = require('../../../util/cast'),
    request = require('request'),
    qs = require('querystring'),
    async = require('async'),
    Q = require('q'),
    apiDefaults = require('./google-books-api-defaults'),
    logger = require('../../log/logger').get('Google-Books-API'),
    requestCacheFactory = require('../../request-queue-cache');

/**
 * ## GoogleBooksAPI Constructor
 *
 * Creates a new GoogleBooksAPI instance. All methods are called on once instance of this object.
 *
 * @param {RequestOptions} requestOptions Options related to the HTTP requests and HTTP queue
 * @param {QueryOptions} queryOptions Options related to the HTTP request query params
 * @param {CacheOptions} cacheOptions Options related to the cache
 */
var GoogleBooksAPI = function (requestOptions, queryOptions, cacheOptions) {

    /**
     * @name RequestOptions
     * @property {Boolean} useSharedQueue When true it will used a shared queue (across all instances)
     * @property {Number} requestQueueDelay Time in `ms` between each queued request
     * @property {Number} requestQueueParallelCount Number of parallel requests to allow when processing the queue
     * @property {String} apiUrl The base url for the Google Books API
     * @property {String} userAgent The user-agent to send with the requests
     */

    /**
     * Query parameters to send to the Google Books API
     *
     * @name QueryOptions
     * @property {String} q The query parameter
     * @property {Number} maxResults Maximum number of results
     * @property {Number} startIndex Index to start on
     * @property {String} langRestrict Language code to restrict the search to, such as `en`
     * @property {String} printType The `printType` to search for
     * @property {String} orderBy Order the results by `relevance` or `newest`
     * @property {Number} pagingQueryLimit The number of pages to query until stopping
     * @property {Object} paging Local only paging params.
     */

    /**
     * Cache options for the cache object
     *
     * @name CacheOptions
     * @property {Number} maxAge Maximum age to store the cached item for. `0` is never
     * @property {Boolean} async Should the cache be handled asynchronously
     * @property {Number} length How many parameters to use for the key
     * @property {Boolean} primitive Type of key
     */

    // Set private defaults using the apiDefaults module
    this._defaults = apiDefaults;

    // Merge defaults with options argument
    this._settings = {};
    this._settings.queryOptions = _.merge({}, this._defaults.queryOptions, queryOptions || {});
    this._settings.cacheOptions = _.merge({}, this._defaults.cacheOptions, cacheOptions || {});
    this._settings.requestOptions = _.merge({}, this._defaults.requestOptions, requestOptions || {});

    // Define a parameter blacklist. These will not get sent to the Google Books API, used internally only.
    this._paramBlacklist = ['paging', 'offset', 'limit', 'pagingQueryLimit', 'pagingQueryParallelCount', 'fields', 'page', 'perPage'];

    // Retrieve a request queue/cache instance using the current settings.
    this._apiCache = requestCacheFactory.create(this._settings.cacheOptions, this._settings.requestOptions);
};

// Private find method. Used to make a single query to the google books api.
// It takes an `options` object.
// Returns a Promise that when fulfilled will contain a response object.


/**
 * ## GoogleBooksAPI.prototype._find
 *
 * Private method to issue a single query to the api
 *
 * @param {QueryOptions|Object} options Query options
 * @returns {Promise.<Object,Error>} Object with the response when fulfilled, error when rejected.
 */
GoogleBooksAPI.prototype._find = function (options) {
    var requestOptions, localOptions, id, params;

    // Ensure options exists
    localOptions = _.clone(options, true) || {};

    // Remove blacklisted parameters
    this._paramBlacklist.forEach(function (param) {
        delete localOptions[param];
    });

    params = _.merge({}, this._settings.queryOptions, localOptions);

    if (_.isEmpty(params.key)) {
        delete params.key;
    }
    // Define request options
    // Merges the class settings query params with passed in options.
    requestOptions = {
        uri: this._settings.requestOptions.apiUrl,
        qs: params,
        headers: {
            'User-Agent': this._settings.requestOptions.userAgent
        },
        json: true
    };

    // create the id used for caching lookup
    id = requestOptions.uri + '?' + qs.stringify(requestOptions.qs);

    logger.log('debug', 'Making request to cache', { id: id });

    if (this._settings.requestOptions.useSharedQueue) {
        // call the shared cache (will request if not in cache, otherwise will return the cached result)
        return Q.ninvoke(GoogleBooksAPI, 'apiCache', id, requestOptions);
    } else {
        // call the local cache (will request if not in cache, otherwise will return the cached result)
        return Q.ninvoke(this, '_apiCache', id, requestOptions);
    }

};

/**
 * ## GoogleBooksAPI.prototype.find
 *
 * Query the Google Books API in an automated paging fashion.
 *
 * @param {QueryOptions|Object} options Query options
 * @returns {Promise.<Object,Error>} Object with the response when fulfilled, error when rejected.
 */
GoogleBooksAPI.prototype.find = function (options) {
    var queue, results, deferred;
    deferred = Q.defer();
    // Parse options into proper types
    options.startIndex = cast.int(options.startIndex, 10, this._settings.queryOptions.startIndex);
    options.maxResults = cast.int(options.maxResults, 10, this._settings.queryOptions.maxResults);

    // Define local paging object
    options.paging = {
        count: 0,
        pagingQueryLimit: cast.int(options.pagingQueryLimit, 10, this._settings.queryOptions.pagingQueryLimit)
    };


    results = [];

    // set up the queue
    queue = async.queue(function (query, next) {
        // the queue action is to run the api query
        this._find(query).then(function (body) {

            query.paging.count = query.paging.count + 1;
            // conditions to allow another query to be queued up:
            // 1. previous response existed
            // 2. previous response had positive total items
            // 3. previous response had items array returned
            // 4. paging query limit is less than 0 (limitless, other than the above criteria) or query has not exceeded the paging query limit
            if (body && body.totalItems && body.items && (query.paging.pagingQueryLimit < 0 || query.paging.count < query.paging.pagingQueryLimit)) {
                query.startIndex = query.startIndex + query.maxResults;
                queue.push(query);
            }
            // push the result to the results array
            results.push(body);
            // fire the callback
            next();
        }, next);
    }.bind(this), 1);


    // Handle when the queue is drained (all workers have returned their results)
    queue.drain = function () {
        var flattened = _.flatten(results);
        if (options.paging.pagingQueryLimit === 1) {
            // call the pagingQuery callback with a flattened result set
            deferred.resolve(flattened[0]);
        } else {
            deferred.resolve(flattened);
        }

    };

    // push the 1st query to the queue
    queue.push(options);

    return deferred.promise;
};

/**
 * ## GoogleBooksAPI.prototype.findOne
 *
 * Find a Google Books volume given its id
 *
 * @param {String} volumeId - The volume identifier
 * @param {QueryOptions|Object} options - The query options
 * @returns {Promise.<Object,Error>} Object with the response when fulfilled, error when rejected.
 */
GoogleBooksAPI.prototype.findOne = function (volumeId, options) {
    var requestOptions, id, params;

    // ensure options exist
    options = options || {};

    params = _.merge({}, this._settings.queryOptions, options);

    if (_.isEmpty(params.key)) {
        delete params.key;
    }

    // define request options
    requestOptions = {
        uri: this._settings.requestOptions.apiUrl + '/' + volumeId,
        qs: params,
        headers: {
            'User-Agent': this._settings.requestOptions.userAgent
        },
        json: true
    };

    // create the id used for caching lookup
    id = requestOptions.uri + '?' + qs.stringify(requestOptions.qs);

    logger.log('debug', 'Making request to cache', { id: id });

    if (this._settings.requestOptions.useSharedQueue) {
        // call the shared cache (will request if not in cache, otherwise will return the cached result)
        return Q.ninvoke(GoogleBooksAPI, 'apiCache', id, requestOptions);
    } else {
        // call the local cache (will request if not in cache, otherwise will return the cached result)
        return Q.ninvoke(this, '_apiCache', id, requestOptions);
    }

};

// Set the static apiCache
GoogleBooksAPI.apiCache = requestCacheFactory.create(apiDefaults.cacheOptions, apiDefaults.requestOptions);

// Export it
module.exports = GoogleBooksAPI;