// # newznab-api-parser.js
// _app/providers/search/newznab/_

// Parser for Newznab API responses
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var Q = require('q'),
    _ = require('lodash');


/**
 * ## NewznabAPIParser Constructor
 *
 * Creates a new NewznabAPIParser instance.
 */
var NewznabAPIParser = function () {};


/**
 * @name NewznabResponse
 * @property {Object} channel
 */
/**
 * Parse a newznab response
 * @param {NewznabResponse} response - response data
 * @returns {Promise} A promise of type Promise<Object, Error>
 */

/**
 * @name NewznabResponse
 * @property {Object} channel
 */

/**
 * @name ReleaseItem
 * @alias item
 * @property {String} id item Id
 * @property {String} title item title
 * @property {Object} attr Attributes
 * @property {String} guid GUID
 * @property {String} link Self link
 * @property {String} description Description of release.
 */

/**
 * ## NewznabAPIParser.prototype.parseResponse
 *
 * Parse a newznab response
 *
 * @param {NewznabResponse} response Response Data
 * @returns {Promise.<ReleaseItem[],Error>} Array with the parsed response when fulfilled, error when rejected.
 */
NewznabAPIParser.prototype.parseResponse = function (response) {
    var deferred;
    deferred = Q.defer();

    if (_.isEmpty(response) || _.isEmpty(response.channel) || _.isEmpty(response.channel.item)) {
        deferred.resolve([]);
    } else {
        if (_.isArray(response.channel.item)) {
            deferred.resolve(response.channel.item);
        } else {
            deferred.resolve([response.channel.item]);
        }
    }
    return deferred.promise;
};

/**
 * ## NewznabAPIParser.prototype.detectIgnoredWords
 *
 * Detects ignored words within the response
 *
 * @param {Array} ignoredWords List of ignored words to detect
 * @param {String} haystack String to search for the ignored words in
 * @returns {String|Null} null when nothing is found, the found string otherwise
 */
NewznabAPIParser.prototype.detectIgnoredWords = function (ignoredWords, haystack) {
    var i, workingWord, ignoredWord;
    ignoredWord = null;
    if (haystack && ignoredWords && ignoredWords.length) {
        for (i = 0; i < ignoredWords.length; i = i + 1) {
            workingWord = ignoredWords[i];
            workingWord = workingWord.replace(/^\s+/g, '').replace(/\s+$/g, '');
            if (!_.isEmpty(workingWord) && haystack.indexOf(workingWord) !== -1) {
                ignoredWord = workingWord;
                break;
            }
        }
    }
    return ignoredWord;
};

// Export it
module.exports = NewznabAPIParser;