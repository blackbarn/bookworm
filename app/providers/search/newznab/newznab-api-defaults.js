/**
 * @author Kyle Brown <blackbarn@gmail.com>
 */
module.exports = {
    queryOptions: {
        t: 'book',
        o: 'json',
        extended: 1,
        maxage: 500,
        apikey: ''
    },
    cacheOptions: {
        maxAge: 0,
        async: true,
        length: 1,
        primitive: true
    },
    requestOptions: {
        apiUrl: '',
        useSharedQueue: true,
        requestQueueDelay: 3000,
        requestQueueParallelCount: 1,
        userAgent: require('../../../../config/config').userAgent
    }

};