// # newznab.js
// _app/providers/search/newznab/_

// Newznab Provider
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var Q = require('q'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    logger = require('../../log/logger').get('Newznab-Provider'),
    NewznabAPI = require('./newznab-api'),
    NewznabAPIParser = require('./newznab-api-parser');


/**
 * ## NewznabProvider Constructor
 * @param {Object} provider Provider Object
 * @constructor
 */
var NewznabProvider = function (provider) {
    // Reference to release model
    this._Release = mongoose.model('Release');
    // Reference to the provider
    this._provider = provider;
    // Reference to an api instance configured for this provider
    this._api = new NewznabAPI(provider.providerDetails.request || {}, provider.providerDetails.query || {}, provider.providerDetails.cache || {});
    // Reference to the parser
    this._parser = new NewznabAPIParser(this._provider);

    // Error strings
    this.errors = {
        NO_RELEASE_DATA: 'No release data provided'
    };
};

/**
 * ## NewznabProvider.prototype.constructRelease
 *
 * Constructs a release out of a Newznab Response Item
 *
 * @param {Object} queryOptions Options of the original query that found the release data.
 * @param {ReleaseItem} data Release Data
 * @returns {Promise.<Release|Null,Error>} Release or Null (ignored) when fulfilled, error when rejected.
 */
NewznabProvider.prototype.constructRelease = function (queryOptions, data) {
    var releaseData, release, attributes, guid, usenetDate, deferred, size, grabs, review, ignoredWords, ignoredWord;

    deferred = Q.defer();

    if (_.isEmpty(data)) {
        deferred.reject(new Error(this.errors.NO_RELEASE_DATA));
    } else {
        ignoredWords = this._provider.providerDetails.ignoredWords || '';

        ignoredWords = ignoredWords.split(',');

        ignoredWord = this._parser.detectIgnoredWords(ignoredWords, data.title);

        attributes = _.pluck(data.attr, '@attributes');

        usenetDate = _.find(attributes, function (attr) {
            return attr.name === 'usenetdate';
        }).value;

        size = _.find(attributes, function (attr) {
            return attr.name === 'size';
        }).value;

        guid = _.find(attributes, function (attr) {
            return attr.name === 'guid';
        }).value;

        grabs = _.find(attributes, function (attr) {
            return attr.name === 'grabs';
        }).value;

        review = _.find(attributes, function (attr) {
            return attr.name === 'review';
        });
        if (review) {
            review = review.value;
        }

        if (ignoredWord) {
            logger.log('info', '[Newznab-Provider]', 'Release %s contains ignored word %s, skipping', data.title, ignoredWord);
            deferred.resolve();
        } else {
            releaseData = {
                externalId: guid,
                name: data.title,
                canonicalName: data.title + '.bw(' + guid + ')',
                description: data.description,
                grabs: grabs,
                review: review,
                size: size,
                releaseDate: usenetDate,
                providerData: {
                    id: data.guid,
                    link: data.link,
                    title: queryOptions.title,
                    author: queryOptions.author
                },
                provider: this._provider,
                user: this._provider.user
            };

            release = new this._Release(releaseData);

            deferred.resolve(Q.ninvoke(release, 'populate', 'provider', 'name type canonicalName description'));
        }
    }
    return deferred.promise;
};

/**
 * ## NewznabProvider.prototype.find
 *
 * Find some releases!
 *
 * @param {Object} options Find Options
 * @returns {Promise.<Release[],Error>} A list of releases when fulfilled, error when rejected.
 */
NewznabProvider.prototype.find = function (options) {
    // First, find some releases given the options
    return this._api.find(options)
        // Parse the response
        .then(this._parser.parseResponse.bind(this._parser))
        // Construct releases out of the items
        .then(function (items) {
            return Q.all(items.map(_.partial(this.constructRelease, options).bind(this)));
        }.bind(this))
        // Exclude any non-objects (ignored releases).
        .then(function (releases) {
            return _.filter(releases, function (release) {
                return _.isObject(release);
            });
        }.bind(this));
};

/**
 * ## NewznabProvider.prototype.findOne
 *
 * Find a single release given its id
 *
 * @param {String} id Release volume id
 * @param {Object} options Search options
 * @returns {Promise.<Release|Null,Error>} A single release (or null if not found) when fulfilled, error when rejected.
 */
NewznabProvider.prototype.findOne = function (id, options) {
    // Find one release
    return this._api.findOne(id, options)
        // Parse the response
        .then(this._parser.parseResponse)
        // When there is a result, make the release
        .then(function (items) {
            if (items && items[0]) {
                return this.constructRelease(options, items[0]);
            } else {
                return null;
            }
        }.bind(this));
};


module.exports = NewznabProvider;