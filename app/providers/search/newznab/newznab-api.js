// # newznab-api.js
// _app/providers/search/newznab/_

// Interface with the Newznab API
// Search (query) or find by id
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var _ = require('lodash'),
    request = require('request'),
    qs = require('querystring'),
    Q = require('q'),
    apiDefaults = require('./newznab-api-defaults'),
    logger = require('../../log/logger').get('Newznab-API'),
    requestCacheFactory = require('../../request-queue-cache');

/**
 * ## NewznabAPI Constructor
 *
 * Creates a new NewznabAPI instance. All methods are called on once instance of this object.
 *
 * @param {RequestOptions} requestOptions Options related to the HTTP requests and HTTP queue
 * @param {QueryOptions} queryOptions Options related to the HTTP request query params
 * @param {CacheOptions} cacheOptions Options related to the cache
 */
var NewznabAPI = function (requestOptions, queryOptions, cacheOptions) {

    /**
     * @name RequestOptions
     * @property {Boolean} useSharedQueue When true it will used a shared queue (across all instances)
     * @property {Number} requestQueueDelay Time in `ms` between each queued request
     * @property {Number} requestQueueParallelCount Number of parallel requests to allow when processing the queue
     * @property {String} apiUrl The base url for the Newznab API
     * @property {String} userAgent The user-agent to send with the requests
     */

    /**
     * Query parameters to send to the Newznab API
     *
     * @name QueryOptions
     * @property {String} t Type of search, for our regular use it is `book`
     * @property {String} o Output type, normally `json`
     * @property {Number} extended 1 for true, 0 for false. Show extended information.
     * @property {Number} maxage Max age of nzb to search for
     * @property {String} id The ID of a newznab item to get (when using `findOne`)
     * @property {String} title The title to search for
     * @property {String} author The author to search for
     */

    /**
     * Cache options for the cache object
     *
     * @name CacheOptions
     * @property {Number} maxAge Maximum age to store the cached item for. `0` is never
     * @property {Boolean} async Should the cache be handled asynchronously
     * @property {Number} length How many parameters to use for the key
     * @property {Boolean} primitive Type of key
     */

        // Set private defaults using the apiDefaults module
    this._defaults = apiDefaults;

    // Merge defaults with options argument
    this._settings = {};
    this._settings.queryOptions = _.merge({}, this._defaults.queryOptions, queryOptions || {});
    this._settings.cacheOptions = _.merge({}, this._defaults.cacheOptions, cacheOptions || {});
    this._settings.requestOptions = _.merge({}, this._defaults.requestOptions, requestOptions || {});

    // Define a parameter blacklist. These will not get sent to the Newznab API, used internally only.
    this._paramBlacklist = ['page', 'perPage', 'fields'];

    // Retrieve a request queue/cache instance using the current settings.
    this._apiCache = requestCacheFactory.create(this._settings.cacheOptions, this._settings.requestOptions);
};

// Private find method. Used to make a single query to the Newznab api.
// It takes an `options` object.
// Returns a Promise that when fulfilled will contain a response object.


/**
 * ## NewznabAPI.prototype.find
 *
 * Query the Newznab API
 *
 * @param {QueryOptions|Object} options Query options
 * @returns {Promise.<Object,Error>} Object with the response when fulfilled, error when rejected.
 */
NewznabAPI.prototype.find = function (options) {
    var requestOptions, localOptions, id;

    // Ensure options exists
    localOptions = _.clone(options, true) || {};

    // Remove blacklisted parameters
    this._paramBlacklist.forEach(function (param) {
        delete localOptions[param];
    });

    // Ensure newznab has /api at the end of the url
    if (!this._settings.requestOptions.apiUrl.match(/\/api$/)) {
        this._settings.requestOptions.apiUrl = this._settings.requestOptions.apiUrl + '/api';
    }
    // Define request options
    // Merges the class settings query params with passed in options.
    requestOptions = {
        uri: this._settings.requestOptions.apiUrl,
        qs: _.merge({}, this._settings.queryOptions, localOptions),
        headers: {
            'User-Agent': this._settings.requestOptions.userAgent
        },
        json: true
    };

    // create the id used for caching lookup
    id = requestOptions.uri + '?' + qs.stringify(requestOptions.qs);

    logger.log('debug', 'Making request to cache', { id: id });

    if (this._settings.requestOptions.useSharedQueue) {
        // call the shared cache (will request if not in cache, otherwise will return the cached result)
        return Q.ninvoke(NewznabAPI, 'apiCache', id, requestOptions);
    } else {
        // call the local cache (will request if not in cache, otherwise will return the cached result)
        return Q.ninvoke(this, '_apiCache', id, requestOptions);
    }

};

/**
 * ## NewznabAPI.prototype.findOne
 *
 * Find a Newznab volume given its id
 *
 * @param {String} guid - The guid of the nzb to find
 * @param {QueryOptions|Object} options - The query options
 * @returns {Promise.<ReleaseItem,Error>} Object with the response when fulfilled, error when rejected.
 */
NewznabAPI.prototype.findOne = function (guid, options) {
    return this.find(_.extend(options, {id: guid}));
};

// Set the static apiCache
NewznabAPI.apiCache = requestCacheFactory.create(apiDefaults.cacheOptions, apiDefaults.requestOptions);

// Export it
module.exports = NewznabAPI;