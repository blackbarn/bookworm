// # request-queue-cache.js
// _app/providers/_

// A request queue & cache factory module.
// It is multi dimensional. First is the cache layer, if the request does not exist in cache it calls the request queue.
// The request queue allows you to limit concurrent requests as well as time between requests.

'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var logger = require('./log/logger').get('Request-Queue-Cache'),
    _ = require('lodash'),
    memoize = require('memoizee'),
    async = require('async'),
    request = require('request');

module.exports = {

    /**
     * ## create
     *
     * Creates an instance of a request queue/cache object.
     *
     * @param {CacheOptions} cacheOptions Options for the cache
     * @param {RequestOptions} requestOptions Options for the request queue
     * @returns {Object} cache instance
     */
    create: function (cacheOptions, requestOptions) {

        var apiCache, requestQueue;

        cacheOptions = cacheOptions || {};
        requestOptions = requestOptions || {};

        // Cache for API calls based off of a id. The id is the full URL with query params.
        apiCache = memoize(function (id, options, next) {
            // When not cached, push the data onto the request queue.
            // `id` is for tracking purposes, `options` are the options when making the request
            requestQueue.push({
                id: id,
                options: options
            }, next);
        }.bind(this), cacheOptions || {});

        // Async Request queue - Queue up API requests so we can rate limit them.
        requestQueue = async.queue(function (data, next) {
            // Log when we are about to issue a remote request
            logger.log('debug', 'Not cached, issuing remote request', { url: data.id });
            // Issue the `GET` request
            request.get(data.options, function (err, response, body) {
                // Using `setTimeout` to delay the response so we can rate limit the queue
                setTimeout(function (data, response, body) {
                    if (err || (body && body.error && !_.isEmpty(body.error.errors))) {
                        // If there was an error with the request, clear its cache
                        apiCache.clear(data.id, true);
                    }
                    if (err) {
                        next(err);
                    } else {
                        next(null, body);
                    }
                }.bind(this), requestOptions.requestQueueDelay, data, response, body);
            }.bind(this));
        }.bind(this), requestOptions.requestQueueParallelCount);

        return apiCache;
    }
};