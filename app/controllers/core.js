// # core.js
// _app/controllers/_

// Core Controller.
'use strict';


/**
 * ## Render Index
 *
 * Render the one and only template, the index.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 */
exports.index = function (req, res) {
    res.render('index-view', {
        // Provide the user object to the template
        user: req.user || null
    });
};