// # authors.js
// _app/controllers/_

// Authors Controller.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    Author = mongoose.model('Author'),
    Book = mongoose.model('Book'),

    _ = require('lodash'),
    Q = require('q'),
    APIError = require('../errors/api-error'),
    mongooseUtil = require('../util/mongoose'),
    Provider = mongoose.model('Provider');


/**
 * ## Pause an author
 *
 * Pauses an [Author](../models/author.js.html) by setting its status to `paused`.
 * Exposing a frequent action as a rest endpoint.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.pause = function (req, res, next) {
    var author = req.author;

    author.status = 'paused';
    // Save the author
    author.save(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Author.modelName));
        } else {
            res.json(author);
        }
    });
};


/**
 * ## Resume an author
 *
 * Resumes an [Author](../models/author.js.html) by setting its status to `active`.
 * Exposing a frequent action as a rest endpoint.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.resume = function (req, res, next) {
    var author = req.author;

    author.status = 'active';
    // Save the author
    author.save(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Author.modelName));
        } else {
            res.json(author);
        }
    });
};

/**
 * ## Refresh an author
 *
 * Refreshes an [Author](../models/author.js.html) by setting its status to `refreshing`.
 * Exposing a frequent action as a rest endpoint.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.refresh = function (req, res, next) {
    var author = req.author;

    author.status = 'refreshing';
    // Save the author
    author.save(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Author.modelName));
        } else {
            res.json(author);
        }
    });
};

/**
 * ## Refresh an author's books
 *
 * Refreshes the books of a [Author](../models/author.js.html) by setting its status to `refreshing_books`.
 * Exposing a frequent action as a rest endpoint.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.refreshBooks = function (req, res, next) {
    var author = req.author;

    author.status = 'refreshing_books';
    // Save the author
    author.save(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Author.modelName));
        } else {
            res.json(author);
        }
    });
};

/**
 * ## Refresh an author's new books
 *
 * Refreshes a [Author](../models/author.js.html) checking for only new books by setting its status to `refreshing_new_books`.
 * Exposing a frequent action as a rest endpoint.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.refreshNewBooks = function (req, res, next) {
    var author = req.author;

    author.status = 'refreshing_new_books';
    // Save the author
    author.save(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Author.modelName));
        } else {
            res.json(author);
        }
    });
};

/**
 * ## Create a author
 *
 * Creates an [Author](../models/author.js.html) when provided with valid author `JSON`.
 * Uses default Provider if one is not...provided.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.create = function (req, res, next) {
    // Create an author object
    var author = new Author(req.body);

    // Assign the current authenticated user to this author.
    author.user = req.user;

    // Retrieve the intended provider, or grab the default.
    Provider.getProviderOrDefault(author.provider, req.user).then(function (provider) {
        // Set provider
        author.provider = provider;
        // Save the author
        author.save(function (err) {
            if (err) {
                // When there is an error, parse it for user friendliness and pass it on.
                next(mongooseUtil.parseError(err, Author.modelName));
            } else {
                // Send response
                res.json(author);
            }
        });
    }, function (err) {
        // Error when getting provider
        next(mongooseUtil.parseError(err, Author.modelName));
    });
};

/**
 * ## Show the current author
 *
 * Simply responds with the resolved author. See further below for the author middleware.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 */
exports.read = function (req, res) {
    res.json(req.author);
};

/**
 * ## Update an author
 *
 * Updates the resolved [Author](../models/author.js.html) with `JSON` provided in the request body.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.update = function (req, res, next) {
    var author = req.author;

    delete req.body.books;
    delete req.body.user;

    // Extend the resolved author with data from the body. Doing this we allow partial updates.
    author = _.extend(author, req.body);

    // Save the author
    author.save(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Author.modelName));
        } else {
            res.json(author);
        }
    });
};

/**
 * ## Update multiple authors
 *
 * Updates multiple [Author](../models/author.js.html) objects given an array of valid `JSON`.
 * Required fields are `id` and `status`. Only `status` is updated in this manner.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.updateMultiple = function (req, res, next) {
    if (_.isArray(req.body)) {
        Q.all(req.body.map(function (authorData) {
            return Q.ninvoke(Author, 'findOne', {
                _id: authorData._id,
                user: req.user
            }).then(function (author) {
                if (author) {
                    author.status = authorData.status;
                }
                return Q.ninvoke(author, 'save').then(function () {
                    return author;
                });
            });
        })).then(function (results) {
            res.json(_.compact(results));
        }).fail(next).done();
    } else {
        res.json(new APIError('Payload is not an array'));
    }
};

/**
 * ## Delete an author
 *
 * Remove the [Author](../models/author.js.html) completely.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.remove = function (req, res, next) {
    var author = req.author;

    author.remove(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Author.modelName));
        } else {
            res.json(author);
        }
    });
};

/**
 * ## List authors
 *
 * List all [Authors](../models/author.js.html).
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.list = function (req, res, next) {
    Author.paginate(Author.getSearchProperties(req.query), req._pagination.page, req._pagination.perPage, function (err, pageCount, authors, itemCount) {
        if (err) {
            next(mongooseUtil.parseError(err, Author.modelName));
        } else {
            res.setPaginationValues(pageCount, itemCount);
            res.json(authors);
        }
    }, {
        sortBy: req._sorting.sortMongo,
        populate: req._expanding.populate
    });
};

/**
 * ## Author Middleware
 *
 * Using the `id` in the route path, attempt to look up the [Author](../models/author.js.html).
 * Also populate the user and books of the author.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 * @param {ObjectId} authorId Author ID
 */
exports.authorByID = function (req, res, next, authorId) {
    var query;

    query = Author.findById(authorId);

    _.forEach(req._expanding.populate, function (populate) {
        query.populate(populate);
    });

    query.exec(function (err, author) {
        if (err) {
            next(mongooseUtil.parseError(err, Author.modelName));
        } else if (!author) {
            next(new APIError('Failed to load Author ' + authorId.id, 404));
        } else {
            req.author = author;
            next();
        }
    });
};

/**
 * ## Author Authorization Middleware
 *
 * Determine if the resolved [Author](../models/author.js.html) is authorized for the request.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.hasAuthorization = function (req, res, next) {
    // Ensures the current user is the owner of the author being looked up.
    if (req.author.user.id !== req.user.id) {
        next(new APIError('User is not authorized', 403));
    } else {
        next();
    }
};