// # users.js
// _app/controllers/_

// Users Controller.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('User'),
    _ = require('lodash'),
    APIError = require('../errors/api-error'),
    mongooseUtil = require('../util/mongoose');

/**
 * ## Sign up
 *
 * Sign up to the application. Only allowing local user/pass sign up for now.
 * Also only allow one user for now.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.signup = function (req, res, next) {
    // For security measures we remove the roles from the req.body object
    delete req.body.roles;

    // Create the user object
    var user = new User(req.body);

    // Add missing user fields
    user.provider = 'local';

    // Generate a new api key
    user.generateApiKey();

    // Get a count of any existing users
    User.count({}, function (err, count) {
        if (err) {
            next(mongooseUtil.parseError(err));
            // TODO: make this count comparison configurable
        } else if (count > 0) {
            // Only allow a set amount of registered users
            next(new APIError('Registered user limit reached', 400));
        } else {
            // Save the user
            user.save(function (err) {
                if (err) {
                    next(mongooseUtil.parseError(err, 'User'));
                } else {
                    // Remove sensitive data before login
                    user.password = undefined;
                    user.salt = undefined;

                    // Use the passport middleware to log the user in
                    req.login(user, function (err) {
                        if (err) {
                            next(err);
                        } else {
                            res.json(user);
                        }
                    });
                }
            });
        }
    });
};

/**
 * ## Sign In
 *
 * Sign into the application.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.signin = function (req, res, next) {
    // Using passport local authentication
    passport.authenticate('local', function (err, user, info) {
        // If there was an issue or the user did not exist, respond accordingly.
        if (err || !user) {
            res.send(400, info);
        } else {
            // Remove sensitive data before login
            user.password = undefined;
            user.salt = undefined;

            // Use the passport middleware to log the user in
            req.login(user, function (err) {
                if (err) {
                    next(err);
                } else {
                    res.json(user);
                }
            });
        }
    })(req, res, next);
};

/**
 * ## Update user
 *
 * Update the resolved [User](../models/user.js.html) with `JSON` provided in the request body.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.update = function (req, res, next) {
    // Grab the user
    var user = req.user;

    // For security measures we remove the roles from the req.body object
    delete req.body.roles;

    // Must have a user to udpate
    if (user) {
        // Extend the resolved user with data from the body. Doing this we allow partial updates.
        user = _.extend(user, req.body);
        user.updated = Date.now();

        // Save teh user
        user.save(function (err) {
            if (err) {
                next(mongooseUtil.parseError(err));
            } else {
                // Log the user in (again)
                req.login(user, function (err) {
                    if (err) {
                        next(err);
                    } else {
                        res.json(user);
                    }
                });
            }
        });
    } else {
        next(new APIError('User is not signed in', 400));
    }
};

/**
 * ## Change Password
 *
 * Change the resolved user's password.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.changePassword = function (req, res, next) {
    /**
     * Password Details
     * @typedef {Object} passwordDetails
     * @property {String} verifyPassword
     * @property {String} newPassword
     * @property {String} currentPassword
     */

    // Grab the password info
    var passwordDetails = req.body;

    // User must exist in order to change their password
    if (req.user) {
        User.findById(req.user.id, function (err, user) {
            if (!err && user) {
                // Ensure they know the current password
                if (user.authenticate(passwordDetails.currentPassword)) {
                    // Ensure new password and the verify password field match
                    if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
                        user.password = passwordDetails.newPassword;

                        user.save(function (err) {
                            if (err) {
                                next(mongooseUtil.parseError(err));
                            } else {
                                req.login(user, function (err) {
                                    if (err) {
                                        next(err);
                                    } else {
                                        res.send({
                                            message: 'Password changed successfully'
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        next(new APIError('Passwords do not match', 400));
                    }
                } else {
                    next(new APIError('Current password is incorrect', 400));
                }
            } else {
                next(new APIError('User not found', 400));
            }
        });
    } else {
        next(new APIError('User not signed in', 400));
    }
};

/**
 * ## Sign Out
 *
 * Sign the user out and redirect them to the root page.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 */
exports.signout = function (req, res) {
    req.logout();
    res.redirect('/');
};

/**
 * ## Send current user
 *
 * Simply responds with the resolved user. See further below for the user middleware.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 */
exports.me = function (req, res) {
    res.json(req.user || null);
};

/**
 * ## User Middleware
 *
 * Using the `id` in the route path, attempt to look up the [User](../models/user.js.html).
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 * @param {String} id User ID
 */
exports.userByID = function (req, res, next, id) {
    User.findOne({
        _id: id
    }).exec(function (err, user) {
        if (err) {
            next(mongooseUtil.parseError(err));
        } else if (!user) {
            next(new APIError('Failed to load User ' + id, 404));
        } else {
            req.profile = user;
            next();
        }
    });
};

/**
 * ## Require Login Routing Middleware
 *
 * When used, ensures the user is authenticated.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.requiresLogin = function (req, res, next) {
    if (!req.isAuthenticated()) {
        passport.authenticate('localapikey', {
            session: false
        })(req, res, next);
    } else {
        next();
    }
};

/**
 * ## User Authorization Routing Middleware Generator
 *
 * Authorizes on a role level.
 *
 * @param {Array} roles List of roles to check for authorization
 */
exports.hasAuthorization = function (roles) {
    var _this = this;

    /**
     * User Authorization Routing Middleware
     * @param {Object} req Express request object
     * @param {Object} res Express response object
     * @param {Function} next Middleware callback
     */
    return function (req, res, next) {
        _this.requiresLogin(req, res, function () {
            if (_.intersection(req.user.roles, roles).length) {
                return next();
            } else {
                next(new APIError('User is not authorized', 403));
            }
        });
    };
};