// # providers.js
// _app/controllers/_

// Providers Controller.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    Provider = mongoose.model('Provider'),
    _ = require('lodash'),
    APIError = require('../errors/api-error'),
    mongooseUtil = require('../util/mongoose');

/**
 * ## Start Provider Process
 *
 * Executes a providers process functionality.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.process = function (req, res, next) {
    // Grab context provider
    var provider = req.provider;

    provider.status = 'running';

    // Save the provider
    provider.save(function (err) {
        if (err) {
            // When there is an error, parse it for user friendliness and pass it on.
            next(mongooseUtil.parseError(err, Provider.modelName));
        } else {
            res.json(201, provider);
        }
    });
};

/**
 * ## Create a provider
 *
 * Creates a [Provider](../models/provider.js.html) when provided with valid provider `JSON`
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.create = function (req, res, next) {
    // Create an provider object
    var provider = new Provider(req.body);
    // Assign the current authenticated user to this provider.
    provider.user = req.user;

    // Save the provider
    provider.save(function (err) {
        if (err) {
            // When there is an error, parse it for user friendliness and pass it on.
            next(mongooseUtil.parseError(err, Provider.modelName));
        } else {
            res.json(201, provider);
        }
    });
};

/**
 * ## Show the current provider
 *
 * Simply responds with the resolved provider. See further below for the provider middleware.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 */
exports.read = function (req, res) {
    res.json(req.provider);
};

/**
 * ## Update an provider
 *
 * Updates the resolved [Provider](../models/provider.js.html) with `JSON` provided in the request body.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.update = function (req, res, next) {
    var provider = req.provider;

    // Disallow changing of user
    delete req.body.user;

    // Extend the resolved provider with data from the body. Doing this we allow partial updates.
    provider = _.merge(provider, req.body);

    provider.markModified('providerDetails');
    // Save the provider
    provider.save(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Provider.modelName));
        } else {
            res.json(provider);
        }
    });
};

/**
 * ## Delete an provider
 *
 * Remove the [Provider](../models/provider.js.html) completely.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.remove = function (req, res, next) {
    var provider = req.provider;

    provider.remove(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Provider.modelName));
        } else {
            res.send(204);
        }
    });
};

/**
 * ## List providers
 *
 * List all [Providers](../models/provider.js.html), sorted by created date descending.
 * Also populate the user and books for each provider.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.list = function (req, res, next) {

    Provider.paginate(Provider.getSearchProperties(req.query), req._pagination.page, req._pagination.perPage, function (err, pageCount, providers, itemCount) {
        if (err) {
            next(mongooseUtil.parseError(err, Provider.modelName));
        } else {
            res.setPaginationValues(pageCount, itemCount);
            res.json(providers);
        }
    }, {
        sortBy: req._sorting.sortMongo,
        populate: req._expanding.populate
    });
};

/**
 * ## Provider Middleware
 *
 * Using the `id` in the route path, attempt to look up the [Provider](../models/provider.js.html).
 * Also populate the user and books of the provider.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 * @param {ObjectId} id Provider ID
 */
exports.providerByID = function (req, res, next, id) {
    var query;

    query = Provider.findById(id);

    _.forEach(req._expanding.populate, function (populate) {
        query.populate(populate);
    });

    query.exec(function (err, provider) {
        if (err) {
            next(mongooseUtil.parseError(err, Provider.modelName));
        } else if (!provider) {
            next(new APIError('Failed to load Provider ' + id, 404));
        } else {
            req.provider = provider;
            next();
        }
    });
};

/**
 * ## Provider Authorization Middleware
 *
 * Determine if the resolved [Provider](../models/provider.js.html) is authorized for the request.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.hasAuthorization = function (req, res, next) {
    // Ensures the current user is the owner of the provider being looked up.
    if (req.provider.user.id !== req.user.id) {
        next(new APIError('User is not authorized', 403));
    } else {
        next();
    }
};