// # logs.js
// _app/controllers/_

// Logs Controller.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    logger = require('../providers/log/logger').get('Log-Controller'),
    _ = require('lodash'),
    APIError = require('../errors/api-error'),
    mongooseUtil = require('../util/mongoose'),
    moment = require('moment');
/**
 * ## List Logs
 *
 * List all Logs
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.list = function (req, res, next) {
    var from, until;

    // Create dates out of query params.
    // If not defined, we create invalid dates.
    from = moment(req.query.from || 'none');
    until = moment(req.query.until || 'none');

    // Query the logger.
    logger.query({
        limit: req._pagination.limit,
        start: req._pagination.skip,
        order: req.query.order || 'desc',
        from: (from.isValid()) ? from.toDate() : null,
        until: (until.isValid()) ? until.toDate() : null
    }, function (err, results) {
        if (err) {
            next(new APIError(err));
        } else {
            res.setPaginationValues(-1, -1);
            res.json((results[req.query.source] || results.mongodb) || []);
        }
    });
};
