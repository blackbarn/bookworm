// # books.js
// _app/controllers/_

// Books Controller.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    Book = mongoose.model('Book'),
    Author = mongoose.model('Author'),
    _ = require('lodash'),
    mongooseUtil = require('../util/mongoose'),
    APIError = require('../errors/api-error'),
    Provider = mongoose.model('Provider'),
    Q = require('q');


/**
 * ## Create a book
 *
 * Creates a [Book](../models/book.js.html) when provided with valid book `JSON`.
 * If the book has author populated, attempt to assign the book to that author.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.create = function (req, res, next) {
    // Create a book object
    var book = new Book(req.body);

    // Assign the current authenticated user to this book.
    book.user = req.user;

    // Retrieve the intended provider, or grab the default.
    Provider.getProviderOrDefault(book.provider, req.user).then(function (provider) {
        // Set provider
        book.provider = provider;

        // Assign this book to the given author
        book.assignAuthorById(req.author._id).then(function () {
            res.json(201, book);
        }).fail(function (err) {
            next(mongooseUtil.parseError(err, Book.modelName));
        });
    }, function (err) {
        // Error when getting provider
        next(mongooseUtil.parseError(err, Author.modelName));
    });
};

/**
 * ## Show current book
 *
 * Simply responds with the resolved book. See further below for the book middleware.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 */
exports.read = function (req, res) {
    res.json(req.book);
};

/**
 * ## Update a book
 *
 * Updates the resolved [Book](../models/book.js.html) with `JSON` provided in the request body.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.update = function (req, res, next) {
    var book = req.book;

    // Disallow changing of user
    delete req.body.user;
    delete req.body.author;

    // Extend the resolved book with data from the body. Doing this we allow partial updates.
    book = _.extend(book, req.body);

    // Save the book
    book.save(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Book.modelName));
        } else {
            res.json(book);
        }
    });
};

/**
 * ## Update multiple books
 *
 * Updates multiple [Book](../models/book.js.html) objects given an array of valid `JSON`.
 * Required fields are `id` and `status`. Only `status` is updated in this manner.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.updateMultiple = function (req, res, next) {
    if (_.isArray(req.body)) {
        Q.all(req.body.map(function (bookData) {
            return Q.ninvoke(Book, 'findOne', {
                _id: bookData._id,
                user: req.user
            }).then(function (book) {
                if (book) {
                    book.status = bookData.status;
                }
                return Q.ninvoke(book, 'save').then(function () {
                    return book;
                });
            });
        })).then(function (results) {
            res.json(_.compact(results));
        }).fail(next).done();
    } else {
        res.json(new APIError('Payload is not an array'));
    }
};

/**
 * ## Delete a book
 *
 * Remove the [Book](../models/book.js.html) completely.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.remove = function (req, res, next) {
    var book = req.book;

    book.remove(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Book.modelName));
        } else {
            res.send(204);
        }
    });
};

/**
 * ## List Books
 *
 * List all [Books](../models/book.js.html), sorted by created date descending.
 * Also populate the user and author for each book.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.list = function (req, res, next) {
    var query = Book.getSearchProperties(req.query);
    if (req.author) {
        query.author = req.author.id;
    }
    if (!query.status) {
        query.status = {
            $ne: 'excluded'
        };
    }

    Book.paginate(query, req._pagination.page, req._pagination.perPage, function (err, pageCount, books, itemCount) {
        if (err) {
            next(mongooseUtil.parseError(err, Book.modelName));
        } else {
            res.setPaginationValues(pageCount, itemCount);
            res.json(books);
        }
    }, {
        sortBy: req._sorting.sortMongo,
        populate: req._expanding.populate
    });
};

/**
 * ## Book Middleware
 *
 * Using the `id` in the route path, attempt to look up the [Book](../models/book.js.html).
 * Also populate the user and author for the book.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 * @param {ObjectId} bookId Book ID
 */
exports.bookByID = function (req, res, next, bookId) {
    var query;

    query = Book.findById(bookId);

    _.forEach(req._expanding.populate, function (populate) {
        query.populate(populate);
    });

    query.exec(function (err, book) {
        if (err) {
            next(mongooseUtil.parseError(err, Book.modelName));
        } else if (!book) {
            next(new APIError('Failed to load ' + Book.modelName + ' ' + bookId, 404));
        } else {
            req.book = book;
            next();
        }
    });
};

/**
 * ## Book Authorization Middleware
 *
 * Determine if the resolved [Book](../models/book.js.html) is authorized for the request.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.hasAuthorization = function (req, res, next) {
    // Ensures the current user is the owner of the book being looked up.
    if (req.book.user.id !== req.user.id) {
        next(new APIError('User is not authorized', 403));
    } else {
        next();
    }
};