// # books.js
// _app/controllers/providers_

// Books Provider Controller.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var BooksProvider = require('../../providers/search/books'),
    booksProvider = new BooksProvider(),
    APIError = require('../../errors/api-error'),
    _ = require('lodash');

/**
 * ## Search for books
 *
 * Search for books from the books provider
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.find = function (req, res, next) {

    if (req.query.title || req.query.author) {
        req.query.q = '';
        if (req.query.title) {
            req.query.q = ' intitle:' + req.query.title;
        }
        if (req.query.author) {
            req.query.q = req.query.q + ' inauthor:' + req.query.author;
        }

    }
    if (req._sorting.sort === '') {
        req._sorting.sort = 'relevance';
    }
    booksProvider.find(_.extend(req.query, {
        startIndex: req._pagination.skip,
        maxResults: req._pagination.limit,
        orderBy: req._sorting.sort
    }))
        .then(function (result) {
            res.json(result);
        }, function (err) {
            err.statusCode = 400;
            next(new APIError(err));
        });
};

/**
 * ## Search for authors
 *
 * Search for authors from the books provider
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.findAuthors = function (req, res, next) {
    booksProvider.findAuthors(_.extend(req.query, {
        startIndex: req._pagination.skip,
        maxResults: req._pagination.limit
    }))
        .then(function (result) {
            res.json(result);
        }, function (err) {
            err.statusCode = 400;
            next(new APIError(err));
        });
};
