// # processors.js
// _app/controllers/providers_

// Processors Provider Controller.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var ProcessorsProvider = require('../../providers/processor/processors'),
    processorsProvider = new ProcessorsProvider(),
    APIError = require('../../errors/api-error'),
    _ = require('lodash');

/**
 * ## start
 *
 * Start Processor
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.start = function (req, res, next) {

    processorsProvider.process({})
        .then(function (result) {
            res.json(result);
        }, function (err) {
            err.statusCode = 400;
            next(new APIError(err));
        });
};
