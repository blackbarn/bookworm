// # releases.js
// _app/controllers/providers_

// Releases Provider Controller.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var ReleasesProvider = require('../../providers/search/releases'),
    releasesProvider = new ReleasesProvider(),
    APIError = require('../../errors/api-error'),
    _ = require('lodash');

/**
 * ## Search for releases
 *
 * Search for releases from the releases provider
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.find = function (req, res, next) {
    releasesProvider.find(_.extend(req.query, {
        limit: req._pagination.limit,
        offset: req._pagination.skip
    }))
        .then(function (result) {
            res.json(result);
        }, function (err) {
            err.statusCode = 400;
            next(new APIError(err));
        });
};
