// # releases.js
// _app/controllers/_

// Releases Controller.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    Release = mongoose.model('Release'),
    _ = require('lodash'),
    APIError = require('../errors/api-error'),
    mongooseUtil = require('../util/mongoose'),
    Provider = mongoose.model('Provider'),
    Q = require('q');


/**
 * ## Create a release
 *
 * Creates a [Release](../models/release.js.html) when provided with valid author `JSON`
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.create = function (req, res, next) {
    // Create a release object
    var release = new Release(req.body);
    // Assign the current authenticated user to this release.
    release.user = req.user;

    // Retrieve the intended provider, or grab the default.
    Provider.getProviderOrDefault(release.provider, req.user).then(function (provider) {
        // Set provider
        release.provider = provider;

        // Assign this book to the given author
        release.assignBookById(req.book.id).then(function () {
            res.json(201, release);
        }).fail(function (err) {
            next(mongooseUtil.parseError(err, Release.modelName));
        });
    }, function (err) {
        // Error when getting provider
        next(mongooseUtil.parseError(err, Release.modelName));
    });
};

/**
 * ## Show current release
 *
 * Simply responds with the resolved release. See further below for the release middleware.
 *
 * @param {Object} req  Express request object
 * @param {Object} res Express response object
 */
exports.read = function (req, res) {
    res.json(req.release);
};

/**
 * ## Update a release
 *
 * Updates the resolved [Release](../models/release.js.html) with `JSON` provided in the request body.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.update = function (req, res, next) {
    var release = req.release;

    // Disallow changing of user
    delete req.body.user;
    delete req.body.book;

    // Extend the resolved release with data from the body. Doing this we allow partial updates.
    release = _.extend(release, req.body);

    // Save the release
    release.save(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Release.modelName));
        } else {
            res.json(release);
        }
    });
};

/**
 * ## Update multiple releases
 *
 * Updates multiple [Release](../models/release.js.html) objects given an array of valid `JSON`.
 * Required fields are `id` and `status`. Only `status` is updated in this manner.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.updateMultiple = function (req, res, next) {
    if (_.isArray(req.body)) {
        Q.all(req.body.map(function (releaseData) {
            return Q.ninvoke(Release, 'findOneAndUpdate', {
                _id: releaseData._id,
                user: req.user
            }, {
                status: releaseData.status
            });
        })).then(function (results) {
            res.json(_.compact(results));
        }).fail(next).done();
    } else {
        res.json(new APIError('Payload is not an array'));
    }
};

/**
 * ## Delete a release
 *
 * Remove the [Release](../models/release.js.html) completely.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.remove = function (req, res, next) {
    var release = req.release;

    release.remove(function (err) {
        if (err) {
            next(mongooseUtil.parseError(err, Release.modelName));
        } else {
            res.send(204);
        }
    });
};

/**
 * ## List of releases
 *
 * List all [Releases](../models/release.js.html), sorted by created date descending.
 * Also populate the user and book for each release.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.list = function (req, res, next) {

    var query = Release.getSearchProperties(req.query);

    if (req.book) {
        query.book = req.book.id;
    }

    Release.paginate(query, req._pagination.page, req._pagination.perPage, function (err, pageCount, releases, itemCount) {
        if (err) {
            next(mongooseUtil.parseError(err, Release.modelName));
        } else {
            res.setPaginationValues(pageCount, itemCount);
            res.json(releases);
        }
    }, {
        sortBy: req._sorting.sortMongo,
        populate: req._expanding.populate
    });
};

/**
 * ## Release Middleware
 *
 * Using the `id` in the route path, attempt to look up the [Release](../models/release.js.html).
 * Also populate the user and book of the release.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 * @param {ObjectId} id  Release ID
 */
exports.releaseByID = function (req, res, next, id) {
    var query;

    query = Release.findById(id);

    _.forEach(req._expanding.populate, function (populate) {
        query.populate(populate);
    });

    query.exec(function (err, release) {
        if (err) {
            next(mongooseUtil.parseError(err, Release.modelName));
        } else if (!release) {
            next(new APIError('Failed to load Release ' + id, 404));
        } else {
            req.release = release;
            next();
        }
    });
};

/**
 * ## Release Authorization Middleware
 *
 * Determine if the resolved [Release](../models/release.js.html) is authorized for the request.
 *
 * @param {Object} req Express request object
 * @param {Object} res Express response object
 * @param {Function} next Middleware callback
 */
exports.hasAuthorization = function (req, res, next) {
    // Ensures the current user is the owner of the release being looked up.
    if (req.release.user.id !== req.user.id) {
        next(new APIError('User is not authorized', 403));
    } else {
        next();
    }
};