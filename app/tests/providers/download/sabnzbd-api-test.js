// # sabnzbd-api-test.js
// _app/tests/providers/download/_

// SABnzbd API Tests.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var chai = require('chai'),
    Q = require('q'),
    request = require('request'),
    sinon = require('sinon'),
    apiDefaults = require('../../../providers/download/sabnzbd/sabnzbd-api-defaults'),
    SabnzbdAPI = require('../../../providers/download/sabnzbd/sabnzbd-api');

// Use the `chai-as-promised` plugin.
chai.use(require('chai-as-promised'));

// ## Globals
var api, deferred;

/**
 * ## Unit Tests
 *
 * SABnzbd API Unit Tests
 */
describe('SABnzbd API Unit Tests:', function () {

    // Set up
    beforeEach(function (done) {
        var cmdStub;
        deferred = Q.defer();
        api = new SabnzbdAPI(apiDefaults);
        cmdStub = sinon.stub(api._api, 'cmd');
        cmdStub.withArgs('addurl').returns(deferred.promise);
        done();
    });

    /**
     * ### init() Tests
     */
    describe('#init()', function () {
        it('should init ok with a host without protocol', function (done) {
            var fn = function () {
                var api = new SabnzbdAPI({
                    host: 'localhost'
                });
                chai.expect(api).to.exist;
            };
            chai.expect(fn).to.not.throw(Error);

            done();
        });

        it('should init ok with a host with protocol', function (done) {
            var fn = function () {
                var api = new SabnzbdAPI({
                    host: 'http://localhost'
                });
                chai.expect(api).to.exist;
            };
            chai.expect(fn).to.not.throw(Error);

            done();
        });

        it('should not throw an exception when not providing options object', function (done) {
            var fn = function () {
                var api = new SabnzbdAPI();
                chai.expect(api).to.exist;
            };
            chai.expect(fn).to.not.throw(Error);

            done();
        });

    });
    /**
     * ### add() Tests
     */
    describe('#add()', function () {
        it('should be able to add a nzb', function () {
            deferred.resolve({status: true});
            return api.add('url', 'category', 'name').then(function (result) {
                chai.expect(api._api.cmd.calledOnce).to.be.true;
                chai.expect(result).to.exist;
                chai.expect(result).to.be.an.instanceOf(Object);
                chai.expect(result.status).to.be.true;
            });
        });

        it('should be rejected when response status is false', function () {
            deferred.resolve({status: false});
            return chai.expect(api.add('', '', '')).to.eventually.be.rejected;
        });
    });

    // Clean up
    afterEach(function (done) {
        api._api.cmd.restore();
        done();
    });
});