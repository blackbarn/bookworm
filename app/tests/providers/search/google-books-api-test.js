// # google-books-api-test.js
// _app/tests/providers/search/_

// Google Books API Tests.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var chai = require('chai'),
    Q = require('q'),
    request = require('request'),
    sinon = require('sinon'),
    apiDefaults = require('../../../providers/search/google-books/google-books-api-defaults'),
    GoogleBooksAPI = require('../../../providers/search/google-books/google-books-api');

// Use the `chai-as-promised` plugin.
chai.use(require('chai-as-promised'));

/**
 * ## Unit Tests
 *
 * Google Books API Unit Tests
 */
describe('Google Books API Unit Tests:', function () {

    // Set timeout for default queue delay
    this.timeout(1600);

    // Set up
    beforeEach(function (done) {
        sinon.stub(request, 'get').yields(null, null, {items: [
            {title: 'book'}
        ], totalItems: 1});
        done();
    });

    /**
     * ### find() Tests
     */
    describe('#find()', function () {
        it('should be able to find books', function () {
            var api = new GoogleBooksAPI({
                useSharedQueue: false,
                requestQueueDelay: 0
            });
            return api.find({q: 'book'}).then(function (result) {
                chai.expect(request.get.calledOnce).to.be.true;
                chai.expect(result).to.exist;
                chai.expect(result).to.be.an.instanceOf(Object);
            });
        });

        it('should be able to find books without a delay', function () {
            //noinspection JSPotentiallyInvalidUsageOfThis
            this.timeout(100);
            var api = new GoogleBooksAPI({
                useSharedQueue: false,
                requestQueueDelay: 0
            });
            return api.find({q: 'book'}).then(function (result) {
                chai.expect(request.get.calledOnce).to.be.true;
                chai.expect(result).to.exist;
                chai.expect(result).to.be.an.instanceOf(Object);
            });
        });

        it('should be able to find books with a delay', function () {
            //noinspection JSPotentiallyInvalidUsageOfThis
            this.timeout(2100);
            var api = new GoogleBooksAPI({
                useSharedQueue: false,
                requestQueueDelay: 2000
            });
            return api.find({q: 'book'}).then(function (result) {
                chai.expect(request.get.calledOnce).to.be.true;
                chai.expect(result).to.exist;
                chai.expect(result).to.be.an.instanceOf(Object);
            });
        });

        it('should be able to handle response errors', function () {
            request.get.restore();
            sinon.stub(request, 'get').yields(null, null, { error: { errors: []}});
            var api = new GoogleBooksAPI({
                useSharedQueue: false,
                requestQueueDelay: 0
            });
            return api.find({q: 'book'}).then(function (result) {
                chai.expect(request.get.calledOnce).to.be.true;
                chai.expect(result).to.exist;
                chai.expect(result).to.be.an.instanceOf(Object);
                chai.expect(result.error).to.exist;
            });
        });

        it('should be able to find books across multiple pages', function () {
            var api = new GoogleBooksAPI({
                useSharedQueue: false,
                requestQueueDelay: 0
            }, {
                pagingQueryLimit: 2
            });
            return api.find({q: 'book'}).then(function (result) {
                chai.expect(request.get.calledTwice).to.be.true;
                chai.expect(result).to.exist;
                chai.expect(result).to.be.an.instanceOf(Array);
                chai.expect(result).to.have.lengthOf(2);
            });
        });

        it('should stop paging when return result is empty', function () {
            request.get.restore();
            sinon.stub(request, 'get').yields(null, null, {items: [
                {title: 'book'}
            ], totalItems: 0});
            var api = new GoogleBooksAPI({
                useSharedQueue: false,
                requestQueueDelay: 0
            }, {
                pagingQueryLimit: 3
            });
            return api.find({q: 'book'}).then(function (result) {
                chai.expect(request.get.calledOnce).to.be.true;
                chai.expect(result).to.exist;
                chai.expect(result).to.be.an.instanceOf(Array);
                chai.expect(result).to.have.lengthOf(1);
            });
        });

        it('should stop paging when there is an error', function () {
            request.get.restore();
            sinon.stub(request, 'get').yields(new Error('Request Error'));
            var api, promise;
            api = new GoogleBooksAPI({
                useSharedQueue: false,
                requestQueueDelay: 0
            }, {
                pagingQueryLimit: 3
            });
            promise = api.find({q: 'book'});

            return promise.fail(function (err) {
                chai.expect(err).to.exist;
                chai.expect(request.get.calledOnce).to.be.true;
            });
        });

        it('should should queue up requests across instances when using shared queue', function () {
            //noinspection JSPotentiallyInvalidUsageOfThis
            this.timeout(3100);

            var api1, api2;

            api1 = new GoogleBooksAPI({
                useSharedQueue: false
            });

            api2 = new GoogleBooksAPI({
                useSharedQueue: true
            });
            return Q.all([api1.find({q: 'book'}), api2.find({q: 'book'})])
                .spread(function (one, two) {
                    chai.expect(request.get.calledTwice).to.be.true;
                    chai.expect(one).to.exist;
                    chai.expect(two).to.exist;
                });
        });
    });

    /**
     * ### findOne() Tests
     */
    describe('#findOne()', function () {

        it('should be able to find one book', function () {
            //noinspection JSPotentiallyInvalidUsageOfThis
            this.timeout(500);
            var api = new GoogleBooksAPI({
                requestQueueDelay: 0,
                useSharedQueue: false
            });
            return api.findOne('someId', {}).then(function (result) {
                chai.expect(request.get.calledOnce).to.be.true;
                chai.expect(result).to.exist;
                chai.expect(result).to.be.an.instanceOf(Object);
            });
        });

        it('should should queue up requests across instances when using shared queue', function () {
            //noinspection JSPotentiallyInvalidUsageOfThis
            this.timeout(3100);

            var api1, api2;

            api1 = new GoogleBooksAPI({
                useSharedQueue: false
            });

            api2 = new GoogleBooksAPI({
                useSharedQueue: true
            });
            Q.all([api1.findOne('xyz', {}), api2.findOne('xyz', {})])
                .spread(function (one, two) {
                    chai.expect(request.get.calledTwice).to.be.true;
                    chai.expect(one).to.exist;
                    chai.expect(two).to.exist;

                });
        });
    });

    // Clean up
    afterEach(function (done) {
        request.get.restore();
        done();
    });
});