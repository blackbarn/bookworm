// # cast-test.js
// _app/tests/util/_

// Cast Tests.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var chai = require('chai'),
    cast = require('../../util/cast');

// Use the `chai-as-promised` plugin.
chai.use(require('chai-as-promised'));

// ## Globals

/**
 * ## Unit Tests
 *
 * Cast Tests
 */
describe('Cast Unit Tests:', function () {
    /**
     * ### bool() Tests
     */
    describe('#bool()', function () {

        it('should be able to convert truthy and falsy values to booleans', function (done) {
            chai.expect(cast.bool('yes')).to.be.true;
            chai.expect(cast.bool('true')).to.be.true;
            chai.expect(cast.bool('no')).to.be.false;
            chai.expect(cast.bool('false')).to.be.false;
            chai.expect(cast.bool('on')).to.be.true;
            chai.expect(cast.bool('off')).to.be.false;
            chai.expect(cast.bool('1')).to.be.true;
            chai.expect(cast.bool('0')).to.be.false;
            chai.expect(cast.bool(true)).to.be.true;
            done();
        });

        it('should return the default value when parsing fails', function (done) {
            chai.expect(cast.bool('something_else', false)).to.be.false;
            chai.expect(cast.bool('xyz', true)).to.be.true;
            done();
        });
    });

    /**
     * ### string() Tests
     */
    describe('#string()', function () {

        it('should be able to convert values to strings', function (done) {
            chai.expect(cast.string('hello')).to.eql('hello');
            chai.expect(cast.string(false)).to.eql('false');
            chai.expect(cast.string(100)).to.eql('100');
            done();
        });

        it('should return the default value when null or undefined', function (done) {
            chai.expect(cast.string(null, 'default value')).to.eql('default value');
            chai.expect(cast.string(undefined, 'default value')).to.eql('default value');
            done();
        });
    });

    /**
     * ### int() Tests
     */
    describe('#int()', function () {

        it('should be able to convert values to integers', function (done) {
            chai.expect(cast.int('2345', 10)).to.eql(2345);
            chai.expect(cast.int(1.9), 10).to.eql(1);
            done();
        });

        it('should return the default value when parsing junk', function (done) {
            chai.expect(cast.int(null, 10, 'default value')).to.eql('default value');
            chai.expect(cast.int('asdf', 10, 'default value')).to.eql('default value');
            done();
        });
    });

    /**
     * ### float() Tests
     */
    describe('#float()', function () {

        it('should be able to convert values to flaots', function (done) {
            chai.expect(cast.float('1234.444', 10)).to.eql(1234.444);
            chai.expect(cast.float(1.9)).to.eql(1.9);
            done();
        });

        it('should return the default value when parsing junk', function (done) {
            chai.expect(cast.float(null, 'default value')).to.eql('default value');
            chai.expect(cast.float('asdf', 'default value')).to.eql('default value');
            done();
        });
    });


});