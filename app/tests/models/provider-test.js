// # provider-test.js
// _app/tests/models/_

// Provider Tests.
'use strict';

require('../../../server.js');

// ## Module Dependencies
// Include all the necessary node modules.
var chai = require('chai'),
    Q = require('q'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Author = mongoose.model('Author'),
    Book = mongoose.model('Book'),
    Provider = mongoose.model('Provider'),
    Release = mongoose.model('Release');

// Use the `chai-as-promised` plugin.
chai.use(require('chai-as-promised'));

// ## Globals
var provider, provider2, user, book, author, release;

/**
 * ## Unit Tests
 *
 * Provider Model Unit Tests
 */
describe('Provider Model Unit Tests:', function () {
    // Set up
    beforeEach(function (done) {
        user = new User({
            username: 'username',
            password: 'password'
        });


        user.save(function () {
            provider = new Provider({
                name: 'Provider Name',
                canonicalName: 'sabnzbd',
                user: user
            });

            provider2 = new Provider({
                name: 'Provider Name',
                canonicalName: 'sabnzbd',
                user: user
            });

            author = new Author({
                name: 'Author',
                user: user,
                provider: provider
            });


            book = new Book({
                title: 'Title',
                user: user,
                provider: provider,
                author: author
            });

            release = new Release({
                name: 'Name',
                canonicalName: 'cname',
                user: user,
                provider: provider,
                book: book
            });
            done();
        });
    });

    /**
     * ### save() Tests
     */
    describe('#save()', function () {
        it('should be able to save without problems', function () {
            return chai.expect(Q.ninvoke(provider, 'save')).to.eventually.be.fulfilled;
        });

        it('should be able to show an error when try to save without name', function () {
            provider.name = '';

            return chai.expect(Q.ninvoke(provider, 'save')).to.eventually.be.rejected;
        });
    });

    /**
     * ### getTypesEnum() Tests
     */
    describe('#getTypesEnum()', function () {
        it('should be able to retrieve a types enum', function (done) {
            var types = Provider.getTypesEnum();
            chai.expect(types).to.be.an.instanceOf(Object);
            done();
        });
    });

    /**
     * ### getNamesEnum() Tests
     */
    describe('#getNamesEnum()', function () {

        it('should be able to retrieve an enum of possible canonical names', function (done) {
            var names = Provider.getNamesEnum();
            chai.expect(names).to.be.an.instanceOf(Object);
            done();
        });
    });

    /**
     * ### assign\[Model\]() Tests
     *
     * Tests for assigning a book, author and release to a provider.
     */
    describe('#assign[Model]()', function () {
        it('should be able to assign a book', function () {

            return provider.assignBook(book)
                .then(function () {
                    chai.expect(book.provider.toString()).to.equal(provider._id.toString());
                    return Q.ninvoke(book, 'populate', 'provider');
                })
                .then(function (book) {
                    return chai.expect(book.provider).to.exist;
                });
        });

        it('should be able to assign a author', function () {

            return provider.assignAuthor(author)
                .then(function () {
                    chai.expect(author.provider.toString()).to.equal(provider._id.toString());
                    return Q.ninvoke(author, 'populate', 'provider');
                })
                .then(function (author) {
                    return chai.expect(author.provider).to.exist;
                });
        });

        it('should be able to assign a release', function () {
            return provider.assignRelease(release)
                .then(function () {
                    chai.expect(release.provider.toString()).to.equal(provider._id.toString());
                    return Q.ninvoke(release, 'populate', 'provider');
                })
                .then(function (release) {
                    return chai.expect(release.provider).to.exist;
                });
        });
    });

    // Clean up
    afterEach(function (done) {
        Provider.remove().exec();
        Author.remove().exec();
        Book.remove().exec();
        Release.remove().exec();
        User.remove().exec();
        done();
    });
});