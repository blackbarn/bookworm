// # book-test.js
// _app/tests/models/_

// Book Tests.
'use strict';

require('../../../server.js');

// ## Module Dependencies
// Include all the necessary node modules.
var chai = require('chai'),
    Q = require('q'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Author = mongoose.model('Author'),
    Book = mongoose.model('Book'),
    Provider = mongoose.model('Provider');

// Use the `chai-as-promised` plugin.
chai.use(require('chai-as-promised'));

// ## Globals
var user, book, book2, author, provider;

/**
 * ## Unit Tests
 *
 * Book Model Unit Tests
 */
describe('Book Model Unit Tests:', function () {
    // Set up
    beforeEach(function (done) {
        user = new User({
            username: 'username',
            password: 'password'
        });
        user.save(function () {
            provider = new Provider({
                name: 'provider',
                canonicalName: 'sabnzbd',
                user: user
            });
            provider.save(function () {
                author = new Author({
                    name: 'Author Name',
                    provider: provider,
                    user: user
                });
                author.save(function () {
                    book = new Book({
                        title: 'Book Title',
                        user: user,
                        provider: provider,
                        author: author
                    });

                    book2 = new Book({
                        title: 'Book Title 2',
                        user: user,
                        provider: provider,
                        author: author
                    });
                    done();
                });
            });

        });
    });

    /**
     * ### save() Tests
     */
    describe('#save()', function () {
        it('should be able to save without problems', function (done) {
            return book.save(function (err) {
                chai.expect(err).to.not.exist;
                done();
            });
        });

        it('should be able to show an error when try to save without title', function (done) {
            book.title = '';

            return book.save(function (err) {
                chai.expect(err).to.exist;
                done();
            });
        });

        it('should not be able to save two books with the same name that belong to the same author', function (done) {
            book.title = 'same';
            book2.title = 'same';
            book.author = author;
            book2.author = author;

            book.save(function (err) {
                chai.expect(err).to.not.exist;
                book2.save(function (err) {
                    chai.expect(err).to.exist;
                    done();
                });
            });
        });
    });

    /**
     * ### isWanted() Tests
     */
    describe('#isWanted()', function () {
        it('should be true when status is wanted', function (done) {
            book.status = 'wanted';
            chai.expect(book.isWanted()).to.be.true;
            done();
        });

        it('should be true when status is wanted_new', function (done) {
            book.status = 'wanted_new';
            chai.expect(book.isWanted()).to.be.true;
            done();
        });
    });

    /**
     * ### assign\[Model\]() Tests
     *
     * Tests for assigning a author and provider to a book.
     */
    describe('#assign[Model]()', function () {
        it('should be able to assign an author', function () {
            return book.assignAuthor(author)
                .then(function () {
                    chai.expect(book.author).to.exist;
                    chai.expect(book.author.toString()).to.equal(author._id.toString());
                    return Q.ninvoke(book, 'populate', 'author');
                })
                .then(function (author) {
                    chai.expect(author).to.exist;
                });
        });

        it('should be able to assign an author by id', function () {
            return Q.ninvoke(author, 'save')
                .then(function () {
                    return book.assignAuthorById(author._id);
                })
                .then(function () {
                    chai.expect(book.author).to.exist;
                    chai.expect(book.author.toString()).to.equal(author._id.toString());
                    return Q.ninvoke(book, 'populate', 'author');
                }).then(function (author) {
                    chai.expect(author).to.exist;
                });
        });

        it('should be able to assign a provider', function () {
            return book.assignProvider(provider)
                .then(function () {
                    chai.expect(book.provider).to.exist;
                });
        });
    });

    // Clean up
    afterEach(function (done) {
        Book.remove().exec();
        Author.remove().exec();
        Provider.remove().exec();
        User.remove().exec();
        done();
    });
});