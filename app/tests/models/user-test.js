// # user-test.js
// _app/tests/models/_

// User Tests.
'use strict';

require('../../../server.js');

// ## Module Dependencies
// Include all the necessary node modules.
var chai = require('chai'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Provider = mongoose.model('Provider');

// Use the `chai-as-promised` plugin.
chai.use(require('chai-as-promised'));

// ## Globals
var user, user2, user3;

/**
 * ## Unit Tests
 *
 * User Model Unit Tests
 */
describe('User Model Unit Tests:', function () {
    // Set up
    before(function (done) {
        user = new User({
            username: 'username',
            password: 'password',
            provider: 'local',
            apikey: 'a'
        });
        user2 = new User({
            username: 'username',
            password: 'password',
            provider: 'local',
            apikey: 'a'
        });
        user3 = new User({
            username: 'username2',
            password: 'password2',
            provider: 'local',
            apikey: 'a'
        });

        done();
    });

    /**
     * ### save() Tests
     */
    describe('#save()', function () {
        it('should begin with no users', function (done) {
            User.find({}, function (err, users) {
                chai.expect(users).to.have.lengthOf(0);
                done();
            });
        });

        it('should be able to save without problems', function (done) {
            user.save(done);
        });

        it('should fail to save an existing user again', function (done) {
            user.save();
            return user2.save(function (err) {
                chai.expect(err).to.exist;
                done();
            });
        });

        it('should be able to show an error when try to save without username', function (done) {
            user.username = '';
            return user.save(function (err) {
                chai.expect(err).to.exist;
                done();
            });
        });
    });

    // Clean up
    after(function (done) {
        Provider.remove().exec();
        User.remove().exec();
        done();
    });
});
