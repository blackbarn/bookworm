// # author-test.js
// _app/tests/models/_

// Author Tests.
'use strict';

require('../../../server.js');

// ## Module Dependencies
// Include all the necessary node modules.
var chai = require('chai'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Author = mongoose.model('Author'),
    Book = mongoose.model('Book'),
    Provider = mongoose.model('Provider');

// Use the `chai-as-promised` plugin.
chai.use(require('chai-as-promised'));

// ## Globals
var user, book, author, author2, provider;


/**
 * ## Unit Tests
 *
 * Author Model Unit Tests
 */
describe('Author Model Unit Tests:', function () {
    // Set up
    beforeEach(function (done) {
        user = new User({
            username: 'username',
            password: 'password'
        });
        user.save(function () {
            provider = new Provider({
                name: 'Provider',
                canonicalName: 'sabnzbd',
                user: user
            });
            provider.save(function () {
                author = new Author({
                    name: 'Author Name',
                    user: user,
                    provider: provider
                });

                author2 = new Author({
                    name: 'Author Name',
                    user: user,
                    provider: provider
                });

                book = new Book({
                    title: 'Title',
                    user: user,
                    provider: provider
                });
                done();
            });
        });

    });

    /**
     * ### save() Tests
     */
    describe('#save()', function () {
        it('should be able to save without problems', function (done) {
            return author.save(function (err) {
                chai.expect(err).to.not.exist;
                done();
            });
        });

        it('should be able to show an error when try to save without name', function (done) {
            author.name = '';

            return author.save(function (err) {
                chai.expect(err).to.exist;
                done();
            });
        });

        it('should not be able to create two authors with the same name (with same user)', function (done) {
            author.name = 'same';
            author2.name = 'same';
            author.save(function () {
                author2.save(function (err2) {
                    chai.expect(err2).to.exist;
                    done();
                });
            });
        });
    });

    /**
     * ### assign\[Model\]() Tests
     *
     * Tests for assigning a book and provider to an author.
     */
    describe('#assign[Model]()', function () {
        it('should be able to assign a book', function () {
            return chai.expect(author.assignBook(book)).to.eventually.exist;
        });

        it('should be able to assign a provider', function () {
            return chai.expect(author.assignProvider(provider)).to.eventually.exist;
        });
    });

    // Clean up
    afterEach(function (done) {
        Author.remove().exec();
        Book.remove().exec();
        Provider.remove().exec();
        User.remove().exec();
        done();
    });
});