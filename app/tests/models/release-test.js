// # release-test.js
// _app/tests/models/_

// Release Tests.
'use strict';

require('../../../server.js');

// ## Module Dependencies
// Include all the necessary node modules.
var chai = require('chai'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Book = mongoose.model('Book'),
    Provider = mongoose.model('Provider'),
    Release = mongoose.model('Release'),
    Author = mongoose.model('Author');

// Use the `chai-as-promised` plugin.
chai.use(require('chai-as-promised'));


// ## Globals
var user, release, release2, provider, book, author;

/**
 * ## Unit Tests
 *
 * Release Model Unit Tests
 */
describe('Release Model Unit Tests:', function () {
    // Set up
    beforeEach(function (done) {
        user = new User({
            username: 'username',
            password: 'password'
        });

        user.save(function () {
            release = new Release({
                name: 'Release Name',
                canonicalName: 'sabnzbd',
                user: user
            });

            release2 = new Release({
                name: 'Name',
                canonicalName: 'sabnzbd',
                user: user
            });

            provider = new Provider({
                name: 'test',
                canonicalName: 'sabnzbd',
                user: user
            });

            provider.save(function () {

                author = new Author({
                    name: 'test',
                    user: user
                });
                author.save(function () {
                    book = new Book({
                        title: 'title',
                        user: user,
                        author: author
                    });
                    book.save(done);
                });

            });

        });
    });

    /**
     * ### save() Tests
     */
    describe('#save()', function () {
        it('should be able to save without problems', function (done) {
            return release.save(function (err) {
                chai.expect(err).to.not.exist;
                done();
            });
        });

        it('should be able to show an error when try to save without name', function (done) {
            release.name = '';
            return release.save(function (err) {
                chai.expect(err).to.exist;
                done();
            });
        });

        it('should be able to show an error when try to save without canonicalName', function (done) {
            release.canonicalName = '';
            return release.save(function (err) {
                chai.expect(err).to.exist;
                done();
            });
        });

        it('should not allow more than one release for the same user to have the same canonicalName', function (done) {
            release.canonicalName = 'same';
            release2.canonicalName = 'same';
            release.save(function (err) {
                chai.expect(err).to.not.exist;
                release2.save(function (err) {
                    chai.expect(err).to.exist;
                    done();
                });
            });
        });
    });

    /**
     * ### assign\[Model\]() Tests
     *
     * Tests for assigning a book and provider to a release.
     */
    describe('#assign[Model]()', function () {
        it('should be able to assign a provider', function () {
            return release.assignProvider(provider)
                .then(function () {
                    chai.expect(release.provider).to.exist;
                });
        });

        it('should be able to assign a book', function () {
            return release.assignBook(book)
                .then(function () {
                    chai.expect(release.book).to.exist;
                });
        });
    });

    afterEach(function (done) {
        Release.remove().exec();
        User.remove().exec();
        done();
    });
});