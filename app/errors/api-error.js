// # api-error.js
// _app/errors/_

// APIError Module.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var util = require('util');

/**
 * ## APIError Constructor
 *
 * Creates a new APIError instance. All methods are called on once instance of this object.
 * Inherits from `Error`.
 *
 * @param {Error|string} msg Either message string or another error object.
 * @param {Number} [statusCode] Optional `HTTP` status code to assign to the error.
 */
var APIError = function (msg, statusCode) {
    // Call inherited constructor
    Error.call(this);

    var obj = {};

    // Define some properties
    this.name = 'APIError';
    this.memory = process.memoryUsage();
    this.statusCode = statusCode || 500;

    // Capture the stack trace
    Error.captureStackTrace(obj, this.constructor);

    // When in a development environment include the stack trace in the object
    if (process.env.NODE_ENV === 'development') {
       this.stack = obj.stack;
    }
    // When `msg` is an Error set values based on that Error
    if (msg instanceof Error) {
        this.name = this.name + ':' + msg.name;
        this.message = msg.message;
        this.code = msg.code;
        this.statusCode = msg.statusCode || msg.status || this.statusCode;
        this.verboseMessage = msg.verboseMessage;
    } else {
        // When `msg` is a string simply pass it through
        this.message = msg;
    }
};

/**
 * ## APIError.prototype.toJSON
 *
 * Custom toJSON for the error object
 *
 * @returns {Object} error as `JSON`
 */
APIError.prototype.toJSON = function () {
    return {
        name: this.name,
        message: this.message,
        verboseMessage: this.verboseMessage,
        memory: this.memory,
        stack: this.stack
    };
};

// Inherit from `Error`
util.inherits(APIError, Error);

// Export
module.exports = APIError;