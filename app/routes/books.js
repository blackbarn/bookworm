// # books.js
// _app/routes/_

// Books Route Definitions
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var users = require('../controllers/users'),
    books = require('../controllers/books'),
    authors = require('../controllers/authors');

/**
 * ## Register Routes
 *
 * Register routes on the Express application
 *
 * @param {Object} app Express application instance
 */
module.exports = function (app) {

    // Books Routes
    app.route('/books')
        .get(users.requiresLogin, books.list)
        .put(users.requiresLogin, books.updateMultiple);

    app.route('/books/:bookId')
        .put(users.requiresLogin, books.update)
        .get(users.requiresLogin, books.read)
        .delete(users.requiresLogin, books.remove);

    app.route('/authors/:authorId/books')
        .get(users.requiresLogin, books.list)
        .post(users.requiresLogin, books.create);

    app.route('/authors/:authorId/books/:bookId')
        .get(users.requiresLogin, books.read)
        .put(users.requiresLogin, books.hasAuthorization, books.update)
        .delete(users.requiresLogin, books.hasAuthorization, books.remove);

    // Finish by binding the middleware
    app.param('bookId', books.bookByID);
    app.param('authorId', authors.authorByID);
};