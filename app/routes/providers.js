// # providers.js
// _app/routes/_

// Providers Route Definitions
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var users = require('../controllers/users'),
    providers = require('../controllers/providers'),
    booksProvider = require('../controllers/providers/books'),
    releasesProvider = require('../controllers/providers/releases');

/**
 * ## Register Routes
 *
 * Register routes on the Express application
 *
 * @param {Object} app Express application instance
 */
module.exports = function (app) {

    // Providers Routes
    app.route('/providers')
        .get(users.requiresLogin, providers.list)
        .post(users.requiresLogin, providers.create);

    app.route('/providers/books')
        .get(users.requiresLogin, booksProvider.find);

    app.route('/providers/authors')
        .get(users.requiresLogin, booksProvider.findAuthors);

    app.route('/providers/releases')
        .get(users.requiresLogin, releasesProvider.find);

    // Providers by `id` routes
    app.route('/providers/:providerId')
        .get(users.requiresLogin, providers.read)
        .put(users.requiresLogin, providers.hasAuthorization, providers.update)
        .delete(users.requiresLogin, providers.hasAuthorization, providers.remove);

    app.route('/providers/:providerId/process')
        .put(users.requiresLogin, providers.hasAuthorization, providers.process);

    // Finish by binding the Provider middleware
    app.param('providerId', providers.providerByID);
};