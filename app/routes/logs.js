// # logs.js
// _app/routes/_

// Logs Route Definitions
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var users = require('../controllers/users'),
    logs = require('../controllers/logs');

/**
 * ## Register Routes
 *
 * Register routes on the Express application
 *
 * @param {Object} app Express application instance
 */
module.exports = function (app) {

    // Logs Routes
    app.route('/logs')
        .get(users.requiresLogin, logs.list);

};