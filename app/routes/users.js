// # users.js
// _app/routes/_

// Users Route Definitions
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var users = require('../controllers/users');

/**
 * ## Register Routes
 *
 * Register routes on the Express application
 *
 * @param {Object} app Express application instance
 */
module.exports = function (app) {
    // User Routes
    app.route('/users/me').get(users.me);
    app.route('/users').put(users.update);
    app.route('/users/password').post(users.changePassword);

    // Setting up the users api
    app.route('/auth/signup').post(users.signup);
    app.route('/auth/signin').post(users.signin);
    app.route('/auth/signout').get(users.signout);

    // Finish by binding the user middleware
    app.param('userId', users.userByID);
};
