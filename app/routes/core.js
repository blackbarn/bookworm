// # core.js
// _app/routes/_

// Core Route Definitions
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var core = require('../controllers/core');

/**
 * ## Register Routes
 *
 * Register routes on the Express application
 *
 * @param {Object} app Express application instance
 */
module.exports = function (app) {
    // Root routing
    app.route('/').get(core.index);
};