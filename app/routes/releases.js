// # releases.js
// _app/routes/_

// Releases Route Definitions
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var users = require('../controllers/users'),
    releases = require('../controllers/releases'),
    authors = require('../controllers/authors'),
    books = require('../controllers/books');

/**
 * ## Register Routes
 *
 * Register routes on the Express application
 *
 * @param {Object} app Express application instance
 */
module.exports = function (app) {


    // Releases Routes
    app.route('/releases')
        .get(users.requiresLogin, releases.list)
        .put(users.requiresLogin, releases.updateMultiple);

    app.route('/releases/:releaseId')
        .put(users.requiresLogin, releases.update)
        .get(users.requiresLogin, releases.read)
        .delete(users.requiresLogin, releases.remove);

    app.route('/authors/:authorId/books/:bookId/releases')
        .get(users.requiresLogin, releases.list)
        .post(users.requiresLogin, releases.create);

    app.route('/authors/:authorId/books/:bookId/releases/:releaseId')
        .get(users.requiresLogin, releases.read)
        .put(users.requiresLogin, releases.hasAuthorization, releases.update)
        .delete(users.requiresLogin, releases.hasAuthorization, releases.remove);

    // Finish by binding the Release middleware
    app.param('releaseId', releases.releaseByID);
    app.param('bookId', books.bookByID);
    app.param('authorId', authors.authorByID);
};