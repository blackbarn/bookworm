// # authors.js
// _app/routes/_

// Authors Route Definitions
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var users = require('../controllers/users'),
    authors = require('../controllers/authors');

/**
 * ## Register Routes
 *
 * Register routes on the Express application
 *
 * @param {Object} app Express application instance
 */
module.exports = function (app) {

    // Authors Routes
    app.route('/authors')
        .get(users.requiresLogin, authors.list)
        .post(users.requiresLogin, authors.create)
        .put(users.requiresLogin, authors.updateMultiple);

    // Authors by `id` routes
    app.route('/authors/:authorId')
        .get(users.requiresLogin, authors.read)
        .put(users.requiresLogin, authors.hasAuthorization, authors.update)
        .delete(users.requiresLogin, authors.hasAuthorization, authors.remove);

    // Finish by binding the Author middleware
    app.param('authorId', authors.authorByID);
};