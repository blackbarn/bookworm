// # provider.js
// _app/models/_

// Provider Model.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    mongooseUtil = require('../util/mongoose'),
    Q = require('q'),
    Schema = mongoose.Schema,
    _ = require('lodash'),
    later = require('later'),
    logger = require('../providers/log/logger').get('Provider-Schema'),
    GoogleBooksProvider = require('../providers/search/google-books/google-books'),
    NewznabProvider = require('../providers/search/newznab/newznab'),
    SabnzbdProvider = require('../providers/download/sabnzbd/sabnzbd'),
    PostProcessProvider = require('../providers/processor/post-processor/post-processor'),
    PushoverProvider = require('../providers/notify/pushover/pushover');

// ## General Variables
// Define some enumeration values.
var types = {
        NZB_DOWNLOAD: 'nzb-download',
        BOOK_SEARCH: 'book-search',
        RELEASE_SEARCH: 'release-search',
        NOTIFY: 'notify',
        UTIL: 'util',
        PROCESSOR: 'processor'
    },
    providers = {
        GOOGLE_BOOKS: {
            name: 'google_books',
            type: types.BOOK_SEARCH,
            defaultDetails: {
                query: {
                    key: ''
                },
                cache: {
                    maxAge: 60000
                },
                filters: {
                    isbn: true,
                    description: true,
                    languages: 'en',
                    ignoredWords: ''
                },
                paging: {
                    refreshBooks: 2,
                    refreshNewBooks: 1,
                    searchBooks: 1,
                    searchAuthors: 1
                }
            }
        },
        GOOD_READS: {
            name: 'good_reads',
            type: types.BOOK_SEARCH,
            defaultDetails: {}
        },
        SABNZBD: {
            name: 'sabnzbd',
            type: types.NZB_DOWNLOAD,
            defaultDetails: {
                apikey: '',
                category: 'default',
                host: ''
            }
        },
        PUSHOVER: {
            name: 'pushover',
            type: types.NOTIFY,
            defaultDetails: {
                api: {
                    uri: 'https://api.pushover.net/1/messages.json',
                    token: '',
                    user: '',
                    priority: 0
                },
                onSnatch: false,
                onDownload: true
            }
        },
        NEWZNAB: {
            name: 'newznab',
            type: types.RELEASE_SEARCH,
            defaultDetails: {
                query: {
                    apikey: '',
                    maxage: 500
                },
                cache: {
                    maxAge: 60000
                },
                request: {
                    apiUrl: undefined
                },
                filters: {
                    ignoredWords: ''
                }
            }
        },
        POST_PROCESS: {
            name: 'post_process',
            type: types.PROCESSOR,
            defaultDetails: {
                frequency: 10,
                downloadDirectory: '',
                destinationDirectory: '',
                folderFormat: '$Author-$Title($Year)',
                fileFormat: '$Author-$Title($Year)',
                keepOriginalFiles: true,
                directoryPermissions: '0755',
                opfName: 'metadata.opf',
                schedule: ''
            }
        },
        LOCAL: {
            name: 'local',
            type: types.UTIL,
            defaultDetails: {}
        }
    },
    defaultProvider = {
        name: 'Local-Default',
        canonicalName: providers.LOCAL.name,
        description: 'Default Provider. No provider was specified so this was assigned.'
    },
    timers = {};

var app = null;

module.exports = function (instance) {
    app = instance;
};
/**
 * ## Provider Schema
 *
 * Define the Provider Model Schema.
 * @name Provider
 */
var ProviderSchema = new Schema({
    name: {
        type: String,
        default: '',
        required: 'Please fill Provider name',
        trim: true
    },
    enabled: {
        type: Boolean,
        default: true
    },
    status: {
        type: String,
        enum: ['none', 'running', 'stopped'],
        default: 'none',
        trim: true
    },
    canonicalName: {
        type: String,
        enum: Object.keys(providers).map(function (k) {
            return providers[k].name;
        }),
        lowercase: true,
        trim: true,
        required: 'Please provide a provider canonical name',
        index: true
    },
    description: {
        type: String,
        trim: true,
        default: ''
    },
    type: {
        type: String,
        enum: Object.keys(types).map(function (k) {
            return types[k];
        }),
        lowercase: true,
        trim: true,
        index: true
    },
    providerDetails: {},
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: 'Provider must be associated with a user'
    }
});

// Save previous status value, and always treat a status set as a modification.
ProviderSchema.path('status').set(function (value) {
    this._status = this.status;
    this.markModified('status');
    return value;
});

// Pre-save hook. Set provider details (mainly to ensure defaults are extended).
ProviderSchema.pre('save', function (next) {
    var details;
    details = _.clone(providers[this.canonicalName.toUpperCase()].defaultDetails, true);
    this.providerDetails = _.merge(details, this.providerDetails);
    next();
});

// Pre-save hook. Set provider details (mainly to ensure defaults are extended).
ProviderSchema.pre('save', function (next) {
    if (this.isModified('providerDetails')) {
        this.initializeSchedule();
    }
    next();
});

// Pre-Save hook to set the type given the provider canonical name automatically.
ProviderSchema.pre('save', function (next) {
    this.type = _.find(providers, function (provider) {
        return provider.name === this.canonicalName;
    }.bind(this)).type;
    next();
});

// Pre-save hook. Uses provider status to initiate async actions.
ProviderSchema.pre('save', function (next) {
    if (this.isModified('status')) {
        if (this._status === 'running' && this.status === 'running') {
            logger.log('warn', 'Provider %s already running', this.name);
        } else {
            switch (this.status) {
                case 'running':
                    this.process();
                    break;
            }
        }
    }
    next();
});

// Post-save hook. Emit instance
ProviderSchema.post('save', function (provider) {
    app.io.emit('provider', provider);
});

/**
 * ## ProviderSchema.statics.getTypesEnum
 *
 * Retriever Provider Types
 *
 * @returns {Object}
 */
ProviderSchema.statics.getTypesEnum = function () {
    return types;
};

/**
 * ## ProviderSchema.statics.getnamesEnum
 *
 * Retrieve Provider Canonical Name possibilities
 *
 * @returns {Object}
 */
ProviderSchema.statics.getNamesEnum = function () {
    return providers;
};

/**
 * ## ProviderSchema.statics.getProvidersByType
 *
 * Retrieves providers by their type
 *
 * @param {String} type Provider type
 * @param {Boolean} onlyEnabled Only retrieve enabled providers.
 * @returns {Promise.<Provider[], Error>}
 */
ProviderSchema.statics.getProvidersByType = function (type, onlyEnabled) {
    // Set up the query
    var query = {
        type: type
    };

    // When we only care about enabled providers, set the query.
    if (onlyEnabled) {
        query.enabled = true;
    }
    // Run it
    return Q.ninvoke(this, 'find', query);
};

/**
 * ## ProviderSchema.statics.getSearchProperties
 *
 * Retrieves properties for searching given a query param object.
 * It will only match properties of this schema not on the ignore list.
 *
 * @param {Object} query Express `req.query` object
 * @returns {Object}
 */
ProviderSchema.statics.getSearchProperties = function (query) {
    return mongooseUtil.getMatchingProperties(ProviderSchema.paths, query, ['providerDetails', 'user']);
};


/**
 * ## ProviderSchema.statics.getDefaultProvider
 *
 * Retrieves the default provider. Creates the default provider if it does not exist yet.
 *
 * @param {User} user User object to associate with the provider.
 * @returns {Promise.<Provider,Error>} Provider when fulfilled, error when rejected.
 */
ProviderSchema.statics.getDefaultProvider = function (user) {
    return Q.ninvoke(this, 'findOrCreate', {
        canonicalName: providers.LOCAL.name
    }, _.extend({}, defaultProvider, {user: user})).spread(function (provider) {
        return provider;
    });
};

/**
 * ## ProviderSchema.statics.getProviderOrDefault
 *
 * Retrieves a provider by id if it exists. If it does not exist it retrieves the default provider instead.
 *
 * @param {ObjectId} id Mongoose ObjectID for the provider
 * @param {User} user User object
 * @returns {Promise.<Provider,Error>} Provider when fulfilled, error when rejected.
 */
ProviderSchema.statics.getProviderOrDefault = function (id, user) {
    var Provider = this;
    if (!id) {
        return Provider.getDefaultProvider(user);
    } else {
        return Q.ninvoke(this, 'findById', id).then(function (provider) {
            if (!provider) {
                return Provider.getDefaultProvider(user);
            } else {
                return provider;
            }
        });
    }

};

/**
 * ## ProviderSchema.methods.initializeSchedule
 *
 * Initializes the schedule for the current provider.
 * It will stop any previous schedules, re-parse the schedule and attempt to schedule it.
 */
ProviderSchema.methods.initializeSchedule = function () {
    var schedule, timer;
    // Grab any current timer
    timer = timers[this._id];

    // When timer exists, notify and clear it.
    if (timer) {
        logger.log('info', '%s - Stopping previous schedule', this.name);
        timer.clear();
    }
    // Verify schedule is not empty
    if (!_.isEmpty(this.providerDetails.schedule)) {

        // Parse schedule
        schedule = later.parse.text(this.providerDetails.schedule);

        // Handle syntax errors
        if (schedule.error >= 0) {
            logger.log('error', '%s - Error parsing Provider schedule. Issue at character %s', this.name, schedule.error);
        } else {
            // Start the schedule
            logger.log('info', '%s - Initializing Schedule for provider', this.name);
            later.date.localTime();
            logger.log('info', '%s - Next occurrence at %s', this.name, later.schedule(schedule).next().toString());
            timers[this.id] = later.setInterval(this.process.bind(this), schedule);
        }
    } else {
        logger.log('warn', '%s - Provider schedule not set', this.name);
    }
};

/**
 * ## ProviderSchema.methods.assignBook
 *
 * Assign a provider to the passed in book
 *
 * @param {Object} book Book Object
 * @return {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
ProviderSchema.methods.assignBook = function (book) {
    var provider = this;
    return Q.ninvoke(this, 'save')
        .then(function () {
            book.provider = provider;
            return Q.ninvoke(book, 'save');
        });
};

/**
 * ## ProviderSchema.methods.assignAuthor
 *
 * Assign a provider to the passed in author
 *
 * @param {Object} author Author Object
 * @return {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
ProviderSchema.methods.assignAuthor = function (author) {
    var provider = this;
    return Q.ninvoke(this, 'save')
        .then(function () {
            author.provider = provider;
            return Q.ninvoke(author, 'save');
        });
};

/**
 * ## ProviderSchema.methods.assignRelease
 *
 * Assign a provider to the passed in release
 *
 * @param {Object} release Release Object
 * @return {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
ProviderSchema.methods.assignRelease = function (release) {
    var provider = this;
    return Q.ninvoke(this, 'save')
        .then(function () {
            release.provider = provider;
            return Q.ninvoke(release, 'save');
        });
};

/**
 * ## ProviderSchema.methods.getInstance
 *
 * Retrieves an instance of the proper provider object for this provider.
 *
 * @returns {Object}
 */
ProviderSchema.methods.getInstance = function () {
    switch (this.type) {
        case types.BOOK_SEARCH:
            switch(this.canonicalName) {
                case providers.GOOGLE_BOOKS.name:
                    return new GoogleBooksProvider(this);
            }
            break;
        case types.RELEASE_SEARCH:
            switch(this.canonicalName) {
                case providers.NEWZNAB.name:
                    return new NewznabProvider(this);
            }
            break;
        case types.NZB_DOWNLOAD:
            switch(this.canonicalName) {
                case providers.SABNZBD.name:
                    return new SabnzbdProvider(this);
            }
            break;
        case types.PROCESSOR:
            switch(this.canonicalName) {
                case providers.POST_PROCESS.name:
                    return new PostProcessProvider(this);
            }
            break;
        case types.NOTIFY:
            switch(this.canonicalName) {
                case providers.PUSHOVER.name:
                    return new PushoverProvider(this);
            }
            break;
    }
    return null;
};

/**
 * ## ProviderSchema.methods.process
 *
 * Execute this provider's process method.
 */
ProviderSchema.methods.process = function () {
    var instance;
    // Grab the object instance for this provider
    instance = this.getInstance();

    // Ensure it has a process method
    if (typeof instance.process === 'function') {
        // Execute process and handle finish state.
        instance.process().fin(function () {
            this.status = 'stopped';
            this.save(function (err) {
                if (err) {
                    logger.log('error', 'Error marking provider as stopped.', logger.errorAsJSON(err));
                }
            });
        }.bind(this));
    } else {
        logger.log('warn', 'Provider %s does not support processing.', this.name);
    }

};

// Timestamp Plugin
ProviderSchema.plugin(require('mongoose-timestamp'), {
    createdAt: 'created',
    updatedAt: 'updated'
});

// Pagination Plugin
ProviderSchema.plugin(require('./plugins/paginate'), {});

// Find or Create Plugin
ProviderSchema.plugin(require('mongoose-findorcreate'), {});

// Define indexes
ProviderSchema.index({canonicalName: 1, user: 1}, {unique: true});

// Register Model
mongoose.model('Provider', ProviderSchema);