// # release.js
// _app/models/_

// Release Model.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    Q = require('q'),
    mongooseUtil = require('../util/mongoose'),
    Schema = mongoose.Schema,
    uuid = require('node-uuid'),
    Book = mongoose.model('Book'),
    NzbsProvider = require('../providers/download/nzbs'),
    logger = require('../providers/log/logger').get('Release-Schema');

var statuses = {
    WANTED: 'wanted',
    AVAILABLE: 'available',
    DOWNLOADED: 'downloaded',
    SNATCHED: 'snatched',
    IGNORED: 'ignored'
};

var app = null;

module.exports = function (instance) {
    app = instance;
};

/**
 * ## Release Schema
 *
 * Define the Release Model Schema.
 * @name Release
 */
var ReleaseSchema = new Schema({
    externalId: {
        type: String,
        default: uuid.v4,
        trim: true,
        index: true
    },
    name: {
        type: String,
        default: '',
        required: 'Please fill Release name',
        trim: true
    },
    canonicalName: {
        type: String,
        trim: true,
        default: '',
        required: 'Please fill Release canonical name'
    },
    status: {
        type: String,
        enum: Object.keys(statuses).map(function (k) {
            return statuses[k];
        }),
        lowercase: true,
        trim: true,
        default: 'available'
    },
    size: {
        type: Number,
        default: 0
    },
    review: {
        type: String,
        default: '',
        trim: true
    },
    grabs: {
        type: Number,
        default: 0
    },
    providerData: {},
    provider: {
        type: Schema.ObjectId,
        ref: 'Provider'
    },
    description: {
        type: String,
        trim: true,
        default: ''
    },
    releaseDate: {
        type: Date
    },
    directory: {
        type: String,
        trim: true,
        default: ''
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    book: {
        type: Schema.ObjectId,
        ref: 'Book'
    }
});

// Ensure when setting status, even if it is the same value, it is tracked as modified.
ReleaseSchema.path('status').set(function (value) {
    this.markModified('status');
    return value;
});

// Pre save hook
ReleaseSchema.pre('save', function (next) {
    var Provider = mongoose.model('Provider');
    if (this.isModified('provider')) {
        Provider.findById(this.provider, function (err, provider) {
            if (err) {
                next(err);
            } else if (!provider) {
                next(new Error('Provider does not exist'));
            } else {
                next();
            }
        });
    } else {
        next();
    }
});

// Pre-save hook. Uses release status to initiate async actions.
ReleaseSchema.pre('save', function (next) {
    if (this.isModified('status') && this.status === statuses.WANTED) {
        this.download();
    }
    next();
});

// Post-save hook. Emit instance
ReleaseSchema.post('save', function (release) {
    app.io.emit('release', release);
});

/**
 * ## ReleaseSchema.statics.getSearchProperties
 *
 * Retrieves properties for searching given a query param object.
 * It will only match properties of this schema not on the ignore list.
 *
 * @param {Object} query Express `req.query` object
 * @returns {Object}
 */
ReleaseSchema.statics.getSearchProperties = function (query) {
    return mongooseUtil.getMatchingProperties(ReleaseSchema.paths, query, ['provider', 'user']);
};


/**
 * ## ReleaseSchema.statics.getReleaseForDirectory
 *
 * Retrieves a release corresponding to the given directory
 *
 * @param {String} directory Release Directory
 * @returns {Promise.<Release|Null, Error>} Release|Null when fulfilled, error when rejected.
 */
ReleaseSchema.statics.getReleaseForDirectory = function (directory) {
    return Q.fcall(function () {
        var regex, id, matches, Release;
        Release = this;
        // Regex to pluck the custom suffix we stick on releases. Grabs the id.
        regex = /\.bw\(([A-Za-z0-9\-]+)\)/g;
        // Try and match
        matches = regex.exec(directory);

        // Grab the id if there was a match
        if (matches && matches[1]) {
            id = matches[1];
        }
        if (id) {
            // When we have an ID, find the release.
            return Q.ninvoke(Release, 'findOne', {
                externalId: id
            }).then(function (release) {
                if (release) {
                    release.directory = directory;
                    return Q.ninvoke(release, 'save')
                        .then(function () {
                            return release;
                        });
                } else {
                    return null;
                }
            });
        } else {
            return null;
        }
    }.bind(this));

};

/**
 * ## ReleaseSchema.methods.assignProvider
 *
 * Assign a provider to an instance of a release
 *
 * @param {Object} provider Provider Object
 * @returns {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
ReleaseSchema.methods.assignProvider = function (provider) {
    this.provider = provider;
    return Q.ninvoke(this, 'save');
};

/**
 * ## ReleaseSchema.methods.download
 *
 * Download this release!
 */
ReleaseSchema.methods.download = function () {
    var nzbsProvider, release;
    release = this;
    nzbsProvider = new NzbsProvider();

    logger.log('info', 'Attempting to download release %s', release.name);

    nzbsProvider.add({
        url: release.providerData.link,
        name: release.canonicalName
    }).then(function () {
        Q.ninvoke(release, 'populate', 'book')
            .then(function (release) {
                release.book.status = 'snatched';
                release.status = 'snatched';
                release.book.notify(release.book.status);
                Q.all([Q.ninvoke(release.book, 'save'), Q.ninvoke(release, 'save')]).fail(function (err) {
                    logger.log('error', logger.errorAsJSON(err));
                });
            });
    });

};

/**
 * ## ReleaseSchema.methods.assignBook
 *
 * Assign this release to a book
 *
 * @param {Object} book Book Object
 * @returns {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
ReleaseSchema.methods.assignBook = function (book) {
    return this.assignBookById(book.id);
};

/**
 * ## ReleaseSchema.methods.assignBookById
 *
 * Assign a book given its id to an instance of a release
 *
 * @param {String} id The Author ID
 * @return {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
ReleaseSchema.methods.assignBookById = function (id) {
    var release = this;

    return Q.ninvoke(Book, 'findById', id)
        .then(function (book) {
            if (book) {
                return book.assignRelease(release);
            } else {
                throw new Error('Book does not exist');
            }
        });
};

/**
 * Determine if the release is retryable by checking its status.
 * @returns {Boolean} True if retryable.
 */
ReleaseSchema.methods.isRetryable = function () {
    return [statuses.SNATCHED, statuses.WANTED, statuses.DOWNLOADED].indexOf(this.status) !== -1;
};

/**
 * ## ReleaseSchema.methods.isAvailable
 *
 * Determines if the release is available
 *
 * @returns {Boolean}
 */
ReleaseSchema.methods.isAvailable = function () {
    return this.status === statuses.AVAILABLE;
};

/**
 * ## ReleaseSchema.methods.isWanted
 *
 * Determines if the release is available
 *
 * @returns {Boolean}
 */
ReleaseSchema.methods.isWanted = function () {
    return this.status === statuses.WANTED;
};

/**
 * ## ReleaseSchema.methods.isAvailable
 *
 * Determines if the release is available
 *
 * @returns {Boolean}
 */
ReleaseSchema.methods.isAvailable = function () {
    return this.status === statuses.AVAILABLE;
};

// Define compound index on `canonicalName` and `provider` as unique.
ReleaseSchema.index({canonicalName: 1, user: 1}, {unique: true});

// Timestamp Plugin
ReleaseSchema.plugin(require('mongoose-timestamp'), {
    createdAt: 'created',
    updatedAt: 'updated'
});

// Pagination Plugin
ReleaseSchema.plugin(require('./plugins/paginate'), {});

// Register Model
mongoose.model('Release', ReleaseSchema);