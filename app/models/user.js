// # user.js
// _app/models/_
// User Model.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    Q = require('q'),
    uuid = require('node-uuid'),
    Schema = mongoose.Schema,
    crypto = require('crypto'),
    Provider = mongoose.model('Provider');

/**
 * ## Validate Local Strategy password
 *
 * A Validation function for local strategy password
 *
 * @param {String} password Password to validate
 */
var validateLocalStrategyPassword = function (password) {
    return (this.provider !== 'local' || (password && password.length > 6));
};

var app = null;

module.exports = function (instance) {
    app = instance;
};

/**
 * ## User Schema
 *
 * Define the User Model Schema.
 * @name User
 */
var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: 'Please fill in a username',
        trim: true
    },
    apikey: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        default: '',
        validate: [validateLocalStrategyPassword, 'Password should be longer']
    },
    salt: {
        type: String
    },
    provider: {
        type: String,
        required: 'Provider is required'
    },
    roles: {
        type: [
            {
                type: String,
                enum: ['user', 'admin']
            }
        ],
        default: ['user']
    }
});

/**
 * ## UserSchema.pre `save`
 *
 * Hook a pre save method to hash the password
 *
 * @param {Function} next Middleware Callback
 */
UserSchema.pre('save', function (next) {
    if (this.password && this.password.length > 6) {
        this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
    }
    next();
});

/**
 * ## UserSchema.methods.hashPassword
 *
 * Create instance method for hashing a password
 *
 * @param {String} password Password to hash
 */
UserSchema.methods.hashPassword = function (password) {
    if (this.salt && password) {
        return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
    } else {
        return password;
    }
};

/**
 * ## UserSchema.methods.authenticate
 *
 * Create instance method for authenticating user
 *
 * @param {String} password Password to authenticate with
 */
UserSchema.methods.authenticate = function (password) {
    return this.password === this.hashPassword(password);
};


/**
 * ## UserSchema.methods.generateApiKey
 *
 * Generate a unique API Key
 *
 * @returns {String} api key
 */
UserSchema.methods.generateApiKey = function () {
    this.apikey = uuid.v4().replace(/-+/g, '');
};

/**
 * ## UserSchema.statics.findUniqueUsername
 *
 * Find possible not used username
 *
 * @param {String} username Username to check
 * @param {String} [suffix] Suffix to use when trying alternates
 * @param {Function} callback Callback
 */
UserSchema.statics.findUniqueUsername = function (username, suffix, callback) {
    var _this = this;
    var possibleUsername = username + (suffix || '');

    _this.findOne({
        username: possibleUsername
    }, function (err, user) {
        if (!err) {
            if (!user) {
                callback(possibleUsername);
            } else {
                return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
            }
        } else {
            callback(null);
        }
    });
};

// Timestamp Plugin
UserSchema.plugin(require('mongoose-timestamp'), {
    createdAt: 'created',
    updatedAt: 'updated'
});

// Register Model
mongoose.model('User', UserSchema);