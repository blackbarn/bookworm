// # author.js
// _app/models/_

// Author Model.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    mongooseUtil = require('../util/mongoose'),
    Q = require('q'),
    uuid = require('node-uuid'),
    logger = require('../providers/log/logger').get('Author-Schema'),
    BooksProvider = require('../providers/search/books'),
    _ = require('lodash'),
    jsonMask = require('json-mask');

var statuses = {
    REFRESH: 'refreshing',
    REFRESH_BOOKS: 'refreshing_books',
    REFRESH_NEW_BOOKS: 'refreshing_new_books',
    PAUSE: 'paused',
    RESUME: 'active'
};

var app = null;

module.exports = function (instance) {
    app = instance;
};

/**
 * ## Author Schema
 *
 * Define the Author Model Schema.
 * @name Author
 */
var AuthorSchema = new Schema({
    externalId: {
        type: String,
        trim: true,
        unique: true,
        default: uuid.v4
    },
    name: {
        type: String,
        default: '',
        required: 'Please fill Author name',
        trim: true
    },
    provider: {
        type: Schema.ObjectId,
        ref: 'Provider',
        required: 'Author must be associated with a provider'
    },
    relevance: {
        type: Number,
        default: 0
    },
    providerData: {},
    description: {
        type: String,
        trim: true,
        default: ''
    },
    status: {
        type: String,
        enum: Object.keys(statuses).map(function (k) {
            return statuses[k];
        }),
        lowercase: true,
        trim: true,
        default: 'active'
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: 'Author must be associated with a user'
    },
    books: [
        {
            type: Schema.ObjectId,
            ref: 'Book'
        }
    ]
});

AuthorSchema.path('status').set(function (value) {
    this._status = this.status;
    return value;
});

// Pre-save hook. Verifies a provider exists before saving.
AuthorSchema.pre('save', function (next) {
    var Provider = mongoose.model('Provider');
    if (this.isModified('provider')) {
        Provider.findById(this.provider, function (err, provider) {
            if (err) {
                next(err);
            } else if (!provider) {
                next(new Error('Provider does not exist'));
            } else {
                next();
            }
        });
    } else {
        next();
    }
});

// Pre-save hook. Uses author status to initiate async actions.
AuthorSchema.pre('save', function (next) {
    if (this.isModified('status')) {
        if (this._status.indexOf('refresh') === 0 && this.status.indexOf('refresh') === 0) {
            logger.log('warn', 'Author %s already being refreshed.', this.name);
        } else {
            switch (this.status) {
                case statuses.REFRESH:
                    this.refresh();
                    break;
                case statuses.REFRESH_BOOKS:
                    this.refreshBooks();
                    break;
                case statuses.REFRESH_NEW_BOOKS:
                    this.refreshBooks(true);
                    break;
            }
        }
    }
    next();
});

// Pre-remove hook. Removes all books for this author individually before removing author.
AuthorSchema.pre('remove', function (next) {
    Q.ninvoke(this, 'populate', { path: 'books'})
        .then(function (author) {
            return Q.all(author.books.map(function (book) {
                return Q.ninvoke(book, 'remove');
            }));
        }).then(function () {
            next();
        }).fail(next).done();
});

// Post-save hook. Emit instance
AuthorSchema.post('save', function (author) {
    app.io.emit('author', author);
});

/**
 * ## AuthorSchema.statics.getSearchProperties
 *
 * Retrieves properties for searching given a query param object.
 * It will only match properties of this schema not on the ignore list.
 *
 * @param {Object} query Express `req.query` object
 * @returns {Object}
 */
AuthorSchema.statics.getSearchProperties = function (query) {
    return mongooseUtil.getMatchingProperties(AuthorSchema.paths, query, ['books', 'user', 'provider', 'providerData']);
};

/**
 * ## AuthorSchema.methods.assignBook
 *
 * Assign a book to an instance of an author
 *
 * @param {Object} book Book Object
 * @returns {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
AuthorSchema.methods.assignBook = function (book) {
    var author = this,
        Author = mongoose.model('Author');

    book.author = author;

    return Q.ninvoke(book, 'save').then(function () {
        return Q.ninvoke(Author, 'update', {
            _id: author.id
        }, {
            $addToSet: {
                books: book.id
            }
        });
    });
};

/**
 * ## AuthorSchema.methods.assignProvider
 *
 * Assign a provider to an instance of an author
 *
 * @param {Object} provider Provider Object
 * @return {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
AuthorSchema.methods.assignProvider = function (provider) {
    this.provider = provider;
    return Q.ninvoke(this, 'save');
};

/**
 * ## AuthorSchema.methods.refresh
 *
 * Refresh the author's information
 *
 */
AuthorSchema.methods.refresh = function () {
    var booksProvider, author;

    author = this;
    booksProvider = new BooksProvider();

    logger.log('info', '%s - Refreshing', author.name);

    booksProvider.findAuthorById(author.externalId).then(function (foundAuthor) {
        if (author) {
            author.merge(foundAuthor);
            author.status = author._status;
            return Q.ninvoke(author, 'save');
        } else {
            logger.log('info', '%s - Not refreshed, could not locate author in the cloud', author.name);
        }
    }).then(function () {
        logger.log('info', '%s - Finished refreshing', author.name);
    }).fail(function (err) {
        logger.log('error', '%s - Error refreshing', author.name, logger.errorAsJSON(err));
    });
};

/**
 * ## AuthorSchema.methods.refreshBooks
 *
 * Refresh the author's books
 *
 * @param {Boolean} [newBooks] Refresh new books only?
 */
AuthorSchema.methods.refreshBooks = function (newBooks) {
    var Book, author, booksProvider;
    author = this;
    Book = mongoose.model('Book');
    booksProvider = new BooksProvider();

    logger.log('info', '%s - Refreshing books', this.name, { newBooks: !!newBooks });

    Q.ninvoke(author, 'populate', 'provider books')
        .then(function (author) {
            // Find books for this author
            return booksProvider.find({
                q: 'inauthor:' + author.name,
                pagingQueryLimit: author.provider.providerDetails && author.provider.providerDetails.paging && author.provider.providerDetails.paging.refreshBooks,
                orderBy: newBooks ? 'newest' : 'relevance'
            }).then(function (remoteBooks) {

                logger.log('debug', '%s - Retrieved %s remote books', author.name, remoteBooks.length);
                logger.log('debug', '%s - Attempting to merge %s remote books', author.name, remoteBooks.length);

                // Merge remote books gathered by the `booksProvider` into this authors books
                return Book.merge(author.books, remoteBooks)
                    .then(function (destination) {
                        var merged = 0;
                        destination.forEach(function (dest) {
                            if (dest._merged) {
                                merged = merged + 1;
                            }
                        });
                        logger.log('info', '%s - Merged %s remote books as they contained updated information', author.name, merged);
                        // Filter out any matching books. We want all new. Books that only exist in the source.
                        return _.reject(remoteBooks, function (sourceBook) {
                            return _.any(destination, function (destBook) {
                                return destBook.equals(sourceBook);
                            });
                        });
                    })
                    .then(function (newBooks) {

                        logger.log('debug', '%s - Saving %s new books', author.name, newBooks.length);

                        // Create all the new books
                        return Q.all(newBooks.map(function (bookData) {
                            var book = new Book(bookData);
                            return book.assignAuthor(author).then(function () {
                                return book;
                            });
                        }));
                    }).then(function (newBooks) {
                        logger.log('info', '%s - Finished refreshing books. Had %s new books.', author.name, newBooks.length);
                        // Set author status to active and save.
                        author.status = author._status;
                        author.save(function (err) {
                            if (err) {
                                logger.log('error', '%s - Error setting author status after refresh', author.name, logger.errorAsJSON(err));
                            }
                        });
                    });
            });
        }).fail(function (err) {
            logger.log('error', logger.errorAsJSON(err));
        });

};

/**
 * ## AuthorSchema.methods.merge
 *
 * Merge one author data into another.
 *
 * @param {Author} author Souce author data
 * @returns {Promise.<Author, Error>} Merged author when fulfilled, error when rejected.
 */
AuthorSchema.methods.merge = function (author) {
    var fields, thisAuthor;
    fields = 'name,description';
    thisAuthor = this;

    return Q.fcall(function () {
        return author;
    })
        .then(function (author) {
        // Check if they are equal in content
        if (_.isEqual(jsonMask(thisAuthor, fields), jsonMask(author, fields))) {
            // When equal, just return the destination author
            return thisAuthor;
        } else {
            // Not equal so we add the new data to the existing book
            thisAuthor = _.extend(thisAuthor, jsonMask(author, fields));
            // Mark as merged for later tracking
            thisAuthor._merged = true;

            return thisAuthor;
        }
    }).fail(function (err) {
        logger.log('error', '%s - Error merging', thisAuthor.name, logger.errorAsJSON(err));
        return thisAuthor;
    });
};

// Define compound index on `name` and `user` as unique.
AuthorSchema.index({name: 1, user: 1}, {unique: true});

// Timestamp Plugin
AuthorSchema.plugin(require('mongoose-timestamp'), {
    createdAt: 'created',
    updatedAt: 'updated'
});

// Pagination Plugin
AuthorSchema.plugin(require('./plugins/paginate'), {});

// Register Model
mongoose.model('Author', AuthorSchema);
