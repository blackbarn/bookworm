// # paginate.js
// _app/models/plugins/_

// Paginate Plugin for Mongoose.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var async = require('async'),
    _ = require('lodash');

/**
 * ## paginate
 *
 * Extend Mongoose Models to paginate queries
 *
 * @param {Object} q Mongoose Query Object
 * @param {Number} pageNumber
 * @param {Number} resultsPerPage
 * @param {Function} callback Callback
 * @param {Object} options Options
 */

function paginate(q, pageNumber, resultsPerPage, callback, options) {

    /*jshint validthis:true */
    var query, skipFrom, sortBy, columns, populate, model = this;

    // Default options
    options = options || {};

    // Optionally allow defining of columns
    columns = options.columns || null;

    // Default sortBy to id if not provided
    sortBy = options.sortBy || {
        _id : 1
    };

    // Default populate to null if it does not exist.
    populate = options.populate || null;

    // Allow for optional callback
    callback = callback || function() {};

    // Calculate skip value
    skipFrom = (pageNumber * resultsPerPage) - resultsPerPage;

    // Set up the query
    query = model.find(q);

    // When we have columns select them
    if (columns !== null) {
        query = query.select(options.columns);
    }

    // Skip, limit and sort the query
    query = query.skip(skipFrom).limit(resultsPerPage).sort(sortBy);

    // Convert populate to an array
    if (!_.isArray(populate)) {
        populate = [populate];
    }
    // Iterate over populate items adding it to the query
    _.forEach(populate, function (populateItem) {
        if (populateItem) {
            query = query.populate(populateItem);
        }
    });

    // Execute in parallel count and results queries
    async.parallel({
        results: function(callback) {
            // Eximply execute the prepared query
            query.exec(callback);
        },
        count: function(callback) {
            // Grab a count for the query for use in pagination
            model.count(q, function(err, count) {
                callback(err, count);
            });
        }
    }, function(err, data) {
        if (err) {
            callback(err);
        } else {
            // Send back err, pageCount, results, itemCount
            callback(null, Math.ceil(data.count / resultsPerPage) || 1, data.results, data.count);
        }
    });
}

module.exports = function(schema) {
    // Apply to the schema
    schema.statics.paginate = paginate;
};