// # book.js
// _app/models/_

// Book Model.
'use strict';

// ## Module Dependencies
// Include all the necessary node modules.
var mongoose = require('mongoose'),
    Q = require('q'),
    mongooseUtil = require('../util/mongoose'),
    Schema = mongoose.Schema,
    Author = mongoose.model('Author'),
    uuid = require('node-uuid'),
    jsonMask = require('json-mask'),
    _ = require('lodash'),
    logger = require('../providers/log/logger').get('Book-Schema'),
    ReleasesProvider = require('../providers/search/releases'),
    NotifiersProvider = require('../providers/notify/notifiers');

var statuses = {
    WANTED: 'wanted',
    WANTED_NEW: 'wanted_new',
    SKIPPED: 'skipped',
    DOWNLOADED: 'downloaded',
    SNATCHED: 'snatched',
    EXCLUDED: 'excluded'
};

var app = null;

module.exports = function (instance) {
    app = instance;
};

/**
 * ## Book Schema
 *
 * Define the Book Model Schema.
 * @name Book
 */
var BookSchema = new Schema({
    externalId: {
        type: String,
        unique: true,
        trim: true,
        default: uuid.v4
    },
    title: {
        type: String,
        default: '',
        required: 'Please fill Book title',
        trim: true
    },
    provider: {
        type: Schema.ObjectId,
        ref: 'Provider'
    },
    providerData: {},
    description: {
        type: String,
        trim: true,
        default: ''
    },
    publisher: {
        type: String,
        trim: true,
        default: ''
    },
    averageRating: {
        type: Number,
        default: 0
    },
    pageCount: {
        type: Number,
        default: 0
    },
    language: {
        type: String,
        trim: true,
        default: ''
    },
    isbn: {
        type: String,
        default: '',
        trim: true
    },
    image: {
        type: String,
        default: ''
    },
    imageSmall: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        enum: Object.keys(statuses).map(function (k) {
            return statuses[k];
        }),
        lowercase: true,
        trim: true,
        default: 'skipped'
    },
    published: {
        type: String,
        default: ''
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User',
        required: 'Book must be associated with a user'
    },
    author: {
        type: Schema.ObjectId,
        ref: 'Author',
        required: 'Book must be associated with an author'
    },
    releases: [
        {
            type: Schema.ObjectId,
            ref: 'Release'
        }
    ]
});

// Pre save hook
BookSchema.pre('save', function (next) {
    var Provider = mongoose.model('Provider');
    if (this.isModified('provider')) {
        Provider.findById(this.provider, function (err, provider) {
            if (err) {
                next(err);
            } else if (!provider) {
                next(new Error('Provider does not exist'));
            } else {
                next();
            }
        });
    } else {
        next();
    }
});


// Pre-save hook. Uses book status to initiate async actions.
BookSchema.pre('save', function (next) {
    if (this.isModified('status')) {
        switch (this.status) {
            case statuses.WANTED:
                this.findAndWantEligibleRelease(false);
                break;
            case statuses.WANTED_NEW:
                this.findAndWantEligibleRelease(true);
                break;
        }
    }
    next();
});

// Pre-remove hook. Removes all releases for this book individually before removing the book.
BookSchema.pre('remove', function (next) {
    Q.ninvoke(this, 'populate', { path: 'releases'})
        .then(function (book) {
            return Q.all(book.releases.map(function (release) {
                return Q.ninvoke(release, 'remove');
            }));
        }).then(function () {
            next();
        }).fail(next).done();
});

// Post-save hook. Emit instance
BookSchema.post('save', function (book) {
    app.io.emit('book', book);
});

/**
 * ## BookSchema.statics.getSearchProperties
 *
 * Retrieves properties for searching given a query param object.
 * It will only match properties of this schema not on the ignore list.
 *
 * @param {Object} query Express `req.query` object
 * @returns {Object}
 */
BookSchema.statics.getSearchProperties = function (query) {
    return mongooseUtil.getMatchingProperties(BookSchema.paths, query, ['providerData']);
};

/**
 * ## BookSchema.statics.merge
 *
 * Merge an array of books into another
 *
 * @param {Book[]} destination The list to merge into
 * @param {Book[]} source The list to merge from
 * @returns {Promise.<Book[], Error>} List of newly merged books when fulfilled, error when rejected.
 */
BookSchema.statics.merge = function (destination, source) {
    return Q.fcall(function () {
        // Only two arrays are valid
        return _.isArray(destination) && _.isArray(source);
    }).then(function (valid) {
        if (valid) {
            // Go through every destination (local) book.
            return Q.all(destination.map(function (destBook) {
                var dupe;
                // See if there is a source (remote) book that matches
                dupe = _.find(source, function (sourceBook) {
                    return destBook.equals(sourceBook);
                });
                // If we have a match, merge the source into the destination.
                if (dupe) {
                    return destBook.merge(dupe);
                } else {
                    return destBook;
                }
            }.bind(this)));
        } else {
            throw new Error('Cannot merge book lists, invalid format.');
        }
    });

};

/**
 * ## BookSchema.methods.isWanted
 *
 * Check if book is wanted
 *
 * @returns {Boolean} True if wanted, false otherwise.
 */
BookSchema.methods.isWanted = function () {
    return this.status.indexOf('wanted') !== -1;
};

/**
 * ## BookSchema.methods.assignAuthor
 *
 * Assign an author to an instance of a book.
 *
 * @param {Object} author Author Object
 * @return {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
BookSchema.methods.assignAuthor = function (author) {
    return this.assignAuthorById(author.id);
};

/**
 * ## BookSchema.methods.assignProvider
 *
 * Assign a provider to an instance of a book
 *
 * @param {Object} provider Provider Object
 * @return {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
BookSchema.methods.assignProvider = function (provider) {
    this.provider = provider;
    return Q.ninvoke(this, 'save');
};

/**
 * ## BookSchema.methods.assignAuthorById
 *
 * Assign an author given its id to an instance of a book.
 *
 * @param {String} id The Author ID
 * @return {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
BookSchema.methods.assignAuthorById = function (id) {
    var book = this;

    return Q.ninvoke(Author, 'findById', id)
        .then(function (author) {
            if (author) {
                return author.assignBook(book);
            } else {
                throw new Error('Author does not exist');
            }
        });
};

/**
 * ## BookSchema.methods.notify
 *
 * Notify about a book
 *
 * @param {String} event Notification event
 */
BookSchema.methods.notify = function (event) {
    var notifiersProvider;

    notifiersProvider = new NotifiersProvider();
    logger.log('info', '%s - Attempting to notify for event %s', this.title, event);
    notifiersProvider.notify({
        event: event,
        book: this
    }).then(function (response) {
        if (response && !response.success) {
            logger.log('warn', '%s - Could not notify successfully.', this.title);
        }
    }.bind(this)).fail(function (err) {
        logger.log('error', logger.errorAsJSON(err));
    });
};

/**
 * ## BookSchema.methods.assignRelease
 *
 * Assign a book to an instance of an author
 *
 * @param {Object} release BRelease Object
 * @returns {Promise.<Null,Error>} Nothing when fulfilled, an error when rejected.
 */
BookSchema.methods.assignRelease = function (release) {
    var book = this,
        Book = mongoose.model('Book');

    release.book = book;

    return Q.ninvoke(release, 'save').then(function () {
        return Q.ninvoke(Book, 'update', {
            _id: book.id
        }, {
            $addToSet: {
                releases: release.id
            }
        });
    });
};

/**
 * ## BookSchema.methods.merge
 *
 * Merge the data of one book into another.
 *
 * @param {Book} book Book data to be merged into this book.
 * @returns {Promise.<Book, Error>} Updated book when fulfilled, error when rejected.
 */
BookSchema.methods.merge = function (book) {
    var fields, thisBook;
    fields = 'published,imageSmall,image,isbn,language,pageCount,description,title';
    thisBook = this;

    return Q.fcall(function () {
        return book;
    })
        .then(function (book) {
        // Check if they are equal in content
        if (_.isEqual(jsonMask(thisBook, fields), jsonMask(book, fields))) {
            // When equal, just return the destination book
            return thisBook;
        } else {
            // Not equal so we add the new data to the existing book
            thisBook = _.extend(thisBook, jsonMask(book, fields));
            // Mark as merged for later tracking
            thisBook._merged = true;
            // Save the new data
            return Q.ninvoke(thisBook, 'save').then(function () {
                return thisBook;
            });
        }
    }).fail(function (err) {
        logger.log('error', logger.errorAsJSON(err));
        return thisBook;
    });

};

/**
 * ## BookSchema.methods.equals
 *
 * Compare two books to see if they are equal.
 *
 * @param {Book} book Book to compare with this one.
 * @returns {Boolean} True if they are equal (enough).
 */
BookSchema.methods.equals = function (book) {
    if (this._id === book._id) {
        return true;
    }
    if (this.externalId === book.externalId) {
        return true;
    }
    return this.title.toLowerCase().trim() === book.title.toLowerCase().trim();

};

/**
 * ## BookSchema.methods.findAndWantEligibleRelease
 *
 * First find an eligible release, then set it as wanted to trigger the dowload.
 *
 * @param {Boolean} newOnly Only find new releases, ones that have not been downloaded before.
 */
BookSchema.methods.findAndWantEligibleRelease = function (newOnly) {
    this.findEligibleRelease(newOnly).then(function (release) {
        if (release) {
            release.status = 'wanted';
            release.save(function (err) {
                if (err) {
                    logger.log('error', '%s - Error saving release', release.name, logger.errorAsJSON(err));
                } else {
                    logger.log('info', '%s - Release %s found and marked as wanted', this.title, release.name);
                }
            }.bind(this));
        }
    }.bind(this)).fail(function (err) {
        logger.log('error', 'Error finding eligible release', logger.errorAsJSON(err));
    });
};
/**
 * ## BookSchema.methods.findRelease
 *
 * Find an eligible release for this book. Can retry existing, try local or search for new in the cloud
 *
 * @param {Boolean} newOnly Only find new releases, ones that have not been downloaded before.
 * @returns {Promise.<Release, Error>} Eligible release when fulfilled, error when rejected.
 */
BookSchema.methods.findEligibleRelease = function (newOnly) {
    var releasesProvider, Release;
    Release = mongoose.model('Release');

    releasesProvider = new ReleasesProvider();

    return Q.ninvoke(this, 'populate', 'releases author')
        .then(function (book) {
            var retryRelease, availableRelease, releases;

            // Sort releases by updated date
            releases = _.sortBy(book.releases, function (release) {
                return release.updated;
            });

            // When not only checking for new, find first retryable release
            if (!newOnly) {
                retryRelease = _.find(releases, function (release) {
                    return release.isRetryable();
                });
            }
            // Find available local releases
            availableRelease = _.find(releases, function (release) {
                return release.isAvailable();
            });

            // If we have a retry release, return it.
            // Otherwise, if we have an available release, return it.
            // Finally, if no retry or local release is available, check the cloud.
            if (retryRelease) {
                logger.log('info', '%s - Retrying previous release: %s', book.title, retryRelease.name);
                return retryRelease;
            } else if (availableRelease) {
                logger.log('info', '%s - Trying sorted available release: %s', book.title, availableRelease.name);
                return availableRelease;
            } else {
                logger.log('info', '%s - Searching for new release in cloud', book.title);
                return releasesProvider.find({
                    title: book.title,
                    author: book.author.name
                }).then(function (remoteReleases) {
                    // Remove any remote releases that we already have locally
                    return _.reject(remoteReleases, function (remoteRelease) {
                        return _.find(releases, function (release) {
                            return release.externalId === remoteRelease.externalId;
                        });
                    });
                }).then(function (remoteReleases) {
                    remoteReleases = remoteReleases || [];
                    // Go through new releases and save them
                    logger.log('info', '%s - Found %s new releases', book.title, remoteReleases.length);
                    return Q.all(remoteReleases.map(function (release) {
                        return release.assignBook(book).then(function () {
                            return release;
                        });
                    }));
                }).then(function (releases) {
                    var release;
                    // Grab first release
                    release = _.first(releases);
                    // Notify of multiple or no releases.
                    if (releases && releases.length > 1) {
                        logger.log('info', '%s - Found multiple releases, trying first %s', book.title, release.name);
                    } else if (releases && releases.length) {
                        logger.log('info', '%s - Found one release %s', book.title, release.name);
                    } else {
                        logger.log('info', '%s - No releases found', book.title);
                    }
                    return release;
                });
            }

        });

};

// Define compound indexes
BookSchema.index({title: 1, author: 1, user: 1}, {unique: true});

// Timestamp Plugin
BookSchema.plugin(require('mongoose-timestamp'), {
    createdAt: 'created',
    updatedAt: 'updated'
});

// Pagination Plugin
BookSchema.plugin(require('./plugins/paginate'), {});

// Register Model
mongoose.model('Book', BookSchema);