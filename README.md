
Bookworm is a e-book search/download automation application.

It is a full-stack JavaScript application, which utilizes [MongoDB](http://www.mongodb.org/), [Node.js](http://www.nodejs.org/), [Express](http://expressjs.com/), and [AngularJS](http://angularjs.org/) at its core.

## NOTE!!!

**_Bookworm can be considered in Alpha and is still in active development._**

## Features

* Search for books using the Google Books API
* Search for NZBs using Newznab Hosts
* Send NZBs to SABnzbd+
* _more to come_

## Prerequisites

Make sure you have installed all these prerequisites on your machine.

* Node.js - [Download & Install Node.js](http://www.nodejs.org/download/) and the npm package manager, if you encounter any problems, you can also use this [Github Gist](https://gist.github.com/isaacs/579814) to install Node.js.
* MongoDB - [Download & Install MongoDB](http://www.mongodb.org/downloads), and make sure it's running on the default port (27017).
* Bower - You're going to use the [Bower Package Manager](http://bower.io/) to manage your front-end packages, in order to install it make sure you've installed Node.js and npm, then install bower globally using npm:

```
$ npm install -g bower
```

* Gulp - [Gulp Streaming Build System](http://gulpjs.com/) is used for the build system, in order to install it make sure you've installed Node.js and npm, then install gulp globally using npm:

```
$ npm install -g gulp
```

## Downloading Bookworm

### Cloning The GitHub Repository

You can also use Git to directly clone the Bookworm repository:

```
$ git clone https://github.com/blackbarn/Bookworm.git
```

This will clone the latest version of the Bookworm repository to a **Bookworm** folder.


## Quick Install

Once you've downloaded the application and installed all the prerequisites you'll want to install the Node.js dependencies.

To install Node.js dependencies you're going to use npm again, in the Bookworm folder run this in the command-line:

```
$ npm install
```

This command does a few things:

* First it will install the dependencies needed for the application to run.
* If you're running in a development environment, it will then also install development dependencies needed for testing and running your application.
* Finally, when the install process is over, npm will initiate a bower install command to install all the front-end modules needed for the application

## Running The Application

After the install process is over, you'll be able to run your application using Gulp, just run gulp default task:

```
$ gulp
```

The application should run on the 3000 port so in your browser just go to [http://localhost:3000](http://localhost:3000)
                            
That's it! your application should be running by now.

To run the application in a production environment:

```
$ NODE_ENV=production gulp
```

To run the application tests:

```
$ NODE_ENV=test gulp test
```

For more Gulp tasks:

```
$ gulp help
```

If you encounter any problems try the Troubleshooting section.

## Providers

### Google Books

Google Books has a vast database of information. However due to this some results may be inaccurate or unreliable. Use the 'exclude' feature to remove books from view that you do not care about.

Google enforces a requests per day limit on the Google Books API. [Reference](http://productforums.google.com/forum/#!msg/books-api/64GYbc9sRW4/jD8CNdpcPhMJ)
The limit is 1000 Requests per day.

You may need to set up a [Google API Console](https://code.google.com/apis/console/) account to retrieve a Google Books API Key.

It will come with 1000 requests/day quota, you can request more if you wish via an online form.

You can place your API key in the settings of Bookworm.

## Project Documentation

Want some code documentation? Run `gulp docker` and check out the `/doc` directory.

Want some source analysis? Run `gulp plato` and check out the `/reports/*/plato` directories.

Want some code coverage? Run `NODE_ENV=test gulp test` and check out the `/reports/*/coverage` directories.

## API Docs

Bookworm makes use of [Swagger](https://helloreverb.com/developers/swagger) to document and publish its API.

To access, start up Bookworm as you would normally and access the local swagger instance:

```
http://host:port/swagger
```

e.g., 

```
http://localhost:3000/swagger
```

Once there, you should see the example api. To browse the Bookworm API fill in the URL with:

```
http://host:port/api-doc
```

e.g.,

```
http://localhost:3000/api-doc
```

Also, either log in to the application in another window or tab, or obtain your user's api key and fill it in for authentication.

Lastly hit "Explore".

If you run Bookworm on a different host, you'll need to change the swagger basePath.

Add the following to a custom environment configuration `json` file in `Bookworm/config/env-custom/` such as `Bookworm/config/env-custom/development.json`.

```
{
  "api": {
    "basePath": "http://your.custom.host:custom-port"
  }
}
```

## Overriding Configurations

You can provide any configuration overrides by using environment specific `json` files placed in `Bookworm/config/env-custom`.

So, you can override `production`, `development`, `test`, and `all`.

Simply write the JSON to conform to the structure in the original configuration including any properties you wish to override with their values.

## Troubleshooting

_coming soon_

## Credits

* Inspired by other great automation applications such as [Sickbeard](), [CouchPotato](), [Headphones]() and [Sabnzbd]()
* Made possible by [MEAN.js](http://meanjs.org/), [MongoDB](http://www.mongodb.org/), [Node.js](http://www.nodejs.org/), [Express](http://expressjs.com/), and [AngularJS](http://angularjs.org/)
* Utilizes the API's of [Google Books](https://developers.google.com/books/?csw=1), [Newznab](https://newznab.readthedocs.org/en/latest/misc/api/) and [Sabnzbd](http://wiki.sabnzbd.org/api)

## License

(The MIT License)

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
